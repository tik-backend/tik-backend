using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.BusinessService;
using Tik.BusinessService.Helper;
using Tik.BusinessService.Interfaces;
using Tik.DomainModels;
using Tik.DomainModels.Filter;
using Tik.DomainModels.ProjectDomainModel;
using Tik.Repository;
using Tik.Repository.CommonRepository;
using Tik.Repository.ProjectRepository;
using System.Linq;

namespace Tik.Management
{
    public class ReviewAnalyzerFunc
    {        
        private readonly IUserRepository _userRepo;
        private readonly IAuthenticationService _authentication;
        private readonly ICompanyRepository _companyRepo;
        private readonly IReviewRepository _reviewRepo;
        private readonly IProjectRepository _projectRepo;

        public ReviewAnalyzerFunc(IUserRepository userRepo, IAuthenticationService authentication, ICompanyRepository companyRepo, IReviewRepository reviewRepo, IProjectRepository projectRepo)
        {
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(_userRepo));
            this._authentication = authentication ?? throw new ArgumentNullException(nameof(_authentication));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(_companyRepo));
            this._reviewRepo = reviewRepo ?? throw new ArgumentNullException(nameof(_reviewRepo));
            this._projectRepo = projectRepo ?? throw new ArgumentNullException(nameof(projectRepo));
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin")]
        [FunctionName("ReviewAnalyzerFunc")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage requestMessage,
            ILogger log)
        {
            AuthenticationStatus status = _authentication.GetAuthenticationStatus(requestMessage.Headers);
            if (status.Code != HttpStatusCode.Accepted)
            {
                var result = new ObjectResult(status.Message);
                result.StatusCode = Convert.ToInt32(status.Code);
                return result;
            }

            var accessToken = _authentication.ValidateAuthenticationToken(requestMessage);
            CompanyEntity companyEntity = await _companyRepo.GetItemById(accessToken.CompanyId);
            if (companyEntity == null)
                return new BadRequestObjectResult("Company does not exists.");

            bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);

            var projectFilter = new ProjectFilter();
            projectFilter.isCompany = isCompany;
            projectFilter.myproject = false;
            projectFilter.companyId = accessToken.CompanyId;
            projectFilter.stages = "ConfirmedCompletedProjects";
            List<ProjectEntity> confirmedList = await _projectRepo.GetFilteredProjectList(projectFilter);


            List<ReviewEntity> reviewList = await _reviewRepo.GetReviewListByMonth(confirmedList.Select(x => x.Id).ToArray(), null, null);



            return new JsonResult(reviewList);
        }
    }
}
