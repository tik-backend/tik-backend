﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.ViewModels;
using Tik.Repository;

namespace Tik.Common
{
    public class CategoryFunc
    {
        private readonly ICategoryRepository _categoryRepo;

        public CategoryFunc(ICategoryRepository categoryRepo)
        {
            this._categoryRepo = categoryRepo ?? throw new ArgumentNullException(nameof(categoryRepo));
        }

        [FunctionName("Category")]
        public async Task<IActionResult> Category(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                List<CompanyEntity> categories = await _categoryRepo.GetCategories();
                List<CategoryViewModel> vmCategories = new List<CategoryViewModel>();
                categories.ForEach(c => vmCategories.Add(new CategoryViewModel() { Id = c.Id, CategoryName = c.CategoryName }));
                return new JsonResult(vmCategories);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
