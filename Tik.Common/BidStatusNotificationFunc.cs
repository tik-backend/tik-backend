using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Tik.DomainModels;
using Tik.Repository.CommonRepository;
using System.Collections.Generic;
using Tik.DomainModels.CommonEntity;
using System.Linq;
using Tik.DomainModels.ViewModels;
using Tik.BusinessService.Helper;
using Tik.BusinessService.Interfaces;
using System.Net.Http;

namespace Tik.Common
{
    public class BidStatusNotificationFunc
    {
        private readonly IBidNotificationRepository _notificationRepo;
        private readonly IAuthenticationService _tokenIssuer;
        public BidStatusNotificationFunc(IBidNotificationRepository notificationRepo, IAuthenticationService tokenIssuer)
        {
            this._notificationRepo = notificationRepo ?? throw new ArgumentNullException(nameof(notificationRepo));
            this._tokenIssuer = tokenIssuer ?? throw new ArgumentNullException(nameof(tokenIssuer));
        }

        [FunctionName("GetNotificationList")]
        public async Task<IActionResult> GetNotificationList(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                bool isCompany = BusinessService.Helper.Common.CheckIsCompanyUser(accessToken.Category);
                List<BidNotification> list = await _notificationRepo.GetNotificationList(isCompany);
                List<BidNotificationViewModel> vmlist = new List<BidNotificationViewModel>();
                list.ForEach(c => vmlist.Add(new BidNotificationViewModel() { Id = c.Id, Status = c.DisplayName }));
                return new JsonResult(vmlist);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
