using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using Tik.DomainModels.ViewModels;

namespace Tik.Common
{
    public class CompanyFunc
    {
        private readonly ICompanyRepository _companyRepo;
        private readonly IUserProfileRepository _userProfileRepo;

        public CompanyFunc(ICompanyRepository companyRepo, IUserProfileRepository userProfileRepo)
        {
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
            this._userProfileRepo = userProfileRepo ?? throw new ArgumentNullException(nameof(userProfileRepo));
        }

        [FunctionName("GetCompany")]
        public async Task<IActionResult> GetCompany(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string companyId = req.Query["companyId"];
                var company = await _companyRepo.GetCompanyById(companyId);
                CompanyViewModel companyDetails = new CompanyViewModel();
                companyDetails.Id = company.Id;                
                companyDetails.UnitName = company.UnitName;
                companyDetails.Review = company.Review;
                if (company.ImageName != null)
                {
                    byte[] imgBytes = _userProfileRepo.GetImageBytes(company.ImageName);
                    companyDetails.Image = imgBytes;
                }

                if (company != null)
                {
                    return new JsonResult(companyDetails);
                }
                else
                {
                    return new BadRequestObjectResult("Company detail is not available.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
