﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Tik.BusinessService.EmailServices;
using Tik.DomainModels;
using Tik.Repository;
using Tik.Repository.CommonRepository;
using Tik.BusinessService.Interfaces;
using Tik.BusinessService;
using Tik.Repository.EmployeeRepository;
using Tik.Repository.CertificationRepository;

[assembly: FunctionsStartup(typeof(Tik.Common.AppStartup))]
namespace Tik.Common
{
    public class AppStartup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IStateRepository, StateRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IForgetPasswordRepository, ForgetPasswordRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IEmailFormatRepository, EmailFormatRepository>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IUserProfileRepository, UserProfileRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IBidNotificationRepository, BidNotificationRepository>();

            services.AddScoped<ICertificationRepository, CertificationRepository>();

            services.Configure<SendGridEmailSettings>(configuration.GetSection("SendGridEmailSettings"));
            services.AddScoped<IEmailService, SendGridEmailService>();

            string conStr = System.Environment.GetEnvironmentVariable($"ConnectionStrings_Tik", EnvironmentVariableTarget.Process);
            string databaseName = System.Environment.GetEnvironmentVariable($"DatabaseName", EnvironmentVariableTarget.Process);
            string blobConStr = System.Environment.GetEnvironmentVariable($"AzureWebJobsStorage", EnvironmentVariableTarget.Process);

            services.AddCosmosDb(conStr, databaseName, blobConStr);
        }
    }
}
