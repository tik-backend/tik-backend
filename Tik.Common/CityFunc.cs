using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using Tik.DomainModels;
using System.Collections.Generic;
using Tik.DomainModels.ViewModels;

namespace Tik.Common
{
    public class CityFunc
    {
        private readonly ICityRepository _cityRepo;

        public CityFunc(ICityRepository cityRepo)
        {
            this._cityRepo = cityRepo ?? throw new ArgumentNullException(nameof(cityRepo));
        }

        [FunctionName("City")]
        public async Task<IActionResult> City(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string stateIdinCity = req.Query["StateId"];
                List<CityEntity> cities = await _cityRepo.GetCities(stateIdinCity);
                List<CityViewModel> vmCities = new List<CityViewModel>();
                cities.ForEach(c => vmCities.Add(new CityViewModel() { Id = c.Id, StateId = c.StateId, CityName = c.CityName }));
                if (vmCities.Count > 0)
                {
                    return new JsonResult(vmCities);
                }
                else
                {
                    return new BadRequestObjectResult("No Cities availbale for the selected State.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
