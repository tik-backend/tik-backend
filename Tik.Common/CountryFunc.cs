using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using System.Net.Http;
using Tik.DomainModels;
using System.Collections.Generic;
using Tik.DomainModels.ViewModels;

namespace Tik.Common
{
    public class CountryFunc
    {
        private readonly ICountryRepository _countryRepo;

        public CountryFunc(ICountryRepository countryRepo)
        {
            this._countryRepo = countryRepo ?? throw new ArgumentNullException(nameof(countryRepo));
        }

        [FunctionName("Country")]
        public async Task<IActionResult> Country(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                List<CountryEntity> countries = await _countryRepo.GetCountries();
                List<CountryViewModel> vmCountries = new List<CountryViewModel>();
                countries.ForEach(c => vmCountries.Add(new CountryViewModel() { Id = c.Id, CountryName = c.CountryName }));
                return new JsonResult(vmCountries);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
