using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using System.Net.Http;
using Tik.DomainModels;
using System.Collections.Generic;
using Tik.DomainModels.ViewModels;

namespace Tik.Common
{
    public class CertificationFunc
    {
        private readonly ICertificationRepository _certificationRepo;

        public CertificationFunc(ICertificationRepository certificationRepo)
        {
            this._certificationRepo = certificationRepo ?? throw new ArgumentNullException(nameof(certificationRepo));
        }

        [FunctionName("Certification")]
        public async Task<IActionResult> Certification(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                List<CertificationEntity> certificates = await _certificationRepo.GetCertificates();
                List<CertificationViewModel> vmCertificates = new List<CertificationViewModel>();
                certificates.ForEach(c => vmCertificates.Add(new CertificationViewModel() { CertificateID = c.CertificateId, CertificateName = c.CertificateName }));
                return new JsonResult(vmCertificates);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }


    }
}
