using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using Tik.DomainModels;
using System.Collections.Generic;
using Tik.DomainModels.ViewModels;

namespace Tik.Common
{
    public class StateFunc
    {
        private readonly IStateRepository _stateRepo;

        public StateFunc(IStateRepository stateRepo)
        {
            this._stateRepo = stateRepo ?? throw new ArgumentNullException(nameof(stateRepo));
        }

        [FunctionName("State")]
        public async Task<IActionResult> State(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string countryIdinState = req.Query["CountryId"];
                List<StateEntity> states = await _stateRepo.GetStates(countryIdinState);
                List<StateViewModel> vmStates = new List<StateViewModel>();
                states.ForEach(c => vmStates.Add(new StateViewModel() { Id = c.Id, stateId = c.StateID, StateName = c.StateName }));
                if (vmStates.Count > 0)
                {
                    return new JsonResult(vmStates);
                }
                else
                {
                    return new BadRequestObjectResult("No States availbale for the selected Country.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
