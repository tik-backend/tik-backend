﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;
using Tik.DomainModels.EmployeeDomainModel.EmployeeViewModel;
using Tik.Repository;
using Tik.Repository.CommonRepository;
using Tik.BusinessService.Interfaces;
using Tik.BusinessService.Helper;

namespace Tik.Common
{
    public class RoleFunc
    {
        private readonly IAuthenticationService _tokenIssuer;
        private readonly IRoleRepository _roleRepo;
        private readonly IUserRepository _userRepo;

        public RoleFunc(IAuthenticationService tokenIssuer, IRoleRepository roleRepo, IUserRepository userRepo)
        {
            this._tokenIssuer = tokenIssuer ?? throw new ArgumentNullException(nameof(tokenIssuer));
            this._roleRepo = roleRepo ?? throw new ArgumentNullException(nameof(roleRepo));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
        }

        [FunctionName("GetRoles")]
        public async Task<IActionResult> GetRoles(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                bool isCompany = BusinessService.Helper.Common.CheckIsCompanyUser(accessToken.Category);

                List<RoleViewModel> vmRoles = new List<RoleViewModel>();
                List<RoleEntity> roles = await _roleRepo.GetRoles(isCompany);
                roles.ForEach(c => vmRoles.Add(new RoleViewModel() { Id = c.Id, EmployeeRole = c.Role }));
                return new JsonResult(vmRoles);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionName("GetMasterAdmin")]
        public async Task<IActionResult> GetMasterAdmin(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string masterAdminsinCompany = req.Query["companyId"];
                string role = AssignRole.MA.Meaning;
                List<UserEntity> masterAdmins = await _userRepo.GetUserByRoles(masterAdminsinCompany, role);
                List<MasterAdminViewModel> vmMasterAdmin = new List<MasterAdminViewModel>();
                masterAdmins.ForEach(m => vmMasterAdmin.Add(new MasterAdminViewModel() { Id = m.Id, MasterAdmin = m.FirstName + ' ' + m.LastName }));
                return new JsonResult(vmMasterAdmin);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionName("GetApprovalAdmin")]
        public async Task<IActionResult> GetApprovalAdmin(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string approvalAdminsinCompany = req.Query["companyId"];
                string role = AssignRole.AA.Meaning;
                List<UserEntity> approvalAdmins = await _userRepo.GetUserByRoles(approvalAdminsinCompany, role);
                List<ApprovalAdminViewModel> vmApprovalAdmin = new List<ApprovalAdminViewModel>();
                approvalAdmins.ForEach(a => vmApprovalAdmin.Add(new ApprovalAdminViewModel() { Id = a.Id, ApprovalAdmin = a.FirstName + ' ' + a.LastName }));
                return new JsonResult(vmApprovalAdmin);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionName("GetMerchandiser")]
        public async Task<IActionResult> GetMerchandiser(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string merchandisersinCompany = req.Query["companyId"];
                string role = AssignRole.M.Meaning;
                List<UserEntity> merchandisers = await _userRepo.GetUserByRoles(merchandisersinCompany, role);
                List<MerchandiserViewModel> vmMerchandiser = new List<MerchandiserViewModel>();
                merchandisers.ForEach(a => vmMerchandiser.Add(new MerchandiserViewModel() { Id = a.Id, Merchandiser = a.FirstName + ' ' + a.LastName }));
                return new JsonResult(vmMerchandiser);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
