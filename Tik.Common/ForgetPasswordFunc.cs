﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;
using Tik.DomainModels.Validators;
using Tik.DomainModels.ViewModels;
using Tik.Repository;
using Tik.Repository.CommonRepository;

namespace Tik.Common
{
    public class ForgetPasswordFunc
    {
        private readonly IForgetPasswordRepository _forgetPswdRepo;
        private readonly IUserRepository _userRepo;

        public ForgetPasswordFunc(IForgetPasswordRepository forgetPswdRepo, IUserRepository userRepo)
        {
            this._forgetPswdRepo = forgetPswdRepo ?? throw new ArgumentNullException(nameof(forgetPswdRepo));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
        }

        [FunctionName("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword([HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string emailAddress = req.Query["EmailAddress"];
                UserEntity userEntity = new UserEntity()
                {
                    EmailAddress = emailAddress
                };

                if (string.IsNullOrEmpty(userEntity.EmailAddress))
                    return new BadRequestObjectResult("Username is Empty.");

                bool isSuccess = await _forgetPswdRepo.CreateTempPasswordandSendMail(userEntity);
                if (!isSuccess)
                    return new BadRequestObjectResult("User not exists.");
                else
                    return new JsonResult("Temporary password has been sent to the EmailAdress - '" + emailAddress + "' successfully.");
            }
            catch (Exception ex)
            {
                log.LogError("Eror creating forget password {0} {1} ", ex.Message, ex.StackTrace);
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionName("ChangePassword")]
        public async Task<IActionResult> ChangePassword([HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                string jsonContent = await req.Content.ReadAsStringAsync();

                var resetDetails = await req.GetJsonBody<ForgetPasswordEntity, ResetPasswordValidator>();
                if (!resetDetails.IsValid)
                {
                    return resetDetails.ToBadRequest();
                }

                UserEntity userEntity = await _userRepo.GetItemById(resetDetails.Value.Id);
                if (userEntity == null)
                    return new BadRequestObjectResult("User not found.");

                var validatePassword = JsonConvert.DeserializeObject<UserViewModel>(jsonContent);
                userEntity.Password = validatePassword.NewPassword;
                if (string.IsNullOrEmpty(userEntity.Password))
                    return new BadRequestObjectResult("New Password is required.");

                if (string.IsNullOrEmpty(validatePassword.ConfirmPassword))
                    return new BadRequestObjectResult("Confirm Password is Required.");
                if (validatePassword.ConfirmPassword != validatePassword.NewPassword)
                {
                    return new BadRequestObjectResult("Password does not match.");
                }

                ForgetPasswordEntity forgetPassword = await _forgetPswdRepo.GetItemById(userEntity.Id);
                if (forgetPassword == null)
                    return new BadRequestObjectResult("Temporary Password not found for the User.");
                if (resetDetails.Value.TemporaryPassword != forgetPassword.TemporaryPassword)
                    return new BadRequestObjectResult("Enter the valid Password.");

                bool isSuccess = await _forgetPswdRepo.UpdatePassword(userEntity.Id, userEntity);
                if (!isSuccess)
                    return new BadRequestObjectResult("User does not exists.");
                else
                    return new JsonResult("Password has been updated successfully.");
            }
            catch (Exception ex)
            {
                log.LogError("Eror creating change password {0} {1} ", ex.Message, ex.StackTrace);
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
