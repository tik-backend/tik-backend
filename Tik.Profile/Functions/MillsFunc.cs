using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Tik.Repository.Profiles.Interfaces;
using Tik.DomainModels.ProfileModels;
using System.Collections.Generic;
using Tik.Profile.ViewModels;
using Tik.DomainModels.Utilities.Constants;
using Tik.DomainModels.Helpers;
using Tik.Repository.Profiles.Implementations;
using System.Net.Http;
using System.Linq;

namespace Tik.Profile
{
    public class MillsFunc
    {
        private readonly IMillsContentQualityRepository _qualityRepo;
        private readonly IMillsContentRepository _contentRepo;
        private readonly IMillsMelangeSlubRepository _melanSlubRepo;
        private readonly ISpecialDyedRepository _specialDyedRepo;
        private readonly IMillsRepository _millsRepo;
        private readonly IFabricStructureRepository _fabricStructureRepo;

        public List<MillsContentTemplateModel> SingleContents = new List<MillsContentTemplateModel>();
        public List<MillsContentTemplateModel> BlendContents = new List<MillsContentTemplateModel>();

        public List<MillsContentQualityTemplate> SingleQualities = new List<MillsContentQualityTemplate>();
        public List<MillsContentQualityTemplate> BlendQualities = new List<MillsContentQualityTemplate>();

        public List<MelanSlubTypeTemplateModel> MelangeTypes = new List<MelanSlubTypeTemplateModel>();
        public List<MelanSlubTypeTemplateModel> SlubTypes = new List<MelanSlubTypeTemplateModel>();

        public List<SpecialDyedYarnTypeTemplate> SingleSpecialDyedTypes = new List<SpecialDyedYarnTypeTemplate>();
        public List<SpecialDyedYarnTypeTemplate> BlendSpecialDyedTypes = new List<SpecialDyedYarnTypeTemplate>();

        public List<FabricTypesTemplateModel> SingleJerseyTypes = new List<FabricTypesTemplateModel>();
        public List<FabricTypesTemplateModel> InterlockTypes = new List<FabricTypesTemplateModel>();
        public List<FabricTypesTemplateModel> Ribtypes = new List<FabricTypesTemplateModel>();

        public MillsFunc(IMillsContentQualityRepository qualityRepo,
            IMillsContentRepository contentRepo,
            IMillsMelangeSlubRepository melanSlubRepo,
            ISpecialDyedRepository specialDyedRepo,
            IMillsRepository millsRepo,
            IFabricStructureRepository fabricStructureRepo)
        {
            this._qualityRepo = qualityRepo ?? throw new ArgumentNullException(nameof(qualityRepo));
            this._contentRepo = contentRepo ?? throw new ArgumentNullException(nameof(contentRepo));
            this._melanSlubRepo = melanSlubRepo ?? throw new ArgumentNullException(nameof(melanSlubRepo));
            this._specialDyedRepo = specialDyedRepo ?? throw new ArgumentNullException(nameof(specialDyedRepo));
            this._millsRepo = millsRepo ?? throw new ArgumentNullException(nameof(millsRepo));
            this._fabricStructureRepo = fabricStructureRepo ?? throw new ArgumentException(nameof(fabricStructureRepo));
        }

        private MillsViewModel GetMillsViewModelWithExistingMillItems(List<MillsDomainModel> mills, MillsViewModel millsViewModel)
        {
            foreach (var mill in mills)
            {
                if (mill.Yarn != null && mill.Yarn.RegularYarn != null)
                {
                    SetExistingRegularWithMillsViewModel(mill.Yarn.RegularYarn, millsViewModel.Yarn.RegularYarn, false);
                }
                if (mill.Fabric != null && mill.Fabric.RegularFabric != null)
                {
                    SetExistingRegularWithMillsViewModel(mill.Fabric.RegularFabric, millsViewModel.Fabric.RegularFabric, true);
                }

                if (mill.Yarn != null && mill.Yarn.SpecialYarn != null)
                {
                    SetExistingSpecialDyedWithMillsViewModel(mill.Yarn.SpecialYarn, millsViewModel.Yarn.SpecialYarn, false);
                }
                if (mill.Fabric != null && mill.Fabric.SpecialFabric != null)
                {
                    SetExistingSpecialDyedWithMillsViewModel(mill.Fabric.SpecialFabric, millsViewModel.Fabric.SpecialFabric, true);
                }

                if (mill.Yarn != null && mill.Yarn.DyedYarn != null)
                {
                    SetExistingSpecialDyedWithMillsViewModel(mill.Yarn.DyedYarn, millsViewModel.Yarn.DyedYarn, false);
                }

                if (mill.Yarn != null && mill.Yarn.MelangeSlubYarn != null && mill.Yarn.MelangeSlubYarn.Melange != null)
                {
                    SetExistingMelangeSlubWithMillsViewModel(mill.Yarn.MelangeSlubYarn.Melange, millsViewModel.Yarn.MelangeSlubYarn.Melange, false);
                }
                if (mill.Fabric != null && mill.Fabric.MelangeSlubFabric != null && mill.Fabric.MelangeSlubFabric.Melange != null)
                {
                    SetExistingMelangeSlubWithMillsViewModel(mill.Fabric.MelangeSlubFabric.Melange, millsViewModel.Fabric.MelangeSlubFabric.Melange, true);
                }

                if (mill.Yarn != null && mill.Yarn.MelangeSlubYarn != null && mill.Yarn.MelangeSlubYarn.Slub != null)
                {
                    SetExistingMelangeSlubWithMillsViewModel(mill.Yarn.MelangeSlubYarn.Slub, millsViewModel.Yarn.MelangeSlubYarn.Slub, false);
                }
                if (mill.Fabric != null && mill.Fabric.MelangeSlubFabric != null && mill.Fabric.MelangeSlubFabric.Slub != null)
                {
                    SetExistingMelangeSlubWithMillsViewModel(mill.Fabric.MelangeSlubFabric.Slub, millsViewModel.Fabric.MelangeSlubFabric.Slub, true);
                }
            }
            return millsViewModel;
        }

        private void SetExistingMelangeSlubWithMillsViewModel(MelanSlubDomainModel existingMelangeSlub, MelanSlubViewModel melanSlubViewModel, bool isFabric)
        {
            var singlecontentOptions = melanSlubViewModel.SingleContents[0].ContentOptions;
            var blendcontentOptions = melanSlubViewModel.BlendContents[0].ContentOptions;
            var melanSlubTypeOptions = melanSlubViewModel.SingleContents[0].MelanSlubTypeOptions;

            SetExistingMelangeSlubSingleContentsToMillsViewModel(existingMelangeSlub.SingleContents, singlecontentOptions, melanSlubViewModel.SingleContents, melanSlubTypeOptions);
            SetExistingMelangeSlubBlendContentsToMillsViewModel(existingMelangeSlub.BlendContents, blendcontentOptions, melanSlubViewModel.BlendContents, melanSlubTypeOptions);

            melanSlubViewModel.AVGCreditTime = existingMelangeSlub.AVGCreditTime;

            SetExistingMelanSlubTypesToMillsViewModel(melanSlubViewModel.MelanSlubTypes, existingMelangeSlub.MelanSlubTypes);

            melanSlubViewModel.Counts = ConvertStringCountDeniersToIntCountDeniers(existingMelangeSlub.Counts);

            if (isFabric && existingMelangeSlub.FabricStructure != null)
            {
                SetExistingFabricStructureToMillsViewModel(existingMelangeSlub.FabricStructure, melanSlubViewModel.FabricStructure);
            }
        }

        private void SetExistingSpecialDyedWithMillsViewModel(SpecialDyedDomainModel existingSpclDyed, SpecialDyedViewModel spclDyedVM, bool isFabric)
        {
            var singlecontentOptions = spclDyedVM.SingleContents[0].ContentOptions;
            var blendcontentOptions = spclDyedVM.BlendContents[0].ContentOptions;

            SetExistingSpclDyedTypesToMillsViewModel(spclDyedVM.SingleSpecialDyedTypes, existingSpclDyed.SingleSpecialDyedTypes);
            SetExistingSpclDyedTypesToMillsViewModel(spclDyedVM.BlendSpecialDyedTypes, existingSpclDyed.BlendSpecialDyedTypes);

            SetExistingSingleContentsToMillsViewModel(existingSpclDyed.SingleContents, singlecontentOptions, spclDyedVM.SingleContents);
            SetExistingBlendContentsToMillsViewModel(existingSpclDyed.BlendContents, blendcontentOptions, spclDyedVM.BlendContents);

            spclDyedVM.AVGCreditTime = existingSpclDyed.AVGCreditTime;

            SetExistingContentQulatityToMillsViewModel(spclDyedVM.SingleQualities, existingSpclDyed.SingleQualities);
            SetExistingContentQulatityToMillsViewModel(spclDyedVM.BlendQualities, existingSpclDyed.BlendQualities);

            spclDyedVM.Counts = ConvertStringCountDeniersToIntCountDeniers(existingSpclDyed.Counts);
            spclDyedVM.Deniers = ConvertStringCountDeniersToIntCountDeniers(existingSpclDyed.Deniers);

            if (isFabric && existingSpclDyed.FabricStructure != null)
            {
                SetExistingFabricStructureToMillsViewModel(existingSpclDyed.FabricStructure, spclDyedVM.FabricStructure);
            }
        }

        private void SetExistingRegularWithMillsViewModel(RegularDomainModel existingRegular, RegularViewModel regularVM, bool IsFabric)
        {
            var singlecontentOptions = regularVM.SingleContents[0].ContentOptions;
            var blendcontentOptions = regularVM.BlendContents[0].ContentOptions;

            SetExistingSingleContentsToMillsViewModel(existingRegular.SingleContents, singlecontentOptions, regularVM.SingleContents);
            SetExistingBlendContentsToMillsViewModel(existingRegular.BlendContents, blendcontentOptions, regularVM.BlendContents);

            regularVM.AvgCreditTime = existingRegular.AVGCreditTime;

            SetExistingContentQulatityToMillsViewModel(regularVM.SingleQualities, existingRegular.SingleQualities);
            SetExistingContentQulatityToMillsViewModel(regularVM.BlendQualities, existingRegular.BlendQualities);

            regularVM.Counts = ConvertStringCountDeniersToIntCountDeniers(existingRegular.Counts);
            regularVM.Deniers = ConvertStringCountDeniersToIntCountDeniers(existingRegular.Deniers);

            if (IsFabric && existingRegular.FabricStructure != null)
            {
                SetExistingFabricStructureToMillsViewModel(existingRegular.FabricStructure, regularVM.FabricStructure);
            }
        }

        private void SetExistingFabricStructureToMillsViewModel(FabricStructureDomainModel existingFabricStructure, FabricStructureViewModel fabricStructureVM)
        {
            if (existingFabricStructure.OpenWidth != null)
            {
                fabricStructureVM.OpenWidth.AllFeeder = existingFabricStructure.OpenWidth.AllFeeder;
                fabricStructureVM.OpenWidth.AlternateFeeder = existingFabricStructure.OpenWidth.AlternateFeeder;
                fabricStructureVM.OpenWidth.GG = existingFabricStructure.OpenWidth.GG;
                fabricStructureVM.OpenWidth.Dia = existingFabricStructure.OpenWidth.Dia;
            }
            if (existingFabricStructure.Tubular != null)
            {
                fabricStructureVM.Tubular.AllFeeder = existingFabricStructure.Tubular.AllFeeder;
                fabricStructureVM.Tubular.AlternateFeeder = existingFabricStructure.Tubular.AlternateFeeder;
                fabricStructureVM.Tubular.GG = existingFabricStructure.Tubular.GG;
                fabricStructureVM.Tubular.Dia = existingFabricStructure.Tubular.Dia;
            }

            fabricStructureVM.SingleJersey = GetExistingFabricTypeToViewModel(existingFabricStructure.SingleJersey);
            fabricStructureVM.Interlock = GetExistingFabricTypeToViewModel(existingFabricStructure.Interlock);
            fabricStructureVM.Rib = GetExistingFabricTypeToViewModel(existingFabricStructure.Rib);
        }

        private List<FabricTypesViewModel> GetExistingFabricTypeToViewModel(List<FabricTypesDomainModel> fabrictypeDomainModels)
        {
            List<FabricTypesViewModel> fabricTypesViewModels = new List<FabricTypesViewModel>();
            if (fabrictypeDomainModels != null)
                foreach (var fabDM in fabrictypeDomainModels)
                {
                    FabricTypesViewModel fabricTypesViewModel = new FabricTypesViewModel() { Id = fabDM.Id, Name = fabDM.Name, IsSelected = true };
                    fabricTypesViewModels.Add(fabricTypesViewModel);
                }
            return fabricTypesViewModels;
        }

        private void SetExistingMelanSlubTypesToMillsViewModel(List<MelanSlubTypesViewModel> melanSlubTypes, List<MelanSlubTypeDomainModel> existingMelanSlubTypes)
        {
            if (existingMelanSlubTypes != null)
                foreach (var existingItem in existingMelanSlubTypes)
                {
                    var item = melanSlubTypes.FirstOrDefault(s => s.Id == existingItem.Id);
                    item.IsSelected = true;
                }
        }

        private void SetExistingSpclDyedTypesToMillsViewModel(List<SpecialDyedTypesViewModel> singleSpecialDyedTypes, List<SpecialDyedYarnTypeDomainModel> existingSingleSpecialDyedTypes)
        {
            if (existingSingleSpecialDyedTypes != null)
                foreach (var existingItem in existingSingleSpecialDyedTypes)
                {
                    var item = singleSpecialDyedTypes.FirstOrDefault(s => s.Id == existingItem.Id);
                    item.IsSelected = true;
                }
        }

        private void SetExistingMelangeSlubBlendContentsToMillsViewModel(List<BlendContentDomainModel> blendContents, List<MillsContentViewModel> blendcontentOptions, List<MelanSlubBlendContentViewModel> blendContentViewModels, List<MelanSlubTypesViewModel> melanSlubTypeOptions)
        {
            for (int i = 0; i < blendContents.Count; i++)
            {
                var blendContent = blendContents[i];
                MelanSlubBlendContentViewModel blendContentViewModel = new MelanSlubBlendContentViewModel();

                blendContentViewModel.MelanSlubTypeOptions = melanSlubTypeOptions;
                blendContentViewModel.MelanSlubType = blendContentViewModel.MelanSlubTypeOptions.FirstOrDefault(m => m.Id == blendContent.MelanSlubType?.Id);
                blendContentViewModel.ContentOptions = blendcontentOptions;
                blendContentViewModel.Content = blendContentViewModel.ContentOptions.FirstOrDefault(s => s.Id == blendContent.Content?.Id);

                blendContentViewModel.Combos = new List<ComboTemplateModel>();
                foreach (var combo in blendContent.Combos)
                {
                    ComboTemplateModel comboTemplateModel = new ComboTemplateModel();
                    comboTemplateModel.CombinationOne = combo.CombinationOne;
                    comboTemplateModel.CombinationTwo = combo.CombinationTwo;
                    blendContentViewModel.Combos.Add(comboTemplateModel);
                }
                blendContentViewModels.Insert(i, blendContentViewModel);
            }
        }

        private void SetExistingMelangeSlubSingleContentsToMillsViewModel(List<SingleContentDomainModel> singleContents, List<MillsContentViewModel> singlecontentOptions, List<MelanSlubSingleContentViewModel> singleContentViewModels, List<MelanSlubTypesViewModel> melanSlubTypeOptions)
        {
            for (int i = 0; i < singleContents.Count; i++)
            {
                var singleContent = singleContents[i];
                MelanSlubSingleContentViewModel singleContentViewModel = new MelanSlubSingleContentViewModel();

                singleContentViewModel.MelanSlubTypeOptions = melanSlubTypeOptions;
                singleContentViewModel.MelanSlubType = singleContentViewModel.MelanSlubTypeOptions.FirstOrDefault(m => m.Id == singleContent.MelanSlubType?.Id);

                singleContentViewModel.ContentOptions = singlecontentOptions;
                singleContentViewModel.Content = singleContentViewModel.ContentOptions.FirstOrDefault(s => s.Id == singleContent.Content?.Id);

                singleContentViewModels.Insert(i, singleContentViewModel);
            }
        }

        private void SetExistingSingleContentsToMillsViewModel(List<SingleContentDomainModel> singleContents, List<MillsContentViewModel> singlecontentOptions, List<SingleContentViewModel> singleContentViewModels)
        {
            for (int i = 0; i < singleContents.Count; i++)
            {
                var singleContent = singleContents[i];
                SingleContentViewModel singleContentViewModel = new SingleContentViewModel();
                singleContentViewModel.Spun = singleContent.Spun;
                singleContentViewModel.Filament = singleContent.Filament;
                singleContentViewModel.ContentOptions = singlecontentOptions;
                singleContentViewModel.Content = singleContentViewModel.ContentOptions.FirstOrDefault(s => s.Id == singleContent.Content?.Id);

                singleContentViewModels.Insert(i, singleContentViewModel);
            }
        }

        private void SetExistingBlendContentsToMillsViewModel(List<BlendContentDomainModel> blendContents, List<MillsContentViewModel> blendcontentOptions, List<BlendContentViewModel> blendContentViewModels)
        {
            for (int i = 0; i < blendContents.Count; i++)
            {
                var blendContent = blendContents[i];
                BlendContentViewModel blendContentViewModel = new BlendContentViewModel();
                blendContentViewModel.ContentOptions = blendcontentOptions;
                blendContentViewModel.Content = blendContentViewModel.ContentOptions.FirstOrDefault(s => s.Id == blendContent.Content?.Id);
                blendContentViewModel.Combos = new List<ComboTemplateModel>();
                foreach (var combo in blendContent.Combos)
                {
                    ComboTemplateModel comboTemplateModel = new ComboTemplateModel();
                    comboTemplateModel.CombinationOne = combo.CombinationOne;
                    comboTemplateModel.CombinationTwo = combo.CombinationTwo;
                    blendContentViewModel.Combos.Add(comboTemplateModel);
                }
                blendContentViewModels.Insert(i, blendContentViewModel);
            }
        }

        private void SetExistingContentQulatityToMillsViewModel(List<MillsContentQualityViewModel> contentQualitiesVM, List<MillsContentQualityDomainModel> existingQualities)
        {
            if (existingQualities != null)
                foreach (var existingQuality in existingQualities)
                {
                    var item = contentQualitiesVM.FirstOrDefault(s => s.Id == existingQuality.Id);
                    item.IsSelected = true;
                }
        }

        private List<int> ConvertStringCountDeniersToIntCountDeniers(List<string> existingCountsDeniers)
        {
            List<int> contsDeniers = new List<int>();
            if (existingCountsDeniers != null)
                foreach (var item in existingCountsDeniers)
                {
                    contsDeniers.Add(Convert.ToInt32(item));
                }
            return contsDeniers;
        }

        [FunctionName("CreateMills")]
        public async Task<IActionResult> CreateMills(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string companyId = req.Query["companyId"];
                var existingMillItems = await _millsRepo.GetMillsByCompanyId(companyId);

                SingleContents = await _contentRepo.GetMillsContents(Constant.MILLS_CONTENTTYPE_SINGLE);
                BlendContents = await _contentRepo.GetMillsContents(Constant.MILLS_CONTENTTYPE_BLEND);

                SingleQualities = await _qualityRepo.GetQualitiesByCategory(Constant.MILLS_CONTENTTYPE_SINGLE);
                BlendQualities = await _qualityRepo.GetQualitiesByCategory(Constant.MILLS_CONTENTTYPE_BLEND);

                MelangeTypes = await _melanSlubRepo.GetMelanSlubTypes(Constant.MILLS_MELANGE);
                SlubTypes = await _melanSlubRepo.GetMelanSlubTypes(Constant.MILLS_SLUB);

                SingleSpecialDyedTypes = await _specialDyedRepo.GetSpecialDyedYarnTypes(Constant.MILLS_CONTENTTYPE_SINGLE);
                BlendSpecialDyedTypes = await _specialDyedRepo.GetSpecialDyedYarnTypes(Constant.MILLS_CONTENTTYPE_BLEND);

                var fabricStructureTypes = await _fabricStructureRepo.GetFabricStructureTypes();
                SingleJerseyTypes = fabricStructureTypes.Where(s => s.Category == Constant.MILLS_SINGLEJERSEY).ToList();
                InterlockTypes = fabricStructureTypes.Where(s => s.Category == Constant.MILLS_INTERLOCK).ToList();
                Ribtypes = fabricStructureTypes.Where(s => s.Category == Constant.MILLS_RIB).ToList();

                MillsTemplateModel millsTemplateModel = GetMillsTemplateModel();

                MillsViewModel millsViewModel = new MillsViewModel().MappingTemplateModelToViewModel(millsTemplateModel);

                if (existingMillItems != null && existingMillItems.Count > 0)
                    millsViewModel.Id = existingMillItems[0].Id;

                return new OkObjectResult(GetMillsViewModelWithExistingMillItems(existingMillItems, millsViewModel));
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        private MillsTemplateModel GetMillsTemplateModel()
        {
            MillsTemplateModel millsModel = new MillsTemplateModel();
            millsModel.Yarn = new YarnTemplateModel();
            millsModel.Yarn.RegularYarn = GetRegularTemplateModel(false);
            millsModel.Yarn.MelangeSlubYarn = GetMelangeSlubTemplateModel(false);
            millsModel.Yarn.SpecialYarn = GetSpecialDyedTemplateModel(false);
            millsModel.Yarn.DyedYarn = GetSpecialDyedTemplateModel(false);

            millsModel.Fabric = new FabricTemplateModel();
            millsModel.Fabric.RegularFabric = GetRegularTemplateModel(true);
            millsModel.Fabric.MelangeSlubFabric = GetMelangeSlubTemplateModel(true);
            millsModel.Fabric.SpecialFabric = GetSpecialDyedTemplateModel(true);

            return millsModel;
        }

        private SpecialDyedTemplateModel GetSpecialDyedTemplateModel(bool IsFabric)
        {
            SpecialDyedTemplateModel specialTemplateModel = GetBasicSpecialAndDyedTemplateModel();

            if (IsFabric)
            {
                specialTemplateModel.FabricStructure = GetFabricStructureTemplateModel();
            }
            return specialTemplateModel;
        }

        private RegularTemplateModel GetRegularTemplateModel(bool IsFabric)
        {
            RegularTemplateModel regularTemplate = GetBasicRegularTemplateModel();

            if (IsFabric)
            {
                regularTemplate.FabricStructure = GetFabricStructureTemplateModel();
            }
            return regularTemplate;
        }

        private MelangeSlubTemplateModel GetMelangeSlubTemplateModel(bool IsFabric)
        {
            MelangeSlubTemplateModel melangeSlubTemplateModel = new MelangeSlubTemplateModel();

            //Melan
            melangeSlubTemplateModel.Melange = GetBasicMelangeSlubTemplateModel(MelangeTypes);

            if (IsFabric)
            {
                melangeSlubTemplateModel.Melange.FabricStructure = GetFabricStructureTemplateModel();
            }

            //Slub
            melangeSlubTemplateModel.Slub = GetBasicMelangeSlubTemplateModel(SlubTypes);

            if (IsFabric)
            {
                melangeSlubTemplateModel.Slub.FabricStructure = GetFabricStructureTemplateModel();
            }
            return melangeSlubTemplateModel;
        }


        private FabricStructureTemplateModel GetFabricStructureTemplateModel()
        {
            FabricStructureTemplateModel fabricStructure = new FabricStructureTemplateModel();
            fabricStructure.SingleJersey = SingleJerseyTypes;
            fabricStructure.Interlock = InterlockTypes;
            fabricStructure.Rib = Ribtypes;
            return fabricStructure;
        }

        private MelanSlubTemplateModel GetBasicMelangeSlubTemplateModel(List<MelanSlubTypeTemplateModel> melangeSlubTypes)
        {
            MelanSlubTemplateModel melangeSlubTemplateModel = new MelanSlubTemplateModel();
            melangeSlubTemplateModel.MelanSlubTypes = melangeSlubTypes;
            melangeSlubTemplateModel.SingleContents = new List<SingleContentTemplateModel>();
            melangeSlubTemplateModel.SingleContents.Add(new SingleContentTemplateModel { MelanSlubTypes = melangeSlubTypes, Contents = SingleContents });
            melangeSlubTemplateModel.BlendContents = new List<BlendContentTemplateModel>();
            melangeSlubTemplateModel.BlendContents.Add(new BlendContentTemplateModel { MelanSlubTypes = melangeSlubTypes, Contents = BlendContents, Combos = new List<ComboTemplateModel>() { new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel() } });
            melangeSlubTemplateModel.Counts = new MillsHelper().GetCountsDeniers();
            return melangeSlubTemplateModel;
        }

        private SpecialDyedTemplateModel GetBasicSpecialAndDyedTemplateModel()
        {
            SpecialDyedTemplateModel specialDyedTemplateModel = new SpecialDyedTemplateModel();

            specialDyedTemplateModel.SingleSpecialDyedTypes = SingleSpecialDyedTypes;
            specialDyedTemplateModel.BlendSpecialDyedTypes = BlendSpecialDyedTypes;

            specialDyedTemplateModel.SingleContents = new List<SingleContentTemplateModel>();
            specialDyedTemplateModel.SingleContents.Add(new SingleContentTemplateModel { Contents = SingleContents });

            specialDyedTemplateModel.BlendContents = new List<BlendContentTemplateModel>();
            specialDyedTemplateModel.BlendContents.Add(new BlendContentTemplateModel { Contents = BlendContents, Combos = new List<ComboTemplateModel>() { new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel() } });

            specialDyedTemplateModel.SingleQualities = SingleQualities;
            specialDyedTemplateModel.BlendQualities = BlendQualities;

            specialDyedTemplateModel.Counts = new MillsHelper().GetCountsDeniers();
            specialDyedTemplateModel.Deniers = new MillsHelper().GetCountsDeniers();

            return specialDyedTemplateModel;
        }

        private RegularTemplateModel GetBasicRegularTemplateModel()
        {
            RegularTemplateModel regularTemplateModel = new RegularTemplateModel();

            regularTemplateModel.SingleContents = new List<SingleContentTemplateModel>();
            regularTemplateModel.SingleContents.Add(new SingleContentTemplateModel { Contents = SingleContents });

            regularTemplateModel.BlendContents = new List<BlendContentTemplateModel>();
            regularTemplateModel.BlendContents.Add(new BlendContentTemplateModel { Contents = BlendContents, Combos = new List<ComboTemplateModel>() { new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel(), new ComboTemplateModel() } });

            regularTemplateModel.SingleQualities = SingleQualities;
            regularTemplateModel.BlendQualities = BlendQualities;

            regularTemplateModel.Counts = new MillsHelper().GetCountsDeniers();
            regularTemplateModel.Deniers = new MillsHelper().GetCountsDeniers();

            return regularTemplateModel;
        }


        [FunctionName("AddMills")]
        public async Task<IActionResult> AddMills(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                string jsonContent = await req.Content.ReadAsStringAsync();

                MillsDomainModel millsDomainModel = JsonConvert.DeserializeObject<MillsDomainModel>(jsonContent);
                String regularYarnErrorMsg = null;
                String specialYarnErrorMsg = null;
                String dyedYarnErrorMsg = null;
                String melangeYarnErrorMsg = null;
                String slubYarnErrorMsg = null;

                String regularFabricErrorMsg = null;
                String specialFabricErrorMsg = null;
                String melangeFabricErrorMsg = null;
                String slubFabricErrorMsg = null;
                //Yarn
                if (millsDomainModel.Yarn != null && millsDomainModel.Yarn.RegularYarn != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Yarn.RegularYarn.SingleContents, millsDomainModel.Yarn.RegularYarn.BlendContents, false))
                        millsDomainModel.Yarn.RegularYarn = null;
                    if (millsDomainModel.Yarn.RegularYarn != null)
                        regularYarnErrorMsg = GetInValidRegularCategoryErrors(millsDomainModel.Yarn.RegularYarn, false);
                }

                if (millsDomainModel.Yarn != null && millsDomainModel.Yarn.SpecialYarn != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Yarn.SpecialYarn.SingleContents, millsDomainModel.Yarn.SpecialYarn.BlendContents, false))
                        millsDomainModel.Yarn.SpecialYarn = null;
                    if (millsDomainModel.Yarn.SpecialYarn != null)
                        specialYarnErrorMsg = GetInValidSpecialDyedCategoryErrors(millsDomainModel.Yarn.SpecialYarn, false);
                }

                if (millsDomainModel.Yarn != null && millsDomainModel.Yarn.DyedYarn != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Yarn.DyedYarn.SingleContents, millsDomainModel.Yarn.DyedYarn.BlendContents, false))
                        millsDomainModel.Yarn.DyedYarn = null;
                    if (millsDomainModel.Yarn.DyedYarn != null)
                        dyedYarnErrorMsg = GetInValidSpecialDyedCategoryErrors(millsDomainModel.Yarn.DyedYarn, false);
                }

                if (millsDomainModel.Yarn != null && millsDomainModel.Yarn.MelangeSlubYarn != null && millsDomainModel.Yarn.MelangeSlubYarn.Melange != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Yarn.MelangeSlubYarn.Melange.SingleContents, millsDomainModel.Yarn.MelangeSlubYarn.Melange.BlendContents, false))
                        millsDomainModel.Yarn.MelangeSlubYarn.Melange = null;
                    if (millsDomainModel.Yarn.MelangeSlubYarn.Melange != null)
                        melangeYarnErrorMsg = GetInValidMelangeSlubCategoryErrors(millsDomainModel.Yarn.MelangeSlubYarn.Melange, false);
                }

                if (millsDomainModel.Yarn != null && millsDomainModel.Yarn.MelangeSlubYarn != null && millsDomainModel.Yarn.MelangeSlubYarn.Slub != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Yarn.MelangeSlubYarn.Slub.SingleContents, millsDomainModel.Yarn.MelangeSlubYarn.Slub.BlendContents, false))
                        millsDomainModel.Yarn.MelangeSlubYarn.Slub = null;
                    if (millsDomainModel.Yarn.MelangeSlubYarn.Slub != null)
                        slubYarnErrorMsg = GetInValidMelangeSlubCategoryErrors(millsDomainModel.Yarn.MelangeSlubYarn.Slub, false);
                }

                //Fabric
                if (millsDomainModel.Fabric != null && millsDomainModel.Fabric.RegularFabric != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Fabric.RegularFabric.SingleContents, millsDomainModel.Fabric.RegularFabric.BlendContents, true))
                        millsDomainModel.Fabric.RegularFabric = null;
                    if (millsDomainModel.Fabric.RegularFabric != null)
                        regularFabricErrorMsg = GetInValidRegularCategoryErrors(millsDomainModel.Fabric.RegularFabric, true);
                }

                if (millsDomainModel.Fabric != null && millsDomainModel.Fabric.SpecialFabric != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Fabric.SpecialFabric.SingleContents, millsDomainModel.Fabric.SpecialFabric.BlendContents, true))
                        millsDomainModel.Fabric.SpecialFabric = null;
                    if (millsDomainModel.Fabric.SpecialFabric != null)
                        specialFabricErrorMsg = GetInValidSpecialDyedCategoryErrors(millsDomainModel.Fabric.SpecialFabric, true);
                }

                if (millsDomainModel.Fabric != null && millsDomainModel.Fabric.MelangeSlubFabric != null && millsDomainModel.Fabric.MelangeSlubFabric.Melange != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Fabric.MelangeSlubFabric.Melange.SingleContents, millsDomainModel.Fabric.MelangeSlubFabric.Melange.BlendContents, true))
                        millsDomainModel.Fabric.MelangeSlubFabric.Melange = null;
                    if (millsDomainModel.Fabric.MelangeSlubFabric.Melange != null)
                        melangeFabricErrorMsg = GetInValidMelangeSlubCategoryErrors(millsDomainModel.Fabric.MelangeSlubFabric.Melange, true);
                }

                if (millsDomainModel.Fabric != null && millsDomainModel.Fabric.MelangeSlubFabric != null && millsDomainModel.Fabric.MelangeSlubFabric.Slub != null)
                {
                    if (!IsValidSingleBlendContents(millsDomainModel.Fabric.MelangeSlubFabric.Slub.SingleContents, millsDomainModel.Fabric.MelangeSlubFabric.Slub.BlendContents, true))
                        millsDomainModel.Fabric.MelangeSlubFabric.Slub = null;
                    if (millsDomainModel.Fabric.MelangeSlubFabric.Slub != null)
                        slubFabricErrorMsg = GetInValidMelangeSlubCategoryErrors(millsDomainModel.Fabric.MelangeSlubFabric.Slub, true);
                }
                //Final Check For empty data
                if (millsDomainModel.Yarn != null && millsDomainModel.Yarn.MelangeSlubYarn?.Slub == null && millsDomainModel.Yarn.MelangeSlubYarn?.Melange == null && millsDomainModel.Yarn.RegularYarn == null && millsDomainModel.Yarn.SpecialYarn == null && millsDomainModel.Yarn.DyedYarn == null
                    && millsDomainModel.Fabric != null && millsDomainModel.Fabric.MelangeSlubFabric?.Slub == null && millsDomainModel.Fabric.MelangeSlubFabric?.Melange == null && millsDomainModel.Fabric.RegularFabric == null && millsDomainModel.Fabric.SpecialFabric == null)
                    return new OkObjectResult("Please give valid data...");

                string validationMsg = null;
                if (!string.IsNullOrEmpty(regularYarnErrorMsg))
                {
                    validationMsg += $" RegularYarn : {regularYarnErrorMsg }";
                }
                if (!string.IsNullOrEmpty(regularFabricErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" RegularFabric : {regularFabricErrorMsg }";
                }
                if (!string.IsNullOrEmpty(specialYarnErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" SpecialYarn : {specialYarnErrorMsg }";
                }
                if (!string.IsNullOrEmpty(specialFabricErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" SpecialFabric : {specialFabricErrorMsg }";
                }
                if (!string.IsNullOrEmpty(melangeYarnErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" MelangeYarn : {melangeYarnErrorMsg }";
                }
                if (!string.IsNullOrEmpty(melangeFabricErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" MelangeFabric : {melangeFabricErrorMsg }";
                }
                if (!string.IsNullOrEmpty(slubYarnErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" SlubYan : {slubYarnErrorMsg }";
                }
                if (!string.IsNullOrEmpty(slubFabricErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" SlubFabric : {slubFabricErrorMsg }";
                }
                if (!string.IsNullOrEmpty(dyedYarnErrorMsg))
                {
                    if (!string.IsNullOrEmpty(validationMsg))
                        validationMsg += Environment.NewLine;
                    validationMsg += $" DyedYarn : {dyedYarnErrorMsg }";
                }
                if (!string.IsNullOrEmpty(validationMsg))
                {
                    return new OkObjectResult(validationMsg);
                }

                await _millsRepo.AddMills(millsDomainModel);

                return new OkObjectResult("Mills saved succesfully...");
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        private string GetInValidMelangeSlubCategoryErrors(MelanSlubDomainModel melanSlub, bool isFabric)
        {
            string errMSg = "";

            if (melanSlub.Counts == null || melanSlub.Counts.Count == 0)
            {
                errMSg = "Missing required counts";
            }

            if (melanSlub.MelanSlubTypes == null || melanSlub.MelanSlubTypes.Count == 0)
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg = "Missing required types";
            }

            if (melanSlub.AVGCreditTime == null)
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required avarage credit period";
            }

            if (isFabric)
            {
                if (melanSlub.FabricStructure == null ||
                    (melanSlub.FabricStructure.SingleJersey == null || melanSlub.FabricStructure.SingleJersey.Count == 0) &&
                    (melanSlub.FabricStructure.Interlock == null || melanSlub.FabricStructure.Interlock.Count == 0) &&
                    (melanSlub.FabricStructure.Rib == null || melanSlub.FabricStructure.Rib.Count == 0))
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing required fabric structure";
                }
                else if (melanSlub.FabricStructure.Tubular == null && melanSlub.FabricStructure.OpenWidth == null)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing fabric details";
                }
            }
            return errMSg;
        }

        private string GetInValidSpecialDyedCategoryErrors(SpecialDyedDomainModel spclDyed, bool isFabric)
        {
            string errMSg = "";

            if (spclDyed.Counts == null || spclDyed.Counts.Count == 0)
            {
                errMSg = "Missing required counts";
            }
            if (spclDyed.Deniers == null || spclDyed.Deniers.Count == 0)
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required deniers";
            }
            if (spclDyed.SingleContents != null && spclDyed.SingleContents.Count > 0)
            {
                if (spclDyed.SingleQualities == null || spclDyed.SingleQualities.Count == 0)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing required single qualities";
                }
                if (spclDyed.SingleSpecialDyedTypes == null || spclDyed.SingleSpecialDyedTypes.Count == 0)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg = "Missing required single content types";
                }
            }
            if (spclDyed.BlendContents != null && spclDyed.BlendContents.Count > 0)
            {
                if (spclDyed.BlendQualities == null || spclDyed.BlendQualities.Count == 0)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing required blend qualities";
                }
                if (spclDyed.BlendSpecialDyedTypes == null || spclDyed.BlendSpecialDyedTypes.Count == 0)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg = "Missing required blend content types";
                }
            }
            if (spclDyed.AVGCreditTime == null)
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required avarage credit period";

            }
            if (isFabric)
            {
                if (spclDyed.FabricStructure == null ||
                    (spclDyed.FabricStructure.SingleJersey == null || spclDyed.FabricStructure.SingleJersey.Count == 0) &&
                    (spclDyed.FabricStructure.Interlock == null || spclDyed.FabricStructure.Interlock.Count == 0) &&
                    (spclDyed.FabricStructure.Rib == null || spclDyed.FabricStructure.Rib.Count == 0))
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing required fabric structure";
                }
                else if (spclDyed.FabricStructure.Tubular == null && spclDyed.FabricStructure.OpenWidth == null)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing fabric details";
                }
            }
            return errMSg;
        }

        private string GetInValidRegularCategoryErrors(RegularDomainModel regular, bool isFabric)
        {
            string errMSg = "";
            if (regular.Counts == null || regular.Counts.Count == 0)
            {
                errMSg = "Missing required counts";
            }
            if (regular.Deniers == null || regular.Deniers.Count == 0)
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required deniers";
            }
            if (regular.SingleContents != null && regular.SingleContents.Count > 0 && (regular.SingleQualities == null || regular.SingleQualities.Count == 0))
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required single qualities";
            }
            if (regular.BlendContents != null && regular.BlendContents.Count > 0 && (regular.BlendQualities == null || regular.BlendQualities.Count == 0))
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required blend qualities";
            }
            if (regular.AVGCreditTime == null)
            {
                if (!string.IsNullOrEmpty(errMSg))
                    errMSg += ", ";
                errMSg += "Missing required avarage credit period";

            }
            if (isFabric)
            {
                if (regular.FabricStructure == null ||
                    (regular.FabricStructure.SingleJersey == null || regular.FabricStructure.SingleJersey.Count == 0) &&
                    (regular.FabricStructure.Interlock == null || regular.FabricStructure.Interlock.Count == 0) &&
                    (regular.FabricStructure.Rib == null || regular.FabricStructure.Rib.Count == 0))
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing required fabric structure";
                }
                else if (regular.FabricStructure.Tubular == null && regular.FabricStructure.OpenWidth == null)
                {
                    if (!string.IsNullOrEmpty(errMSg))
                        errMSg += ", ";
                    errMSg += "Missing fabric details";
                }
            }
            return errMSg;
        }

        private bool IsValidSingleBlendContents(List<SingleContentDomainModel> singleContents, List<BlendContentDomainModel> blendContents, bool IsFabric)
        {
            bool emptySingleContent = false;
            bool emptyBlendContent = false;

            for (int i = 0; i < singleContents.Count; i++)
            {
                var singleContent = singleContents[i];
                if (singleContent.Content == null)
                {
                    emptySingleContent = true;
                    singleContents.Remove(singleContent);
                }
            }
            for (int i = 0; i < blendContents.Count; i++)
            {
                var blendContent = blendContents[i];
                if (blendContent.Content == null)
                {
                    emptyBlendContent = true;
                    blendContents.Remove(blendContent);
                }
            }

            return !(emptyBlendContent && emptySingleContent && blendContents.Count == 0 && singleContents.Count == 0);

        }
    }
}
