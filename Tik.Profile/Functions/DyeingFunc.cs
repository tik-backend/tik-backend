using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.DomainModels.Utilities.Constants;
using Tik.Profile.ViewModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Profile.Functions
{
    public class DyeingFunc
    {

        private readonly IMillsContentRepository _contentRepo;
        private readonly IFabricStructureRepository _fabricStructureRepo;
        private readonly IDyeingDipStrentnerRepository _dyeingDipStrentnerRepo;
        private readonly ICompactingMachinesRepository _compactingMachinesRepo;
        private readonly IDyeingDryingProcessesRepository _dyeingDryingProcessesRepo;
        private readonly IDyeingSpecialFinishesRepository _dyeingSpecialFinishesRepo;
        private readonly IDyeingProcessesRepository _dyeingProcessesRepo;
        private readonly IDyeingRepository _dyeingRepo;
        private readonly IDyeingVesselsRepository _dyeingVesselsRepo;

        public List<MillsContentDomainModel> SingleContents = new List<MillsContentDomainModel>();
        public List<MillsContentDomainModel> BlendContents = new List<MillsContentDomainModel>();

        public List<FabricTypesDomainModel> SingleJerseyTypes = new List<FabricTypesDomainModel>();
        public List<FabricTypesDomainModel> InterlockTypes = new List<FabricTypesDomainModel>();
        public List<FabricTypesDomainModel> Ribtypes = new List<FabricTypesDomainModel>();

        public List<DyeingDipStrenterDomainModel> DyeingDipStrenterTypes = new List<DyeingDipStrenterDomainModel>();

        public List<CompactingMachinesDomainModel> CompactingMachineTypes_Tubular = new List<CompactingMachinesDomainModel>();
        public List<CompactingMachinesDomainModel> CompactingMachineTypes_OpenWidth = new List<CompactingMachinesDomainModel>();

        public List<DyeingSpecialFinishesDomainModel> DyeingSpecialFinishTypes_Tubular = new List<DyeingSpecialFinishesDomainModel>();
        public List<DyeingSpecialFinishesDomainModel> DyeingSpecialFinishTypes_OpenWidth = new List<DyeingSpecialFinishesDomainModel>();

        public List<DyeingTubularDryingProcessDomainModel> DyeingDryingProcessTypes_Tubular = new List<DyeingTubularDryingProcessDomainModel>();
        public List<DyeingProcessDomainModel> DyeingProcessTypes = new List<DyeingProcessDomainModel>();

        public List<string> DyeingVesselSizes = new List<string>();

        public DyeingFunc(IDyeingDipStrentnerRepository dyeingDipStrentnerRepo,
            IMillsContentRepository contentRepo,
            IFabricStructureRepository fabricStructureRepo,
            ICompactingMachinesRepository compactingMachinesRepo,
            IDyeingDryingProcessesRepository dyeingDryingProcessesRepo,
            IDyeingSpecialFinishesRepository dyeingSpecialFinishesRepo,
            IDyeingProcessesRepository dyeingProcessesRepo,
            IDyeingRepository dyeingRepo,
            IDyeingVesselsRepository dyeingVesselsRepo)
        {
            this._contentRepo = contentRepo ?? throw new ArgumentNullException(nameof(contentRepo));
            this._fabricStructureRepo = fabricStructureRepo ?? throw new ArgumentException(nameof(fabricStructureRepo));
            this._compactingMachinesRepo = compactingMachinesRepo ?? throw new ArgumentException(nameof(compactingMachinesRepo));
            this._dyeingDipStrentnerRepo = dyeingDipStrentnerRepo ?? throw new ArgumentException(nameof(dyeingDipStrentnerRepo));
            this._dyeingDryingProcessesRepo = dyeingDryingProcessesRepo ?? throw new ArgumentException(nameof(dyeingDryingProcessesRepo));
            this._dyeingSpecialFinishesRepo = dyeingSpecialFinishesRepo ?? throw new ArgumentException(nameof(dyeingSpecialFinishesRepo));
            this._dyeingProcessesRepo = dyeingProcessesRepo ?? throw new ArgumentException(nameof(dyeingProcessesRepo));
            this._dyeingRepo = dyeingRepo ?? throw new ArgumentException(nameof(dyeingRepo));
            this._dyeingVesselsRepo = dyeingVesselsRepo ?? throw new ArgumentException(nameof(dyeingVesselsRepo));
        }

        [FunctionName("AddDyeing")]
        public async Task<IActionResult> AddDyeing(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                string jsonContent = await req.Content.ReadAsStringAsync();

                DyeingDomainModel dyeingDomainModel = JsonConvert.DeserializeObject<DyeingDomainModel>(jsonContent);

                await _dyeingRepo.AddDyeing(dyeingDomainModel);

                return new OkObjectResult("Dyeing saved succesfully...");
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionName("CreateDyeing")]
        public async Task<IActionResult> CreateDyeing(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string companyId = req.Query["companyId"];
                var existingDyeingItems = await _dyeingRepo.GetDyeingByCompanyId(companyId);

                var scs = await _contentRepo.GetMillsContents(Constant.MILLS_CONTENTTYPE_SINGLE);
                foreach (var sc in scs)
                    SingleContents.Add(new MillsContentDomainModel() { Id = sc.Id, Name = sc.Name });

                var bcs = await _contentRepo.GetMillsContents(Constant.MILLS_CONTENTTYPE_BLEND);
                foreach (var bc in bcs)
                    BlendContents.Add(new MillsContentDomainModel() { Id = bc.Id, Name = bc.Name });

                CompactingMachineTypes_Tubular = await _compactingMachinesRepo.GetCompactingMachinesByCategory(Constant.DYEING_TUBULAR);
                CompactingMachineTypes_OpenWidth = await _compactingMachinesRepo.GetCompactingMachinesByCategory(Constant.DYEING_OPENWIDTH);

                DyeingDipStrenterTypes = await _dyeingDipStrentnerRepo.GetDyeingDipStrentner();

                DyeingDryingProcessTypes_Tubular = await _dyeingDryingProcessesRepo.GetDyeingDryingProcessesByCategory(Constant.DYEING_TUBULAR);

                DyeingSpecialFinishTypes_Tubular = await _dyeingSpecialFinishesRepo.GetDyeingSpecialFinishesByCategory(Constant.DYEING_TUBULAR);
                DyeingSpecialFinishTypes_OpenWidth = await _dyeingSpecialFinishesRepo.GetDyeingSpecialFinishesByCategory(Constant.DYEING_OPENWIDTH);

                DyeingProcessTypes = await _dyeingProcessesRepo.GetDyeingProcesses();
                DyeingVesselSizes = GetDyeingVesselSizes(await _dyeingVesselsRepo.GetDyeingVesselSizes());


                var fabricStructureTypes = await _fabricStructureRepo.GetFabricStructureTypes();
                foreach (var fs in fabricStructureTypes.Where(s => s.Category == Constant.MILLS_SINGLEJERSEY).ToList())
                    SingleJerseyTypes.Add(new FabricTypesDomainModel() { Id = fs.Id, Name = fs.Name });

                foreach (var fs in fabricStructureTypes.Where(s => s.Category == Constant.MILLS_INTERLOCK).ToList())
                    InterlockTypes.Add(new FabricTypesDomainModel() { Id = fs.Id, Name = fs.Name });

                foreach (var fs in fabricStructureTypes.Where(s => s.Category == Constant.MILLS_RIB).ToList())
                    Ribtypes.Add(new FabricTypesDomainModel() { Id = fs.Id, Name = fs.Name });


                DyeingDomainModel dyeingDomainModel = GetInitialDyeingDomainModel();

                DyeingViewModel dyeingViewModel = new DyeingViewModel();
                dyeingViewModel = dyeingViewModel.MappingDomainModelToViewModel(dyeingDomainModel);

                if (existingDyeingItems != null && existingDyeingItems.Count > 0)
                    dyeingViewModel.Id = existingDyeingItems[0].Id;

                var response = GetDyeingViewModelWithExistingDyeingItems(existingDyeingItems, dyeingViewModel);

                return new OkObjectResult(response);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        private List<string> GetDyeingVesselSizes(List<DyeingVesselsDomainModel> dyeingVesselsDomainModels)
        {
            List<string> dyeingVesselSizes = new List<string>();
            foreach(var sizes in dyeingVesselsDomainModels)
            {
                dyeingVesselSizes.Add(sizes.Size);
            }
            return dyeingVesselSizes;
        }

        private DyeingViewModel GetDyeingViewModelWithExistingDyeingItems(List<DyeingDomainModel> existingDyeingItems, DyeingViewModel dyeingViewModel)
        {
            foreach (var dyeingItem in existingDyeingItems)
            {
                if (dyeingItem.FabricDyeing != null && dyeingItem.FabricDyeing.Winch != null)
                {
                    SetExistingFabricDyeingProcessesWithViewModel(dyeingItem.FabricDyeing.Winch, dyeingViewModel.FabricDyeing.Winch);
                }
                if (dyeingItem.FabricDyeing != null && dyeingItem.FabricDyeing.SoftFlow != null)
                {
                    SetExistingFabricDyeingProcessesWithViewModel(dyeingItem.FabricDyeing.SoftFlow, dyeingViewModel.FabricDyeing.SoftFlow);
                }
            }
            return dyeingViewModel;
        }

        private void SetExistingFabricDyeingProcessesWithViewModel(DyeingMethodologyDomianModel existingDyeingItem, DyeingMethodologyViewModel dyeingVM)
        {
            dyeingVM.SingleContents = GetExistingContentsTrueInDyeingViewModel(existingDyeingItem.SingleContents);
            dyeingVM.BlendContents = GetExistingContentsTrueInDyeingViewModel(existingDyeingItem.BlendContents);

            SetExistingDyeingProcessToViewModel(existingDyeingItem.DyeingProcesses, dyeingVM.DyeingProcesses);

            dyeingVM.Interlock = GetExistingFabricTypesToViewModel(existingDyeingItem.Interlock);
            dyeingVM.SingleJersey = GetExistingFabricTypesToViewModel(existingDyeingItem.SingleJersey);
            dyeingVM.Rib = GetExistingFabricTypesToViewModel(existingDyeingItem.Rib);
            dyeingVM.VesselSizes = existingDyeingItem.VesselSizes;

            dyeingVM.LycraBlend = existingDyeingItem.LycraBlend;

            SetExistingTubularProcessToViewModel(existingDyeingItem.TubularProcess, dyeingVM.TubularProcess);
            SetExistingOPWProcessToViewModel(existingDyeingItem.OpenWidthProcess, dyeingVM.OpenWidthProcess);

        }

        private void SetExistingOPWProcessToViewModel(DyeingOpenWidthProcessDomainModel existingOpenWidthProcess, DyeingOpenWidthProcessViewModel dyeingOpenWidthProcessViewModel)
        {
            dyeingOpenWidthProcessViewModel.HeatSetting = existingOpenWidthProcess.HeatSetting;
            dyeingOpenWidthProcessViewModel.OpenWidthCompactingMachines = existingOpenWidthProcess.OpenWidthCompactingMachines;
            dyeingOpenWidthProcessViewModel.SpecialFinishes = existingOpenWidthProcess.SpecialFinishes;
            SetExistingOpenWidthDryingProcessToViewModel(existingOpenWidthProcess.OpenWidthDryingProcesses, dyeingOpenWidthProcessViewModel.OpenWidthDryingProcesses);
        }

        private void SetExistingOpenWidthDryingProcessToViewModel(DyeingOpenWidthDryingProcessDomainModel existingOPWDryingProcesses, DyeingOpenWidthDryingProcessViewModel OPWDryingProcessVM)
        {
            OPWDryingProcessVM.RelaxDryer = existingOPWDryingProcesses.RelaxDryer;
            OPWDryingProcessVM.StenterDryer = existingOPWDryingProcesses.StenterDryer;
            OPWDryingProcessVM.DipStenters = GetExistingDipStentersToViewModel(existingOPWDryingProcesses.DipStenters);
        }

        private void SetExistingTubularProcessToViewModel(DyeingTubularProcessDomainModel existingubularProcess, DyeingTubularProcessViewModel dyeingTubularProcessViewModel)
        {
            dyeingTubularProcessViewModel.HeatSetting = existingubularProcess.HeatSetting;
            dyeingTubularProcessViewModel.OpenWidthCompactingMachines = existingubularProcess.OpenWidthCompactingMachines;
            dyeingTubularProcessViewModel.TubularCompactingMachines = existingubularProcess.TubularCompactingMachines;
            SetExistingOpenWidthDryingProcessToViewModel(existingubularProcess.OpenWidthDryingProcesses, dyeingTubularProcessViewModel.OpenWidthDryingProcesses);
            dyeingTubularProcessViewModel.TubularDryingProcesses = GetExistingTubularDryingProcessToViewModel(existingubularProcess.TubularDryingProcesses);
            dyeingTubularProcessViewModel.SpecialFinishes = existingubularProcess.SpecialFinishes;
        }

        private List<DyeingTubularDryingProcessViewModel> GetExistingTubularDryingProcessToViewModel(List<DyeingTubularDryingProcessDomainModel> existingTubularDryingProcesses)
        {
            List<DyeingTubularDryingProcessViewModel> dyeingTubularDryingProcessViewModels = new List<DyeingTubularDryingProcessViewModel>();
            if (existingTubularDryingProcesses != null)
                foreach (var exTubularDrying in existingTubularDryingProcesses)
                {
                    DyeingTubularDryingProcessViewModel dyeingTubularDryingProcessViewModel = new DyeingTubularDryingProcessViewModel();
                    dyeingTubularDryingProcessViewModel.Id = exTubularDrying.Id;
                    dyeingTubularDryingProcessViewModel.Name = exTubularDrying.Name;
                    dyeingTubularDryingProcessViewModel.IsSelected = true;

                    dyeingTubularDryingProcessViewModels.Add(dyeingTubularDryingProcessViewModel);
                }

            return dyeingTubularDryingProcessViewModels;
        }

        private DyeingOpenWidthDryingProcessViewModel GetExistingDryingProcessToViewModel(DyeingOpenWidthDryingProcessDomainModel existingOPWDryingProcesses)
        {
            DyeingOpenWidthDryingProcessViewModel dyeingOpenWidthDryingProcessViewModel = new DyeingOpenWidthDryingProcessViewModel();
            dyeingOpenWidthDryingProcessViewModel.RelaxDryer = existingOPWDryingProcesses.RelaxDryer;
            dyeingOpenWidthDryingProcessViewModel.StenterDryer = existingOPWDryingProcesses.StenterDryer;

            dyeingOpenWidthDryingProcessViewModel.DipStenters = GetExistingDipStentersToViewModel(existingOPWDryingProcesses.DipStenters);

            return dyeingOpenWidthDryingProcessViewModel;
        }

        private List<DyeingDipStrenterViewModel> GetExistingDipStentersToViewModel(List<DyeingDipStrenterDomainModel> existingDipStenters)
        {
            List<DyeingDipStrenterViewModel> dipStentersVMs = new List<DyeingDipStrenterViewModel>();
            if (existingDipStenters != null)
                foreach (var exOPWCom in existingDipStenters)
                {
                    DyeingDipStrenterViewModel dipStenterVM = new DyeingDipStrenterViewModel();
                    dipStenterVM.Id = exOPWCom.Id;
                    dipStenterVM.Name = exOPWCom.Name;
                    dipStenterVM.IsSelected = true;
                    dipStentersVMs.Add(dipStenterVM);
                }
            return dipStentersVMs;
        }

        private List<FabricTypesViewModel> GetExistingFabricTypesToViewModel(List<FabricTypesDomainModel> existingFabricTypes)
        {
            List<FabricTypesViewModel> fabricTypesViewModels = new List<FabricTypesViewModel>();
            if (existingFabricTypes != null)
                foreach (var exFab in existingFabricTypes)
                {
                    FabricTypesViewModel fabricTypesViewModel = new FabricTypesViewModel();
                    fabricTypesViewModel.Id = exFab.Id;
                    fabricTypesViewModel.Name = exFab.Name;
                    fabricTypesViewModel.IsSelected = true;
                    fabricTypesViewModels.Add(fabricTypesViewModel);
                }
            return fabricTypesViewModels;
        }

        private void SetExistingDyeingProcessToViewModel(List<DyeingProcessDomainModel> existingDyeingProcesses, List<DyeingProcessViewModel> dyeingProcessesVM)
        {
            if (existingDyeingProcesses != null)
                foreach (var exCont in existingDyeingProcesses)
                {
                    var item = dyeingProcessesVM.FirstOrDefault(dpvm => dpvm.Id == exCont.Id);
                    if (item != null)
                        item.IsSelected = true;
                }
        }

        private List<DyeingContentViewModel> GetExistingContentsTrueInDyeingViewModel(List<MillsContentDomainModel> existingContents)
        {
            List<DyeingContentViewModel> contentsVM = new List<DyeingContentViewModel>();
            if (existingContents != null)
                foreach (var exCont in existingContents)
                {
                    DyeingContentViewModel dyeingContentViewModel = new DyeingContentViewModel();
                    dyeingContentViewModel.Id = exCont.Id;
                    dyeingContentViewModel.Name = exCont.Name;
                    dyeingContentViewModel.IsSelected = true;
                    contentsVM.Add(dyeingContentViewModel);
                }
            return contentsVM;
        }

        private DyeingDomainModel GetInitialDyeingDomainModel()
        {
            DyeingDomainModel dyeingDomainModel = new DyeingDomainModel();
            dyeingDomainModel.YarnDyeing = null;

            dyeingDomainModel.FabricDyeing = new DyeingFabricDomainModel();
            dyeingDomainModel.FabricDyeing.SoftFlow = GetInitialDyeingMethodology();
            dyeingDomainModel.FabricDyeing.Winch = GetInitialDyeingMethodology();

            return dyeingDomainModel;
        }

        private DyeingMethodologyDomianModel GetInitialDyeingMethodology()
        {
            DyeingMethodologyDomianModel DyeingMethodology = new DyeingMethodologyDomianModel();
            DyeingMethodology.BlendContents = BlendContents;
            DyeingMethodology.SingleContents = SingleContents;
            DyeingMethodology.DyeingProcesses = DyeingProcessTypes;

            DyeingMethodology.SingleJersey = SingleJerseyTypes;
            DyeingMethodology.Interlock = InterlockTypes;
            DyeingMethodology.Rib = Ribtypes;

            DyeingMethodology.VesselSizes = DyeingVesselSizes;

            DyeingMethodology.TubularProcess = new DyeingTubularProcessDomainModel();
            DyeingMethodology.TubularProcess.OpenWidthCompactingMachines = GetCompactingMachines(CompactingMachineTypes_OpenWidth);
            DyeingMethodology.TubularProcess.OpenWidthDryingProcesses = new DyeingOpenWidthDryingProcessDomainModel();
            DyeingMethodology.TubularProcess.OpenWidthDryingProcesses.DipStenters = DyeingDipStrenterTypes;
            DyeingMethodology.TubularProcess.TubularCompactingMachines = GetCompactingMachines(CompactingMachineTypes_Tubular);
            DyeingMethodology.TubularProcess.TubularDryingProcesses = DyeingDryingProcessTypes_Tubular;
            DyeingMethodology.TubularProcess.SpecialFinishes = GetDyeingSpecialFinishes(DyeingSpecialFinishTypes_Tubular);

            DyeingMethodology.OpenWidthProcess = new DyeingOpenWidthProcessDomainModel();
            DyeingMethodology.OpenWidthProcess.OpenWidthCompactingMachines = GetCompactingMachines(CompactingMachineTypes_OpenWidth);
            DyeingMethodology.OpenWidthProcess.OpenWidthDryingProcesses = new DyeingOpenWidthDryingProcessDomainModel();
            DyeingMethodology.OpenWidthProcess.OpenWidthDryingProcesses.DipStenters = DyeingDipStrenterTypes;
            DyeingMethodology.OpenWidthProcess.SpecialFinishes = GetDyeingSpecialFinishes(DyeingSpecialFinishTypes_OpenWidth);
            return DyeingMethodology;
        }

        private List<string> GetCompactingMachines(List<CompactingMachinesDomainModel> compactingMachineTypes)
        {
            List<string> compactingMachines = new List<string>();
            foreach (var machine in compactingMachineTypes)
            {
                compactingMachines.Add(machine.Name);
            }
            return compactingMachines;
        }

        private List<string> GetDyeingSpecialFinishes(List<DyeingSpecialFinishesDomainModel> dyeingSpecialFinishTypes)
        {
            List<string> specialFinishes = new List<string>();
            foreach(var dsf in dyeingSpecialFinishTypes)
            {
                specialFinishes.Add(dsf.Name);
            }
            return specialFinishes;
        }
    }
}
