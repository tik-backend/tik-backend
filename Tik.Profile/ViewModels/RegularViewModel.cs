﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class RegularViewModel : MillsCommonViewModel
    {
        public List<SingleContentViewModel> SingleContents { get; set; }
        public List<BlendContentViewModel> BlendContents { get; set; }
        public List<MillsContentQualityViewModel> SingleQualities { get; set; }
        public List<MillsContentQualityViewModel> BlendQualities { get; set; }
        public List<int> Counts { get; set; }
        public List<int> AvailableCounts { get; set; }
        public List<int> Deniers { get; set; }
        public List<int> AvailableDeniers { get; set; }
        public string AvgCreditTime { get; set; }
        private string _YarnFabricCategory;
        public FabricStructureViewModel FabricStructure { get; set; }

        public RegularViewModel(string YarnFabricCategory) : base(YarnFabricCategory)
        {
            _YarnFabricCategory = YarnFabricCategory;
        }

        public RegularViewModel MappingTemplateModelToViewModel(RegularTemplateModel regularTemplateModel, bool isFabric)
        {
            //Mapping to ViewModel
            RegularViewModel regularVM = new RegularViewModel(_YarnFabricCategory);

            SingleContentViewModel singleContentVM = new SingleContentViewModel();
            regularVM.SingleContents = singleContentVM.MappingTemplateModelToViewModel(regularTemplateModel.SingleContents);

            BlendContentViewModel blendContentVM = new BlendContentViewModel();
            regularVM.BlendContents = blendContentVM.MappingTemplateModelToViewModel(regularTemplateModel.BlendContents);

            MillsContentQualityViewModel millsContentQualityViewModel = new MillsContentQualityViewModel();
            regularVM.BlendQualities = millsContentQualityViewModel.MappingTemplateModelToViewModel(regularTemplateModel.BlendQualities);
            regularVM.SingleQualities = millsContentQualityViewModel.MappingTemplateModelToViewModel(regularTemplateModel.SingleQualities);

            regularVM.AvailableCounts = regularTemplateModel.Counts;
            regularVM.AvailableDeniers = regularTemplateModel.Deniers;

            if (isFabric)
            {
                regularVM.FabricStructure = new FabricStructureViewModel();
                regularVM.FabricStructure.MappingTemplateModelToViewModel(regularTemplateModel.FabricStructure);
            }
            return regularVM;
        }
    }
}
