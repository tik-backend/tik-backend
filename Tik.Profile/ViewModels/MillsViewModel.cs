﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels;
using Tik.DomainModels.ProfileModels;
using Tik.DomainModels.Utilities.Constants;

namespace Tik.Profile.ViewModels
{
    public class MillsCommonViewModel
    {
        public string ContentLable { get; set; }
        public string QualityLable { get; set; }
        public string CountLable { get; set; }
        public string TypeLable { get; set; }
        public string StructureLable { get; set; }

        public MillsCommonViewModel(string YarnFabricType)
        {
            ContentLable = $"Choose the single & blend contents you offer in {YarnFabricType}";
            QualityLable = $"Choose the single & blend qualities you offer in {YarnFabricType}";
            CountLable = $"Choose the counts and deniers you offer in {YarnFabricType}";
            TypeLable = $"Choose the types you offer in {YarnFabricType}";
            StructureLable = $"Choose the Fabric structures you offer in {YarnFabricType}";
        }
    }
    public class MillsViewModel
    {
        public string Id { get; set; }
        public YarnViewModel Yarn { get; set; }
        public FabricViewModel Fabric { get; set; }
        public MillsViewModel MappingTemplateModelToViewModel(MillsTemplateModel millsModel)
        {
            MillsViewModel millsViewModel = new MillsViewModel();
            millsViewModel.Yarn = new YarnViewModel();
            millsViewModel.Yarn.RegularYarn = new RegularViewModel(Constant.MILLS_Regular + " Yarn");
            millsViewModel.Yarn.RegularYarn = millsViewModel.Yarn.RegularYarn.MappingTemplateModelToViewModel(millsModel.Yarn.RegularYarn, false);

            
            millsViewModel.Yarn.MelangeSlubYarn = new MelangeSlubParentViewModel();
            millsViewModel.Yarn.MelangeSlubYarn.Melange = new MelanSlubViewModel(Constant.MILLS_MELANGE + " Yarn");
            millsViewModel.Yarn.MelangeSlubYarn.Melange = millsViewModel.Yarn.MelangeSlubYarn.Melange.MappingTemplateModelToViewModel(millsModel.Yarn.MelangeSlubYarn.Melange, false);

            millsViewModel.Yarn.MelangeSlubYarn.Slub = new MelanSlubViewModel(Constant.MILLS_SLUB + " Yarn");
            millsViewModel.Yarn.MelangeSlubYarn.Slub = millsViewModel.Yarn.MelangeSlubYarn.Slub.MappingTemplateModelToViewModel(millsModel.Yarn.MelangeSlubYarn.Slub, false);

            millsViewModel.Yarn.SpecialYarn = new SpecialDyedViewModel(Constant.MILLS_Special + " Yarn");
            millsViewModel.Yarn.SpecialYarn = millsViewModel.Yarn.SpecialYarn.MappingTemplateModelToViewModel(millsModel.Yarn.SpecialYarn, false);

            millsViewModel.Yarn.DyedYarn = new SpecialDyedViewModel(Constant.MILLS_Dyed + " Yarn");
            millsViewModel.Yarn.DyedYarn = millsViewModel.Yarn.DyedYarn.MappingTemplateModelToViewModel(millsModel.Yarn.DyedYarn, false);

            millsViewModel.Fabric = new FabricViewModel();
            millsViewModel.Fabric.RegularFabric = new RegularViewModel(Constant.MILLS_Regular + " Fabric");
            millsViewModel.Fabric.RegularFabric = millsViewModel.Fabric.RegularFabric.MappingTemplateModelToViewModel(millsModel.Fabric.RegularFabric, true);

            millsViewModel.Fabric.SpecialFabric = new SpecialDyedViewModel(Constant.MILLS_Special + " Fabric");
            millsViewModel.Fabric.SpecialFabric = millsViewModel.Fabric.SpecialFabric.MappingTemplateModelToViewModel(millsModel.Fabric.SpecialFabric, true);

            millsViewModel.Fabric.MelangeSlubFabric = new MelangeSlubParentViewModel();
            millsViewModel.Fabric.MelangeSlubFabric.Melange = new MelanSlubViewModel(Constant.MILLS_MELANGE + " Fabric");
            millsViewModel.Fabric.MelangeSlubFabric.Melange = millsViewModel.Fabric.MelangeSlubFabric.Melange.MappingTemplateModelToViewModel(millsModel.Fabric.MelangeSlubFabric.Melange, true);

            millsViewModel.Fabric.MelangeSlubFabric.Slub = new MelanSlubViewModel(Constant.MILLS_SLUB + " Fabric");
            millsViewModel.Fabric.MelangeSlubFabric.Slub = millsViewModel.Fabric.MelangeSlubFabric.Slub.MappingTemplateModelToViewModel(millsModel.Fabric.MelangeSlubFabric.Slub, true);
           
            return millsViewModel;
        }
    }

   

    public class MillsContentQualityViewModel
    {
        public string Name;
        public string Id;
        public bool IsSelected;

        public List<MillsContentQualityViewModel> MappingTemplateModelToViewModel(List<MillsContentQualityTemplate> millsContentQualities)
        {
            List<MillsContentQualityViewModel> millsContentQualityViewModels = new List<MillsContentQualityViewModel>();
            foreach (var quality in millsContentQualities)
            {
                MillsContentQualityViewModel millsContentQualityViewModel = new MillsContentQualityViewModel();
                millsContentQualityViewModel.Name = quality.Name;
                millsContentQualityViewModel.Id = quality.Id;
                millsContentQualityViewModel.IsSelected = false;
                millsContentQualityViewModels.Add(millsContentQualityViewModel);
            }
            return millsContentQualityViewModels;
        }
    }
   
    public class SingleContentViewModel
    {
        public List<MillsContentViewModel> ContentOptions { get; set; }
        public MillsContentViewModel Content { get; set; }
        public string Filament { get; set; }
        public string Spun { get; set; }

        public List<SingleContentViewModel> MappingTemplateModelToViewModel(List<SingleContentTemplateModel> singleContentModels)
        {
            var SingleContents = new List<SingleContentViewModel>();
            foreach (var singleContent in singleContentModels)
            {
                SingleContentViewModel singleContentVM = new SingleContentViewModel();
                MillsContentViewModel millsContentViewModel = new MillsContentViewModel();
                singleContentVM.ContentOptions = millsContentViewModel.MappingTemplateModelToViewModel(singleContent.Contents);
                SingleContents.Add(singleContentVM);
            }
            return SingleContents;
        }
    }
    public class MillsContentViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public List<MillsContentViewModel> MappingTemplateModelToViewModel(List<MillsContentTemplateModel> ContentsModel)
        {
            List<MillsContentViewModel> ContentsVM = new List<MillsContentViewModel>();
            foreach (var content in ContentsModel)
            {
                MillsContentViewModel millsContentVM = new MillsContentViewModel();
                millsContentVM.Name = content.Name;
                millsContentVM.Id = content.Id;
                ContentsVM.Add(millsContentVM);
            }
            return ContentsVM;
        }
    }
    public class BlendContentViewModel
    {
        public List<MillsContentViewModel> ContentOptions { get; set; }
        public MillsContentViewModel Content { get; set; }
        public List<ComboTemplateModel> Combos { get; set; }

        public List<BlendContentViewModel> MappingTemplateModelToViewModel(List<BlendContentTemplateModel> blendContentModels)
        {
            var BlendContents = new List<BlendContentViewModel>();
            foreach (var blendContent in blendContentModels)
            {
                BlendContentViewModel blendContentVM = new BlendContentViewModel();
                MillsContentViewModel millsContentViewModel = new MillsContentViewModel();
                blendContentVM.ContentOptions = millsContentViewModel.MappingTemplateModelToViewModel(blendContent.Contents);


                blendContentVM.Combos = blendContent.Combos;

                BlendContents.Add(blendContentVM);
            }
            return BlendContents;
        }
    }
}
