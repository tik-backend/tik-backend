﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class FabricViewModel
    {
        public RegularViewModel RegularFabric { get; set; }
        public MelangeSlubParentViewModel MelangeSlubFabric { get; set; }
        public SpecialDyedViewModel SpecialFabric { get; set; }
    }
    public class FabricStructureViewModel
    {
        public List<FabricTypesViewModel> AvailableSingleJersey { get; set; }
        public List<FabricTypesViewModel> SingleJersey { get; set; }
        public List<FabricTypesViewModel> AvailableInterlock { get; set; }
        public List<FabricTypesViewModel> Interlock { get; set; }
        public List<FabricTypesViewModel> AvailableRib { get; set; }
        public List<FabricTypesViewModel> Rib { get; set; }
        public FabricRollFormatViewModel Tubular { get; set; }
        public FabricRollFormatViewModel OpenWidth { get; set; }

        internal void MappingTemplateModelToViewModel(FabricStructureTemplateModel fabricStructureTemplateModel)
        {
            FabricTypesViewModel fabricTypesViewModel = new FabricTypesViewModel();
            AvailableSingleJersey = fabricTypesViewModel.MappingTemplateModelToViewModel(fabricStructureTemplateModel.SingleJersey);
            AvailableInterlock = fabricTypesViewModel.MappingTemplateModelToViewModel(fabricStructureTemplateModel.Interlock);
            AvailableRib = fabricTypesViewModel.MappingTemplateModelToViewModel(fabricStructureTemplateModel.Rib);

            Tubular = new FabricRollFormatViewModel();
            OpenWidth = new FabricRollFormatViewModel();
        }
    }
    public class FabricTypesViewModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsSelected { get; set; }

        public List<FabricTypesViewModel> MappingTemplateModelToViewModel(List<FabricTypesTemplateModel> fabricTypesTemplateModels)
        {
            List<FabricTypesViewModel> fabricTypesViewModels = new List<FabricTypesViewModel>();
            foreach (var fabrictype in fabricTypesTemplateModels)
            {
                FabricTypesViewModel fab = new FabricTypesViewModel();
                fab.Id = fabrictype.Id;
                fab.Name = fabrictype.Name;
                fabricTypesViewModels.Add(fab);
            }
            return fabricTypesViewModels;
        }

        public List<FabricTypesViewModel> MappingDomainModelToViewModel(List<FabricTypesDomainModel> fabricTypesTemplateModels)
        {
            List<FabricTypesViewModel> fabricTypesViewModels = new List<FabricTypesViewModel>();
            foreach (var fabrictype in fabricTypesTemplateModels)
            {
                FabricTypesViewModel fab = new FabricTypesViewModel();
                fab.Id = fabrictype.Id;
                fab.Name = fabrictype.Name;
                fabricTypesViewModels.Add(fab);
            }
            return fabricTypesViewModels;
        }
    }
    public class FabricRollFormatViewModel
    {
        public List<string> GG { get; set; }
        public List<string> Dia { get; set; }
        public string AllFeeder { get; set; }
        public string AlternateFeeder { get; set; }
    }
}
