﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class MelangeSlubParentViewModel
    {
        public MelanSlubViewModel Melange { get; set; }
        public MelanSlubViewModel Slub { get; set; }

    }
    public class MelanSlubViewModel : MillsCommonViewModel
    {
        public List<MelanSlubTypesViewModel> MelanSlubTypes { get; set; }
        public List<MelanSlubSingleContentViewModel> SingleContents { get; set; }
        public List<MelanSlubBlendContentViewModel> BlendContents { get; set; }
        public List<int> Counts { get; set; }
        public List<int> AvailableCounts { get; set; }
        public string AVGCreditTime { get; set; }
        private string _YarnFabricCategory;
        public FabricStructureViewModel FabricStructure { get; set; }
        public MelanSlubViewModel(string YarnFabricCategory) : base(YarnFabricCategory)
        {
            _YarnFabricCategory = YarnFabricCategory;
        }

        public MelanSlubViewModel MappingTemplateModelToViewModel(MelanSlubTemplateModel melanSlubTemplateModel, bool isFabric)
        {
            MelanSlubViewModel melangeSlubYarnViewModel = new MelanSlubViewModel(_YarnFabricCategory);
            MelanSlubTypesViewModel melangeSlubTypesViewModel = new MelanSlubTypesViewModel();
            melangeSlubYarnViewModel.MelanSlubTypes = melangeSlubTypesViewModel.MappingTemplateModelToViewModel(melanSlubTemplateModel.MelanSlubTypes);

            MelanSlubSingleContentViewModel melangeSlubSingleContentViewModel = new MelanSlubSingleContentViewModel();
            melangeSlubYarnViewModel.SingleContents = melangeSlubSingleContentViewModel.MappingTemplateModelToViewModel(melanSlubTemplateModel.SingleContents);

            MelanSlubBlendContentViewModel melangeSlubBlendContentViewModel = new MelanSlubBlendContentViewModel();
            melangeSlubYarnViewModel.BlendContents = melangeSlubBlendContentViewModel.MappingTemplateModelToViewModel(melanSlubTemplateModel.BlendContents);
            melangeSlubYarnViewModel.AvailableCounts = melanSlubTemplateModel.Counts;

            if (isFabric)
            {
                melangeSlubYarnViewModel.FabricStructure = new FabricStructureViewModel();
                melangeSlubYarnViewModel.FabricStructure.MappingTemplateModelToViewModel(melanSlubTemplateModel.FabricStructure);
            }
            return melangeSlubYarnViewModel;
        }
    }

    public class MelanSlubTypesViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }

        public List<MelanSlubTypesViewModel> MappingTemplateModelToViewModel(List<MelanSlubTypeTemplateModel> melanSlubTypes)
        {
            List<MelanSlubTypesViewModel> melanSlubTypesViewModels = new List<MelanSlubTypesViewModel>();
            foreach (var melansubType in melanSlubTypes)
            {
                melanSlubTypesViewModels.Add(new MelanSlubTypesViewModel() { Id = melansubType.Id, Name = melansubType.Name });
            }
            return melanSlubTypesViewModels;
        }
    }
    public class MelanSlubSingleContentViewModel
    {
        public List<MillsContentViewModel> ContentOptions { get; set; }
        public MillsContentViewModel Content { get; set; }
        public List<MelanSlubTypesViewModel> MelanSlubTypeOptions { get; set; }
        public MelanSlubTypesViewModel MelanSlubType { get; set; }


        public List<MelanSlubSingleContentViewModel> MappingTemplateModelToViewModel(List<SingleContentTemplateModel> singleContentModels)
        {
            var SingleContents = new List<MelanSlubSingleContentViewModel>();
            foreach (var singleContent in singleContentModels)
            {
                MelanSlubSingleContentViewModel singleContentVM = new MelanSlubSingleContentViewModel();
                MillsContentViewModel millsContentViewModel = new MillsContentViewModel();
                singleContentVM.ContentOptions = millsContentViewModel.MappingTemplateModelToViewModel(singleContent.Contents);
                MelanSlubTypesViewModel melangeSlubTypesViewModel = new MelanSlubTypesViewModel();
                singleContentVM.MelanSlubTypeOptions = melangeSlubTypesViewModel.MappingTemplateModelToViewModel(singleContent.MelanSlubTypes);
                SingleContents.Add(singleContentVM);
            }
            return SingleContents;
        }
    }

    public class MelanSlubBlendContentViewModel
    {
        public List<MillsContentViewModel> ContentOptions { get; set; }
        public MillsContentViewModel Content { get; set; }
        public List<ComboTemplateModel> Combos { get; set; }
        public List<MelanSlubTypesViewModel> MelanSlubTypeOptions { get; set; }
        public MelanSlubTypesViewModel MelanSlubType { get; set; }

        public List<MelanSlubBlendContentViewModel> MappingTemplateModelToViewModel(List<BlendContentTemplateModel> blendContentModels)
        {
            var BlendContents = new List<MelanSlubBlendContentViewModel>();
            foreach (var blendContent in blendContentModels)
            {
                MelanSlubBlendContentViewModel melangeSlubBlendContentVM = new MelanSlubBlendContentViewModel();
                MillsContentViewModel blendContentVM = new MillsContentViewModel();
                melangeSlubBlendContentVM.ContentOptions = blendContentVM.MappingTemplateModelToViewModel(blendContent.Contents);

                melangeSlubBlendContentVM.Combos = blendContent.Combos;

                MelanSlubTypesViewModel melangeSlubTypesViewModel = new MelanSlubTypesViewModel();
                melangeSlubBlendContentVM.MelanSlubTypeOptions = melangeSlubTypesViewModel.MappingTemplateModelToViewModel(blendContent.MelanSlubTypes);
                BlendContents.Add(melangeSlubBlendContentVM);
            }
            return BlendContents;
        }
    }
}
