﻿using System;
using System.Collections.Generic;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class DyeingTubularProcessViewModel
    {
        public string HeatSetting { get; set; }
        public List<string> AvailableSpecialFinishes { get; set; }
        public List<DyeingTubularDryingProcessViewModel> AvailableTubularDryingProcesses { get; set; }
        public DyeingOpenWidthDryingProcessViewModel OpenWidthDryingProcesses { get; set; }
        public List<string> AvailableOpenWidthCompactingMachines { get; set; }
        public List<string> AvailableTubularCompactingMachines { get; set; }
        public List<string> SpecialFinishes { get; set; }
        public List<DyeingTubularDryingProcessViewModel> TubularDryingProcesses { get; set; }
        public List<string> OpenWidthCompactingMachines { get; set; }
        public List<string> TubularCompactingMachines { get; set; }

        public DyeingTubularProcessViewModel MappingDomainModelToViewModel(DyeingTubularProcessDomainModel tubularProcess)
        {
            DyeingTubularProcessViewModel dyeingTubularProcessViewModel = new DyeingTubularProcessViewModel();

            dyeingTubularProcessViewModel.AvailableSpecialFinishes= tubularProcess.SpecialFinishes;

            DyeingTubularDryingProcessViewModel dyeingTubularDryingProcessViewModel = new DyeingTubularDryingProcessViewModel();
            dyeingTubularProcessViewModel.AvailableTubularDryingProcesses = dyeingTubularDryingProcessViewModel.MappingDomainModelToViewModel(tubularProcess.TubularDryingProcesses);

            DyeingOpenWidthDryingProcessViewModel openWidthDryingProcessViewModel = new DyeingOpenWidthDryingProcessViewModel();
            dyeingTubularProcessViewModel.OpenWidthDryingProcesses = openWidthDryingProcessViewModel.MappingDomainModelToViewModel(tubularProcess.OpenWidthDryingProcesses);

            dyeingTubularProcessViewModel.AvailableOpenWidthCompactingMachines = tubularProcess.OpenWidthCompactingMachines;
            dyeingTubularProcessViewModel.AvailableTubularCompactingMachines = tubularProcess.TubularCompactingMachines;
           
            return dyeingTubularProcessViewModel;
        }
    }

    public class DyeingTubularDryingProcessViewModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsSelected { get; set; }

        public List<DyeingTubularDryingProcessViewModel> MappingDomainModelToViewModel(List<DyeingTubularDryingProcessDomainModel> dryingProcess)
        {
            List<DyeingTubularDryingProcessViewModel> dyeingTubularDryingProcessViewModels = new List<DyeingTubularDryingProcessViewModel>();
            foreach (var dp in dryingProcess)
                dyeingTubularDryingProcessViewModels.Add(new DyeingTubularDryingProcessViewModel() { Id = dp.Id, Name = dp.Name });

            return dyeingTubularDryingProcessViewModels;
        }
    }
}