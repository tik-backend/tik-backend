﻿using System.Collections.Generic;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class DyeingContentViewModel : MillsContentViewModel
    {
        public bool IsSelected { get; set; }

        public List<DyeingContentViewModel> MappingDomainModelToViewModel(List<MillsContentDomainModel> ContentsModel)
        {
            List<DyeingContentViewModel> ContentsVM = new List<DyeingContentViewModel>();
            foreach (var content in ContentsModel)
            {
                DyeingContentViewModel millsContentVM = new DyeingContentViewModel();
                millsContentVM.Name = content.Name;
                millsContentVM.Id = content.Id;
                ContentsVM.Add(millsContentVM);
            }
            return ContentsVM;
        }
    }
}
