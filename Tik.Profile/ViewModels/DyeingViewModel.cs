﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class DyeingViewModel
    {
        public string Id { get; set; }
        public DyeingYarnViewModel YarnDyeing { get; set; }
        public DyeingFabricViewModel FabricDyeing { get; set; }
        public DyeingViewModel MappingDomainModelToViewModel(DyeingDomainModel dyeingDomainModel)
        {
            DyeingViewModel dyeingViewModel = new DyeingViewModel();
            dyeingViewModel.YarnDyeing = null;

            DyeingFabricViewModel dyeingFabricViewModel = new DyeingFabricViewModel();
            dyeingViewModel.FabricDyeing = dyeingFabricViewModel.MappingDomainModelToViewModel(dyeingDomainModel.FabricDyeing);

            return dyeingViewModel;
        }
    }
}
