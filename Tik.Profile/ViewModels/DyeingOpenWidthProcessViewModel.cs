﻿using System;
using System.Collections.Generic;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class DyeingOpenWidthProcessViewModel
    {
        public string HeatSetting { get; set; }
        public List<string> AvailableSpecialFinishes { get; set; }
        public List<string> SpecialFinishes { get; set; }
        public DyeingOpenWidthDryingProcessViewModel OpenWidthDryingProcesses { get; set; }
        public List<string> AvailableOpenWidthCompactingMachines { get; set; }
        public List<string> OpenWidthCompactingMachines { get; set; }

        public DyeingOpenWidthProcessViewModel MappingDomainModelToViewModel(DyeingOpenWidthProcessDomainModel openWidthProcess)
        {
            DyeingOpenWidthProcessViewModel dyeingOpenWidthProcessViewModel = new DyeingOpenWidthProcessViewModel();

            dyeingOpenWidthProcessViewModel.AvailableSpecialFinishes = openWidthProcess.SpecialFinishes;

            DyeingOpenWidthDryingProcessViewModel dyeingOpenWidthDryingProcessViewModel = new DyeingOpenWidthDryingProcessViewModel();
            dyeingOpenWidthProcessViewModel.OpenWidthDryingProcesses = dyeingOpenWidthDryingProcessViewModel.MappingDomainModelToViewModel(openWidthProcess.OpenWidthDryingProcesses);

            dyeingOpenWidthProcessViewModel.AvailableOpenWidthCompactingMachines = openWidthProcess.OpenWidthCompactingMachines;


            return dyeingOpenWidthProcessViewModel;
        }
    }
    public class DyeingOpenWidthDryingProcessViewModel
    {
        public string StenterDryer { get; set; }
        public string RelaxDryer { get; set; }
        public List<DyeingDipStrenterViewModel> AvailableDipStenters { get; set; }
        public List<DyeingDipStrenterViewModel> DipStenters { get; set; }

        public DyeingOpenWidthDryingProcessViewModel MappingDomainModelToViewModel(DyeingOpenWidthDryingProcessDomainModel openWidthDryingProcesses)
        {
            DyeingOpenWidthDryingProcessViewModel dyeingOpenWidthDryingProcessViewModel = new DyeingOpenWidthDryingProcessViewModel();

            DyeingDipStrenterViewModel dyeingDipStrenterViewModel = new DyeingDipStrenterViewModel();
            dyeingOpenWidthDryingProcessViewModel.AvailableDipStenters = dyeingDipStrenterViewModel.MappingDomainModelToViewModel(openWidthDryingProcesses.DipStenters);

            return dyeingOpenWidthDryingProcessViewModel;
        }
    }
    public class DyeingDipStrenterViewModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsSelected { get; set; }

        public List<DyeingDipStrenterViewModel> MappingDomainModelToViewModel(List<DyeingDipStrenterDomainModel> dipStenters)
        {
            List<DyeingDipStrenterViewModel> dyeingDipStrenterViewModels = new List<DyeingDipStrenterViewModel>();
            foreach (var sf in dipStenters)
                dyeingDipStrenterViewModels.Add(new DyeingDipStrenterViewModel() { Id = sf.Id, Name = sf.Name });

            return dyeingDipStrenterViewModels;
        }
    }
}