﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class SpecialDyedViewModel : MillsCommonViewModel
    {
        public List<SpecialDyedTypesViewModel> SingleSpecialDyedTypes { get; set; }
        public List<SpecialDyedTypesViewModel> BlendSpecialDyedTypes { get; set; }
        public List<SingleContentViewModel> SingleContents { get; set; }
        public List<BlendContentViewModel> BlendContents { get; set; }
        public List<MillsContentQualityViewModel> SingleQualities { get; set; }
        public List<MillsContentQualityViewModel> BlendQualities { get; set; }
        public List<int> Counts { get; set; }
        public List<int> AvailableCounts { get; set; }
        public List<int> Deniers { get; set; }
        public List<int> AvailableDeniers { get; set; }
        public string AVGCreditTime { get; set; }
        private string _YarnFabricCategory;
        public FabricStructureViewModel FabricStructure { get; set; }
        public SpecialDyedViewModel(string YarnFabricCategory) : base(YarnFabricCategory)
        {
            _YarnFabricCategory = YarnFabricCategory;
        }

        public SpecialDyedViewModel MappingTemplateModelToViewModel(SpecialDyedTemplateModel spclDyedTemplateModel, bool isFabric)
        {
            //Mapping to ViewModel
            SpecialDyedViewModel specialDyedViewModel = new SpecialDyedViewModel(_YarnFabricCategory);
            SpecialDyedTypesViewModel singleSpecialDyedTypesViewModel = new SpecialDyedTypesViewModel();
            specialDyedViewModel.SingleSpecialDyedTypes = singleSpecialDyedTypesViewModel.MappingTemplateModelToViewModel(spclDyedTemplateModel.SingleSpecialDyedTypes);

            SpecialDyedTypesViewModel blendSpecialDyedTypesViewModel = new SpecialDyedTypesViewModel();
            specialDyedViewModel.BlendSpecialDyedTypes = blendSpecialDyedTypesViewModel.MappingTemplateModelToViewModel(spclDyedTemplateModel.BlendSpecialDyedTypes);

            SingleContentViewModel singleContentVM = new SingleContentViewModel();
            specialDyedViewModel.SingleContents = singleContentVM.MappingTemplateModelToViewModel(spclDyedTemplateModel.SingleContents);

            BlendContentViewModel blendContentVM = new BlendContentViewModel();
            specialDyedViewModel.BlendContents = blendContentVM.MappingTemplateModelToViewModel(spclDyedTemplateModel.BlendContents);

            MillsContentQualityViewModel millsContentQualityViewModel = new MillsContentQualityViewModel();
            specialDyedViewModel.BlendQualities = millsContentQualityViewModel.MappingTemplateModelToViewModel(spclDyedTemplateModel.BlendQualities);
            specialDyedViewModel.SingleQualities = millsContentQualityViewModel.MappingTemplateModelToViewModel(spclDyedTemplateModel.SingleQualities);

            specialDyedViewModel.AvailableCounts = spclDyedTemplateModel.Counts;
            specialDyedViewModel.AvailableDeniers = spclDyedTemplateModel.Deniers;

            if (isFabric)
            {
                specialDyedViewModel.FabricStructure = new FabricStructureViewModel();
                specialDyedViewModel.FabricStructure.MappingTemplateModelToViewModel(spclDyedTemplateModel.FabricStructure);
            }

            return specialDyedViewModel;
        }


    }
    public class SpecialDyedTypesViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected;

        public List<SpecialDyedTypesViewModel> MappingTemplateModelToViewModel(List<SpecialDyedYarnTypeTemplate> specialDyedYarnFabricTypesModel)
        {
            List<SpecialDyedTypesViewModel> specialDyedTypes = new List<SpecialDyedTypesViewModel>();
            foreach (var yarnType in specialDyedYarnFabricTypesModel)
            {
                SpecialDyedTypesViewModel specialDyedTypesVM = new SpecialDyedTypesViewModel();
                specialDyedTypesVM.Id = yarnType.Id;
                specialDyedTypesVM.Name = yarnType.Name;
                specialDyedTypesVM.IsSelected = false;
                specialDyedTypes.Add(specialDyedTypesVM);
            }
            return specialDyedTypes;
        }
    }
}
