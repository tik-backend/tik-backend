﻿using System;
using System.Collections.Generic;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class DyeingFabricViewModel
    {
        public DyeingMethodologyViewModel Winch { get; set; }
        public DyeingMethodologyViewModel SoftFlow { get; set; }

        public DyeingFabricViewModel MappingDomainModelToViewModel(DyeingFabricDomainModel dyeingFabric)
        {
            DyeingFabricViewModel dyeingFabricViewModel = new DyeingFabricViewModel();

            DyeingMethodologyViewModel dyeingMethodologyViewModel = new DyeingMethodologyViewModel();
            dyeingFabricViewModel.Winch = dyeingMethodologyViewModel.MappingDomainModelToViewModel(dyeingFabric.Winch);
            dyeingFabricViewModel.SoftFlow = dyeingMethodologyViewModel.MappingDomainModelToViewModel(dyeingFabric.SoftFlow);

            return dyeingFabricViewModel;
        }
    }

    public class DyeingMethodologyViewModel
    {
        public List<DyeingProcessViewModel> DyeingProcesses { get; set; }
        public List<DyeingContentViewModel> AvailableSingleContents { get; set; }
        public List<DyeingContentViewModel> AvailableBlendContents { get; set; }
        public List<FabricTypesViewModel> AvailableSingleJerseys { get; set; }
        public List<FabricTypesViewModel> AvailableInterlocks { get; set; }
        public List<FabricTypesViewModel> AvailableRibs { get; set; }
        public List<string> AvailableVesselSizes { get; set; }
        public List<DyeingContentViewModel> SingleContents { get; set; }
        public List<DyeingContentViewModel> BlendContents { get; set; }
        public List<FabricTypesViewModel> SingleJersey { get; set; }
        public List<FabricTypesViewModel> Interlock { get; set; }
        public List<FabricTypesViewModel> Rib { get; set; }
        public List<string> VesselSizes { get; set; }
        public string LycraBlend { get; set; }
        public DyeingTubularProcessViewModel TubularProcess { get; set; }
        public DyeingOpenWidthProcessViewModel OpenWidthProcess { get; set; }

        public DyeingMethodologyViewModel MappingDomainModelToViewModel(DyeingMethodologyDomianModel dyeingMethodology)
        {
            DyeingMethodologyViewModel dyeingMethodologyViewModel = new DyeingMethodologyViewModel();

            DyeingProcessViewModel dyeingProcessViewModel = new DyeingProcessViewModel();
            dyeingMethodologyViewModel.DyeingProcesses = dyeingProcessViewModel.MappingDomainModelToViewModel(dyeingMethodology.DyeingProcesses);

            DyeingContentViewModel dyeingContentViewModel = new DyeingContentViewModel();
            dyeingMethodologyViewModel.AvailableSingleContents = dyeingContentViewModel.MappingDomainModelToViewModel(dyeingMethodology.SingleContents);
            dyeingMethodologyViewModel.AvailableBlendContents = dyeingContentViewModel.MappingDomainModelToViewModel(dyeingMethodology.BlendContents);

            FabricTypesViewModel fabricTypesViewModel = new FabricTypesViewModel();
            dyeingMethodologyViewModel.AvailableSingleJerseys = fabricTypesViewModel.MappingDomainModelToViewModel(dyeingMethodology.SingleJersey);
            dyeingMethodologyViewModel.AvailableRibs = fabricTypesViewModel.MappingDomainModelToViewModel(dyeingMethodology.Rib);
            dyeingMethodologyViewModel.AvailableInterlocks = fabricTypesViewModel.MappingDomainModelToViewModel(dyeingMethodology.Interlock);

            dyeingMethodologyViewModel.AvailableVesselSizes = dyeingMethodology.VesselSizes;

            DyeingTubularProcessViewModel dyeingTubularProcessViewModel = new DyeingTubularProcessViewModel();
            dyeingMethodologyViewModel.TubularProcess = dyeingTubularProcessViewModel.MappingDomainModelToViewModel(dyeingMethodology.TubularProcess);

            DyeingOpenWidthProcessViewModel dyeingOpenWidthProcessViewModel = new DyeingOpenWidthProcessViewModel();
            dyeingMethodologyViewModel.OpenWidthProcess = dyeingOpenWidthProcessViewModel.MappingDomainModelToViewModel(dyeingMethodology.OpenWidthProcess);

            return dyeingMethodologyViewModel;
        }
    }

    public class DyeingProcessViewModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsSelected { get; set; }

        public List<DyeingProcessViewModel> MappingDomainModelToViewModel(List<DyeingProcessDomainModel> dyeingProcesses)
        {
            List<DyeingProcessViewModel> dyeingProcessViewModels = new List<DyeingProcessViewModel>();
            foreach(var dp in dyeingProcesses)
            {
                dyeingProcessViewModels.Add(new DyeingProcessViewModel { Id = dp.Id, Name = dp.Name });
            }
            return dyeingProcessViewModels;
        }
    }

   
}