﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProfileModels;

namespace Tik.Profile.ViewModels
{
    public class YarnViewModel
    {
        public RegularViewModel RegularYarn { get; set; }
        public MelangeSlubParentViewModel MelangeSlubYarn { get; set; }
        public SpecialDyedViewModel SpecialYarn { get; set; }
        public SpecialDyedViewModel DyedYarn { get; set; }
    }
}
