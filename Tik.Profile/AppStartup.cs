﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Tik.BusinessService;
using Tik.BusinessService.Interfaces;
using Tik.Repository;
using Tik.Repository.Profiles.Implementations;
using Tik.Repository.Profiles.Interfaces;
using Tik.Repository.CommonRepository;
using Tik.Repository.ProjectRepository;
using IMillsRepository = Tik.Repository.Profiles.Interfaces.IMillsRepository;
using MillsRepository = Tik.Repository.Profiles.Implementations.MillsRepository;
using Tik.Repository.CertificationRepository;

[assembly: FunctionsStartup(typeof(Tik.Profile.AppStartup))]
namespace Tik.Profile
{
    public class AppStartup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            services.AddScoped<IUserProfileRepository, UserProfileRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
            services.AddScoped<IBiddingAuditRepository, BiddingAuditRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ICertificationRepository, CertificationRepository>();
            services.AddScoped<IMillsContentRepository, MillsContentRepository>();

            services.AddScoped<IMillsContentQualityRepository, MillsContentQualityRepository>();

            services.AddScoped<IMillsMelangeSlubRepository, MillsMelangeSlubRepository>();

            services.AddScoped<ISpecialDyedRepository, SpecialDyedRepository>();

            services.AddScoped<IMillsRepository, MillsRepository>();
            services.AddScoped<IFabricStructureRepository, FabricStructureRepository>();

            services.AddScoped<ICompactingMachinesRepository, CompactingMachinesRepository>();
            services.AddScoped<IDyeingSpecialFinishesRepository, DyeingSpecialFinishesRepository>();
            services.AddScoped<IDyeingRepository, DyeingRepository>();
            services.AddScoped<IDyeingProcessesRepository, DyeingProcessesRepository>();
            services.AddScoped<IDyeingDryingProcessesRepository, DyeingDryingProcessessRepository>();
            services.AddScoped<IDyeingDipStrentnerRepository, DyeingDipStrentnerRepository>();
            services.AddScoped<IDyeingVesselsRepository, DyeingVesselsRepository>();

            string conStr = System.Environment.GetEnvironmentVariable($"ConnectionStrings_Tik", EnvironmentVariableTarget.Process);
            string databaseName = System.Environment.GetEnvironmentVariable($"DatabaseName", EnvironmentVariableTarget.Process);
            string blobConStr = System.Environment.GetEnvironmentVariable($"AzureWebJobsStorage", EnvironmentVariableTarget.Process);

            services.AddCosmosDb(conStr, databaseName, blobConStr);
        }
    }
}
