using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using Tik.DomainModels;
using System.Linq;
using Tik.DomainModels.ViewModels;
using Tik.BusinessService;
using System.Net.Http;
using System.Net;
using Tik.BusinessService.Interfaces;
using Tik.Repository.CommonRepository;
using System.Collections.Generic;
using Tik.DomainModels.Validators;
using Tik.DomainModels.ProjectDomainModel;
using Tik.Repository.ProjectRepository;
using Tik.BusinessService.Helper;
using Tik.DomainModels.Enum;
using Newtonsoft.Json;

namespace Tik.Profile
{
    public class ProfileFunc
    {
        private readonly IUserProfileRepository _profileRepo;
        private readonly IUserRepository _userRepo;
        private readonly IAuthenticationService _authentication;
        private readonly ICompanyRepository _companyRepo;
        private readonly IReviewRepository _reviewRepo;
        private readonly IProjectRepository _projectRepo;
        private readonly ICertificationRepository _certificationRepo;

        public ProfileFunc(IUserProfileRepository profileRepo, IUserRepository userRepo, IAuthenticationService authentication, ICompanyRepository companyRepo, IReviewRepository reviewRepo, IProjectRepository projectRepo, ICertificationRepository certificationRepo)
        {
            this._profileRepo = profileRepo ?? throw new ArgumentNullException(nameof(_profileRepo));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(_userRepo));
            this._authentication = authentication ?? throw new ArgumentNullException(nameof(_authentication));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(_companyRepo));
            this._reviewRepo = reviewRepo ?? throw new ArgumentNullException(nameof(_reviewRepo));
            this._projectRepo = projectRepo ?? throw new ArgumentNullException(nameof(projectRepo));
            this._certificationRepo = certificationRepo ?? throw new ArgumentNullException(nameof(certificationRepo));
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("EditProfile")]
        public async Task<IActionResult> EditProfile(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _authentication.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _authentication.ValidateAuthenticationToken(request);
                CompanyEntity companyEntity = await _companyRepo.GetItemById(accessToken.CompanyId);
                if (companyEntity == null)
                    return new BadRequestObjectResult("Company does not exists.");

                var formCollection = await req.ReadFormAsync();
                IFormFile file = null;
                if (formCollection.Files.Count > 0)
                    file = formCollection.Files.First();

                string id = formCollection["id"];
                if (string.IsNullOrEmpty(id))
                    return new BadRequestObjectResult("User Id is required.");
                UserEntity user = await _userRepo.GetItemById(id);
                if (user == null)
                    return new BadRequestObjectResult("User not found.");

                companyEntity.UnitName = formCollection["unitName"];
                companyEntity.Title = formCollection["title"];
                companyEntity.Description = formCollection["description"];
                //user.EmailAddress = formCollection["emailAddress"];                
                companyEntity.Id = user.CompanyId;
                var Certifications = Convert.ToString(formCollection["certification"]);
                IList<CertificationEntity> certificationEntities = await _certificationRepo.GetCertificates();
                companyEntity.Certification = Certifications != null ? certificationEntities.Where(x=> Certifications.ToLower().Split(',').Contains(x.CertificateName.ToLower())).Select(x=>x.CertificateId).ToArray() : null;

                if (string.IsNullOrEmpty(companyEntity.UnitName))
                    return new BadRequestObjectResult("Unit Name is required.");
                if (string.IsNullOrEmpty(companyEntity.Title))
                    return new BadRequestObjectResult("Title is required.");
                if (string.IsNullOrEmpty(companyEntity.Description))
                    return new BadRequestObjectResult("Description is required.");
                //if (string.IsNullOrEmpty(user.EmailAddress))
                //    return new BadRequestObjectResult("Email Id is required.");
                if (companyEntity.Id != user.CompanyId)
                    return new BadRequestObjectResult("User does not belongs to the respective Company.");
                if (companyEntity.Certification == null || companyEntity.Certification.Length == 0)
                    return new BadRequestObjectResult("Certification is required.");               

                user.PhoneNumber = formCollection["phoneNumber1"];
                user.AddressDetails = new AddressDetails()
                {
                    Address = formCollection["address"],
                    PhoneNumber2 = formCollection["phoneNumber2"]
                };
                if (string.IsNullOrEmpty(user.PhoneNumber))
                    return new BadRequestObjectResult("Phone Number-1 is required.");
                if (string.IsNullOrEmpty(user.AddressDetails.Address))
                    return new BadRequestObjectResult("AddressName is required.");
                if (string.IsNullOrEmpty(user.AddressDetails.PhoneNumber2))
                    return new BadRequestObjectResult("Phone Number-2 is required.");

                bool isSuccess = await _profileRepo.RegisterUserProfile(id, companyEntity, user, file);
                if (!isSuccess)
                {
                    return new BadRequestObjectResult("User Profile not found");
                }
                else
                {
                    await _profileRepo.EditUserAddress(user.Id, user);
                    return new OkObjectResult("User Profile saved succesfully...");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("LoadProfile")]
        public async Task<IActionResult> LoadProfile(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _authentication.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                string id = req.Query["id"];
                var user = await _userRepo.GetItemById(id);
                if (user == null)
                    return new BadRequestObjectResult("User Profile not found");

                var userProfile = await _profileRepo.GetItemById(user.CompanyId);
                var userProfileAdrs = await _userRepo.GetItemById(id);

                IList<CertificationEntity> certificationEntities = await _certificationRepo.GetCertificates();
                UserProfileViewModel userProfileViewModel = new UserProfileViewModel();
                userProfileViewModel.Id = id;
                if (userProfile != null)
                {
                    if (userProfile.ImageName != null)
                    {
                        byte[] imgBytes = _profileRepo.GetImageBytes(userProfile.ImageName);
                        userProfileViewModel.ImageName = userProfile.ImageName;
                        userProfileViewModel.Image = imgBytes;
                    }

                    userProfileViewModel.Id = userProfile.Id;
                    userProfileViewModel.UnitName = userProfile.UnitName;
                    userProfileViewModel.Title = userProfile.Title;
                    userProfileViewModel.Description = userProfile.Description;
                    userProfileViewModel.EmailAddress = user.EmailAddress;
                    userProfileViewModel.Review = userProfile.Review;
                    userProfileViewModel.Certification = userProfile.Certification == null ? null : certificationEntities.Where(x => userProfile.Certification.Contains(x.CertificateId)).Select(x => x.CertificateName).ToArray();
                }
                if (userProfileAdrs != null)
                {
                    userProfileViewModel.Address = userProfileAdrs.AddressDetails.Address;
                    userProfileViewModel.PhoneNumber1 = userProfileAdrs.PhoneNumber;
                    userProfileViewModel.PhoneNumber2 = userProfileAdrs.AddressDetails.PhoneNumber2;
                }
                return new JsonResult(userProfileViewModel);
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin")]
        [FunctionName("Review")]
        public async Task<IActionResult> Review(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            { 
                AuthenticationStatus status = _authentication.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }
            

                var accessToken = _authentication.ValidateAuthenticationToken(request);
                CompanyEntity companyEntity = await _companyRepo.GetItemById(accessToken.CompanyId);
                if (companyEntity == null)
                    return new BadRequestObjectResult("Company does not exists.");

                var user = await _userRepo.GetUserByCompanyId(companyEntity.Id);
                if (user == null)
                    return new BadRequestObjectResult("User not found.");

                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);
                string jsonContent = await request.Content.ReadAsStringAsync();
                var reviewEntity = JsonConvert.DeserializeObject<ReviewEntity>(jsonContent);
                reviewEntity.isCompany = isCompany;
                var review = await DomainModels.HttpRequestExtensions.ValidateRequest<ReviewEntity, ReviewValidator>(reviewEntity);
                if (!review.IsValid)
                {
                    return review.ToBadRequest();
                }                             

                ProjectEntity projectConfirmed = await _projectRepo.GetBidById(review.Value.ProjectId);
                if (projectConfirmed == null)
                {
                    return new BadRequestObjectResult("Select only confirmed project.");
                }
                Bids bidConfirmed = projectConfirmed.bids.Where(x => x.Id == review.Value.BiddingId && x.bidStatus == BidStatus.Confirmed).FirstOrDefault();
                if (bidConfirmed == null)
                {
                    return new BadRequestObjectResult("User allowed to review only for the confirmed projects.");
                }

                ReviewEntity reviewCount = await _reviewRepo.GetReviewStatusById(review.Value.ProjectId, isCompany);
                if (reviewCount != null)
                    return new BadRequestObjectResult("This project can be reviewed only once.");

                if (isCompany)
                    review.Value.ReviewRating = (review.Value.TimelyDeliveryRating.Value + review.Value.ProductQualityRating.Value) / 2;
                else
                    review.Value.ReviewRating = (review.Value.PaymentRating.Value + review.Value.InputMaterialRating.Value) / 2;

                string companyId = isCompany ? bidConfirmed.companyId : projectConfirmed.companyId;
                bool isSuccess = await _reviewRepo.ReviewRating(companyId, projectConfirmed, bidConfirmed, user, review.Value, isCompany);
                if (!isSuccess)
                {
                    return new BadRequestObjectResult("Review Ratings not updated.");
                }
                else
                {
                    List<ReviewEntity> totalTransactions = await _reviewRepo.GetReviewAverage(companyId);
                    var average = _profileRepo.GetRatingAverage(totalTransactions);
                    await _profileRepo.InsertRatingAverage(companyId, average);

                    int reviewCounts = await _reviewRepo.CheckReviewStatus(review.Value.ProjectId);
                    if (reviewCounts == 2)
                    {
                        projectConfirmed.stages = ProjectStages.Completed;
                        bidConfirmed.bidStatus = BidStatus.Completed;
                        await _projectRepo.UpdateItemAsync(projectConfirmed.Id, projectConfirmed);
                    }
                }
                return new OkObjectResult("Review Ratings saved succesfully.");
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}