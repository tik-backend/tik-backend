using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.BusinessService;
using Tik.Dashboard.ViewModel;
using Tik.DomainModels;
using Tik.DomainModels.ProjectDomainModel;
using Tik.Repository;
using Tik.Repository.ProjectRepository;

namespace Tik.Dashboard
{
    public class SearchProjectFunc
    {
        private readonly IMillsRepository _repo;
        private readonly ICompanyRepository _companyRepo;
        private readonly ICertificationRepository _certificationRepo;
        public SearchProjectFunc(IMillsRepository repo, ICompanyRepository companyRepo, ICertificationRepository certificationRepo)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
            this._certificationRepo = certificationRepo ?? throw new ArgumentNullException(nameof(certificationRepo));
        }

        [FunctionAuthorize("MasterAdmin", "Quote InCharge")]
        [FunctionName("SearchProject")]
        public async Task<IActionResult> SearchProject(
            [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req,
          ILogger log)
        {
            try
            {
                string jsonContent = await req.Content.ReadAsStringAsync();
                var project = JsonConvert.DeserializeObject<ProjectEntity>(jsonContent);
                IEnumerable<MillsEntity> data = await _repo.SearchProject(project.controls);
                IList<MillsEntity> filterdData = data.Where(x => x.CompanyId != null).GroupBy(x => x.CompanyId).Select(x => x.FirstOrDefault()).ToList();
                string[] CompanyIds = filterdData.Select(x => x.CompanyId).ToArray();
                IEnumerable<CompanyEntity> companys = await _companyRepo.GetItemListByIds(CompanyIds);
                List<ProjectSearchViewModel> result = new List<ProjectSearchViewModel>();
                IList<CertificationEntity> certificationEntities = await _certificationRepo.GetCertificates();
                foreach (MillsEntity entity in filterdData)
                {
                    var Certification = companys.Where(x => x.Id == entity.CompanyId).FirstOrDefault()?.Certification;
                    result.Add(new ProjectSearchViewModel
                    {
                        CompanyId = entity.CompanyId,
                        MillName = companys.Where(x => x.Id == entity.CompanyId).FirstOrDefault()?.UnitName,
                        CreditPeriod = entity.AVGCreditTime,
                        Review = Convert.ToString(companys.Where(x => x.Id == entity.CompanyId).FirstOrDefault()?.Review),
                        Certification = Certification != null? certificationEntities.Where(x => Certification.Contains(x.CertificateId)).Select(x => x.CertificateName).ToArray(): null
                    });
                }
                return new JsonResult(result);

            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving search result {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }
    }
}
