using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.BusinessService;
using Tik.BusinessService.Helper;
using Tik.BusinessService.Interfaces;
using Tik.Dashboard.ViewModel;
using Tik.DomainModels;
using Tik.DomainModels.Enum;
using Tik.DomainModels.Filter;
using Tik.DomainModels.ProjectDomainModel;
using Tik.DomainModels.ViewModels;
using Tik.Repository;
using Tik.Repository.CommonRepository;
using Tik.Repository.ProjectRepository;

namespace Tik.Dashboard
{
    public class CreateProjectFunc
    {
        private readonly IProjectRepository _projectRepo;
        private readonly IAuthenticationService _tokenIssuer;
        private readonly ICompanyRepository _companyRepo;
        private readonly IUserRepository _userRepo;
        private readonly IBiddingAuditRepository _bidRepo;
        private readonly IUserProfileRepository _userProfileRepo;
        private readonly IReviewRepository _reviewRepo;

        public CreateProjectFunc(IAuthenticationService tokenIssuer, ICompanyRepository companyRepo, IProjectRepository projectRepo, IUserRepository userRepo, IBiddingAuditRepository bidRepo, IUserProfileRepository userProfileRepo, IReviewRepository reviewRepo)
        {
            this._projectRepo = projectRepo ?? throw new ArgumentNullException(nameof(projectRepo));
            this._tokenIssuer = tokenIssuer ?? throw new ArgumentNullException(nameof(tokenIssuer));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
            this._bidRepo = bidRepo ?? throw new ArgumentNullException(nameof(bidRepo));
            this._userProfileRepo = userProfileRepo ?? throw new ArgumentNullException(nameof(userProfileRepo));
            this._reviewRepo = reviewRepo ?? throw new ArgumentNullException(nameof(_reviewRepo));
        }

        [FunctionName("FilterRejectedBids")]
        [FunctionAuthorize]
        public async Task<IActionResult> FilterRejectedBids(
           [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req,
         ILogger log)
        {

            AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
            if (status.Code != HttpStatusCode.Accepted)
            {
                var result = new ObjectResult(status.Message);
                result.StatusCode = Convert.ToInt32(status.Code);
                return result;
            }

            var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
            string jsonContent = await req.Content.ReadAsStringAsync();
            var filter = JsonConvert.DeserializeObject<BidFilter>(jsonContent);
            filter.companyId = accessToken.CompanyId;
            bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);
            if (!isCompany)
                return new BadRequestObjectResult("Access denied");

            List<Bids> bidList = await _projectRepo.GetBidListByFilter(filter);

            if (bidList.Count <= 0)
                return new JsonResult(bidList);

            string[] companyIds = bidList.Select(x => x.companyId).ToArray();
            if (filter.review != null)
            {
                IEnumerable<CompanyEntity> companyList = await _companyRepo.GetItemListByIds(companyIds);
                companyIds = companyList.Where(x => x.Review >= filter.review).Select(x => x.Id).ToArray();
                bidList = bidList.Where(x => companyIds.Contains(x.companyId)).ToList();
            }

            if (bidList.Count <= 0)
                return new JsonResult(bidList);

            IEnumerable<UserEntity> userList = await _userRepo.GetUserListsByCompanyIds(companyIds); IEnumerable<CompanyEntity> bidCompanys = null;

            var bidAuditList = await _bidRepo.GetBidAudit(filter.projectId);
            bidAuditList = bidAuditList.Where(x => x.isCompany == false).ToList();
            bidCompanys = await _companyRepo.GetItemListByIds(companyIds);

            List<BidsViewModel> bidListViewModel = new List<BidsViewModel>();
            ProjectEntity entity = await _projectRepo.GetItemById(filter.projectId);
            if (entity.stages != ProjectStages.Confirmed && entity.stages != ProjectStages.Completed)
                bidList = bidList.Where(x => x.bidStatus == BidStatus.Approved || x.bidStatus == BidStatus.Selected || x.bidStatus == BidStatus.BidApproved || x.bidStatus == BidStatus.AwaitingConfirmation).OrderBy(x => x.CreatedDate).ToList();
            else
                bidList = bidList.Where(x => x.bidStatus == BidStatus.Rejected).OrderBy(x => x.CreatedDate).ToList();

            string submittedDate = "", bidSubmitterId = ""; ;
            foreach (Bids bid in bidList.OrderByDescending(x=>(int)(x.bidStatus)).ToList())
            {
                if (bid.bidStatus == BidStatus.Authenticated)
                {
                    bidSubmitterId = bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Authenticated).Select(x => x.userId).FirstOrDefault();
                    submittedDate = TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Authenticated).Select(x => x.CreatedDate).FirstOrDefault());
                }
                else
                {
                    bidSubmitterId = bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.userId).FirstOrDefault();
                    submittedDate = TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.CreatedDate).FirstOrDefault());
                }

                CompanyEntity company = bidCompanys.Where(x => x.Id == bid.companyId).FirstOrDefault();
                if (company == null || string.IsNullOrEmpty(bid.price))
                    continue;

                byte[] imgBytes = new byte[0];
                if (company.ImageName != null)
                {
                    imgBytes = _userProfileRepo.GetImageBytes(company.ImageName);
                }

                bool isPriceDecimal = Decimal.TryParse(bid.price, out decimal price);

                bidListViewModel.Add(new BidsViewModel()
                {
                    Id = bid.Id,
                    companyId = bid.companyId,
                    CompanyName = company.UnitName,
                    Logo = imgBytes,
                    Review = company.Review,
                    RequestedPrice = price,
                    RequestedCredit = bid.creditPeriod,
                    RequestedDelivery = bid.deliveryPeriod,
                    Status = bid.bidStatus.ToString(),
                    Message = bid.Message,
                    SubmittedBy = bidSubmitterId != null ? userList.Where(x => x.Id == bidSubmitterId).FirstOrDefault()?.FullName : "",
                    SubmittedDate = submittedDate
                });               
            }
            if (filter.maxPrice > 0)
                bidListViewModel = bidListViewModel.Where(x => x.RequestedPrice >= filter.minPrice && x.RequestedPrice <= filter.maxPrice).ToList();


            bidListViewModel = bidListViewModel.OrderBy(x => x.RequestedPrice).ToList();

            return new JsonResult(bidListViewModel);
        }

        [FunctionAuthorize]
        [FunctionName("GetUserFullName")]
        public async Task<IActionResult> GetUserFullName(
            [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(request);
                var user = await _userRepo.GetItemById(accessToken.Id);

                if (user != null)
                {
                    UserFullNameViewModel userViewModel = new UserFullNameViewModel
                    {
                        FullName = user.FullName,
                        CurrentDate = DateTime.UtcNow.Date.ToString("dd-MM-yyyy"),
                        CurrentDay = DateTime.Now.ToString("dddd")
                    };
                    return new JsonResult(userViewModel);
                }
                else
                {
                    return new BadRequestObjectResult("User detail is not available.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("GetConfirmedBidDetails")]
        public async Task<IActionResult> GetConfirmedBidDetails(
            [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                string projectId = req.Query["projectId"];
                if (string.IsNullOrEmpty(projectId))
                {
                    return new BadRequestObjectResult("ProjectId is required");
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(request);
                ProjectEntity projectConfirmed = await _projectRepo.GetBidById(projectId);
                if (projectConfirmed == null)
                {
                    return new BadRequestObjectResult("Select only completed or confirmed projects.");
                }

                Bids bidConfirmed = projectConfirmed.bids.Where(x => (x.companyId != accessToken.CompanyId &&( x.bidStatus == BidStatus.Completed || x.bidStatus == BidStatus.Confirmed))).FirstOrDefault();

                if (bidConfirmed != null)
                {
                    CompanyEntity company = await _companyRepo.GetCompanyById(bidConfirmed.companyId);

                    ConfirmedBiddingViewModel confirmedBiddingViewModel = new ConfirmedBiddingViewModel
                    {
                        Price = bidConfirmed.price,
                        CreditPeriod = bidConfirmed.creditPeriod,
                        DeliveryPeriod = bidConfirmed.deliveryPeriod,
                        CompanyReview = company.Review
                    };
                    return new JsonResult(confirmedBiddingViewModel);
                }
                else
                {
                    return new NoContentResult();
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
