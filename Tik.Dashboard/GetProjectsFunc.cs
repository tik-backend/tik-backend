using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Tik.BusinessService;
using Tik.BusinessService.Helper;
using Tik.BusinessService.Interfaces;
using Tik.Dashboard.ViewModel;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;
using Tik.DomainModels.Filter;
using Tik.DomainModels.ProjectDomainModel;
using Tik.Repository;
using Tik.Repository.ProjectRepository;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Tik.DomainModels.Enum;
using Tik.Repository.CommonRepository;
using Microsoft.AspNetCore.Http;

namespace Tik.Dashboard
{
    public class GetProjectsFunc
    {
        private readonly IProjectRepository _projectRepo;
        private readonly IAuthenticationService _tokenIssuer;
        private readonly ICompanyRepository _companyRepo;
        private readonly IUserRepository _userRepo;
        private readonly IReviewRepository _reviewRepo;
        private readonly IBiddingAuditRepository _bidRepo;
        private readonly IUserProfileRepository _userProfileRepo;

        private readonly int MIN_BID_REQUIRED;
        private readonly int MIN_TIME_TO_RESPOND;
        private readonly int MAX_TIME_TO_RESPOND;

        public GetProjectsFunc(IAuthenticationService tokenIssuer, ICompanyRepository companyRepo, IProjectRepository projectRepo, IUserRepository userRepo, IReviewRepository reviewRepo, IBiddingAuditRepository bidRepo, IUserProfileRepository userProfileRepo)
        {
            this._projectRepo = projectRepo ?? throw new ArgumentNullException(nameof(projectRepo));
            this._tokenIssuer = tokenIssuer ?? throw new ArgumentNullException(nameof(tokenIssuer));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
            this._reviewRepo = reviewRepo ?? throw new ArgumentNullException(nameof(reviewRepo));
            this._bidRepo = bidRepo ?? throw new ArgumentNullException(nameof(bidRepo));
            this._userProfileRepo = userProfileRepo ?? throw new ArgumentNullException(nameof(userProfileRepo));

            MIN_BID_REQUIRED = Convert.ToInt32(System.Environment.GetEnvironmentVariable($"MIN_BID_REQUIRED", EnvironmentVariableTarget.Process));
            MIN_TIME_TO_RESPOND = Convert.ToInt32(System.Environment.GetEnvironmentVariable($"MIN_TIME_TO_RESPOND", EnvironmentVariableTarget.Process));
            MAX_TIME_TO_RESPOND = Convert.ToInt32(System.Environment.GetEnvironmentVariable($"MAX_TIME_TO_RESPOND", EnvironmentVariableTarget.Process));
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("Dashboard")]
        public async Task<IActionResult> GetProjectList(
            [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage req,
            ILogger log)
        {

            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                List<ProjectEntity> list = null;
                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);
                list = await _projectRepo.ListProject(accessToken.CompanyId, isCompany);

                List<ProjectDashboardViewModel> dashboard = await GetDashboardList(list, isCompany, accessToken.CompanyId, accessToken);

                return new JsonResult(dashboard);
            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving project list {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("GetProjectListByFilter")]
        public async Task<IActionResult> GetProjectListByFilter(
           [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req,
           ILogger log)
        {

            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                string jsonContent = await req.Content.ReadAsStringAsync();
                var projectFilter = JsonConvert.DeserializeObject<ProjectFilter>(jsonContent);
                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                projectFilter.isCompany = Common.CheckIsCompanyUser(accessToken.Category);


                List<ProjectEntity> list = null;
                projectFilter.companyId = accessToken.CompanyId;
                List<string> creatorList = new List<string>();
                if (projectFilter.myproject == true)
                {
                    AssignRole role = AssignRole.QI;
                    if (accessToken.Role == AssignRole.QI.Meaning)
                    {
                        role = AssignRole.QI;
                    }
                    else if (accessToken.Role == AssignRole.M.Meaning)
                    {
                        role = AssignRole.M;
                    }
                    list = await _projectRepo.GetProjectListByCreator(accessToken.CompanyId, role, projectFilter.isCompany, accessToken.Id);

                    if (projectFilter.isCompany)
                        list = list.Where(x => projectFilter.Status == null || projectFilter.Status.Contains((int)x.stages)).ToList();
                    else
                        list = list.Where(x => projectFilter.Status == null || x.bids.Any(b => b.companyId == projectFilter.companyId && projectFilter.Status.Contains((int)b.bidStatus))).ToList();
                }
                else
                    list = await _projectRepo.GetFilteredProjectList(projectFilter);


                if(projectFilter.stages == ProjectStages.NoResponse.ToString())
                {
                    list = list.Where(x => x.confirmationDate.Date < DateTime.Now.Date).ToList();
                }

                List<ProjectDashboardViewModel> dashboard = await GetDashboardList(list, projectFilter.isCompany, accessToken.CompanyId, accessToken);
                return new JsonResult(dashboard);
            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving filtered project list {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }

        [FunctionName("GetMyProjectList")]
        [FunctionAuthorize]
        public async Task<IActionResult> GetMyProjectList(
           [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage req,
           ILogger log)
        {

            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                List<ProjectEntity> list = null;
                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);

                List<string> creatorList = new List<string>();
                AssignRole role = AssignRole.QI;
                if (accessToken.Role == AssignRole.QI.Meaning)
                {
                    role = AssignRole.QI;
                }
                else if (accessToken.Role == AssignRole.M.Meaning)
                {
                    role = AssignRole.M;
                }

                list = await _projectRepo.GetProjectListByCreator(accessToken.CompanyId, role, isCompany, accessToken.Id);

                List<ProjectDashboardViewModel> dashboard = await GetDashboardList(list, isCompany, accessToken.CompanyId, accessToken);

                return new JsonResult(dashboard);
            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving project list {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }


        private async Task<List<ProjectDashboardViewModel>> GetDashboardList(List<ProjectEntity> list, bool isCompany, string companyId, AuthToken accessToken)
        {
            await UpdateProjectStatus(list, isCompany, accessToken);
            List<ProjectDashboardViewModel> dashboard = new List<ProjectDashboardViewModel>();
            dashboard = list.ConvertAll<ProjectDashboardViewModel>(x => x);
            List<string> CompanyIds = new List<string>();
            IEnumerable<UserEntity> users = null;
            if (isCompany)
                CompanyIds = list.Select(x => x.companyId).Distinct().ToList();
            else
                CompanyIds.Add(companyId);

            users = await _userRepo.GetUserListsByCompanyIds(CompanyIds.ToArray());
            foreach (ProjectDashboardViewModel data in dashboard)
            {
                if (!isCompany)
                {
                    var val = list.Where(x => x.Id == data.Id).SelectMany(x => x.bids).Where(x => x.companyId == companyId).FirstOrDefault();
                    if (val != null)
                    {
                        if (val.bidStatus == BidStatus.Selected || val.bidStatus == BidStatus.BidApproved || val.bidStatus == BidStatus.Approved || val.bidStatus == BidStatus.AwaitingConfirmation)
                            val.bidStatus = BidStatus.AwaitingConfirmation;

                        data.InStages = val.bidStatus.GetAttribute<DisplayAttribute>().Name;
                        data.ApprovalAdmin = val.ApprovalAdminId == null ? "" : users.Where(x => x.Id == val.ApprovalAdminId).FirstOrDefault()?.FullName;
                    }
                }
                else
                {
                    data.Merchandiser = data.MerchandiserId == null ? "" : users.Where(x => x.Id == data.MerchandiserId).FirstOrDefault()?.FullName;
                }
            }
            return dashboard;
        }

        private string GetProjectStatus(DateTime updatedDate, CompanyEntity company)
        {
            double min_time_response = 0, max_time_response = 0;

            min_time_response = company.MinimumTimeToRespond.HasValue ? company.MinimumTimeToRespond.Value : MIN_TIME_TO_RESPOND;
            max_time_response = company.MaximumTimeToRespond.HasValue ? company.MaximumTimeToRespond.Value : MAX_TIME_TO_RESPOND;

            bool projectDelay_Orange = DateTime.Now > updatedDate.AddHours(min_time_response);
            bool projectDelayed_Red = DateTime.Now > updatedDate.AddHours(max_time_response);
            if (projectDelayed_Red)
                return ProjectStatus.Red.ToString();
            else if (projectDelay_Orange)
                return ProjectStatus.Orange.ToString();
            else
                return ProjectStatus.Green.ToString();
        }

        private async Task UpdateProjectStatus(List<ProjectEntity> list, bool isCompany, AuthToken accessToken)
        {
            CompanyEntity company = await _companyRepo.GetCompanyById(accessToken.CompanyId);
            int min_bid_required = 0;
            min_bid_required = company.MinimumBidsRequired.HasValue ? company.MinimumBidsRequired.Value : MIN_BID_REQUIRED;
            int totalBidsReceived = 0, totalBidSelected = 0;
            DateTime updateDate;
            foreach (ProjectEntity proj in list)
            {
                updateDate = proj.UpdatedDate.HasValue ? proj.UpdatedDate.Value : proj.CreatedDate;
                if (isCompany)
                {
                    proj.status = ProjectStatus.Green.ToString();
                    if (proj.stages == ProjectStages.BidReceived) // In Quote Incharge
                    {
                        totalBidsReceived = proj.bids.Where(x => (x.bidStatus == BidStatus.Approved)).Count();
                        if (totalBidsReceived >= min_bid_required)
                        {
                            proj.status = GetProjectStatus(updateDate, company);
                        }
                    }
                    else if (proj.stages == ProjectStages.BidSelected) // In Merchandiser
                    {
                        totalBidSelected = proj.bids.Where(x => x.bidStatus == BidStatus.Selected).Count();
                        if (totalBidSelected >= 1)
                        {
                            proj.status = GetProjectStatus(updateDate, company);
                        }
                    }
                    else if (proj.stages == ProjectStages.BidAuthenticated) // In Approval Admin
                    {
                        totalBidSelected = proj.bids.Where(x => x.bidStatus == BidStatus.BidApproved).Count();
                        if (totalBidSelected >= 1)
                        {
                            proj.status = GetProjectStatus(updateDate, company);
                        }
                    }
                }
                else
                {
                    proj.status = ProjectStatus.Green.ToString();
                    Bids bid = proj.bids.Where(x => x.companyId == accessToken.CompanyId).FirstOrDefault();
                    if (bid != null)
                    {
                        updateDate = bid.UpdatedDate.HasValue ? bid.UpdatedDate.Value : bid.CreatedDate;
                        if (bid.bidStatus == BidStatus.Initiated)
                        {
                            proj.status = GetProjectStatus(updateDate, company);
                        }
                        else if (bid.bidStatus == BidStatus.Authenticated)
                        {
                            proj.status = GetProjectStatus(updateDate, company);
                        }
                    }
                }
            }

        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("PendingReview")]
        public async Task<IActionResult> GetPendingReviewProjectList(
          [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage req,
          ILogger log)
        {

            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }
                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);
                ProjectFilter projectFilter = new ProjectFilter();
                projectFilter.companyId = accessToken.CompanyId;
                projectFilter.isCompany = isCompany;
                projectFilter.stages = "ConfirmedProjects";
                List<ProjectEntity> list = await _projectRepo.GetFilteredProjectList(projectFilter);
                string[] projectId = list.Select(x => x.Id).ToArray();
                List<ReviewEntity> reviewList = await _reviewRepo.GetReviewStatusByProjectIds(projectId, isCompany);
                foreach (ProjectEntity project in list.ToList())
                {
                    ReviewEntity reviewCount = reviewList.Where(x => x.ProjectId == project.Id).FirstOrDefault();
                    if (reviewCount != null)
                    {
                        list.Remove(project);
                    }
                }

                List<ProjectDashboardViewModel> dashboard = await GetDashboardList(list, isCompany, accessToken.CompanyId, accessToken);

                return new JsonResult(dashboard);
            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving project list {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();

            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("GetNotificationCount")]
        public async Task<IActionResult> GetNotificationCount(
         [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage req,
         ILogger log)
        {

            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                List<ProjectEntity> list = null;
                List<ProjectEntity> newEnquiry = null;
                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);

                List<string> creatorList = new List<string>();
                AssignRole role = AssignRole.QI;
                var projectFilter = new ProjectFilter();
                projectFilter.isCompany = isCompany;
                projectFilter.myproject = false;
                projectFilter.companyId = accessToken.CompanyId;
                if (accessToken.Role == AssignRole.QI.Meaning && !isCompany)
                {   
                    projectFilter.stages = "Initiated";
                    newEnquiry = await _projectRepo.GetFilteredProjectList(projectFilter);
                }

                projectFilter.stages = "ConfirmedProjects";
                List<ProjectEntity> pendingReviewList = await _projectRepo.GetFilteredProjectList(projectFilter);

                string[] projectIds = pendingReviewList.Select(x => x.Id).ToArray();

                if (accessToken.Role == AssignRole.QI.Meaning || accessToken.Role == AssignRole.M.Meaning)
                {                   
                    list = await _projectRepo.GetProjectListByCreator(accessToken.CompanyId, AssignRole.GetRole(accessToken.Role), isCompany, accessToken.Id);
                }
                else
                {
                    list = await _projectRepo.ListProject(accessToken.CompanyId, isCompany);
                }
                List<ReviewEntity> reviewList = await _reviewRepo.GetReviewStatusByProjectIds(projectIds, isCompany);              

                int projectReviewCount = 0;
                foreach (ProjectEntity project in pendingReviewList)
                {
                    ReviewEntity reviewCount = reviewList.Where(x => x.ProjectId == project.Id).FirstOrDefault();
                    if (reviewCount == null)
                    {
                        projectReviewCount += 1;
                    }
                }

                if (isCompany)
                {
                    CompanyNotificationCountViewModel companyNotificationCountViewModel = new CompanyNotificationCountViewModel()
                    {
                        NewProjectCount = list.Where(x => x.stages == ProjectStages.EnquirySent).Count(),
                        BidReceivedCount = list.Where(x => x.stages == ProjectStages.BidReceived).Count(),
                        PendingAuthenticationCount = list.Where(x => x.stages == ProjectStages.BidSelected).Count(),
                        ApprovalPendingCount = list.Where(x => x.stages == ProjectStages.BidAuthenticated).Count(),
                        NoShowCount = list.Where(x => x.stages == ProjectStages.NoShow).Count(),
                        ConfirmedCount = list.Where(x => x.stages == ProjectStages.Confirmed).Count(),
                        CompletedCount = list.Where(x => x.stages == ProjectStages.Completed).Count(),
                        ProjectReviewCount = projectReviewCount,
                        NoResponseCount = list.Where(x => x.confirmationDate.Date < DateTime.Now.Date && (x.stages != ProjectStages.Confirmed && x.stages != ProjectStages.Completed && x.stages != ProjectStages.NoShow && x.stages != ProjectStages.NoResponse && x.stages != ProjectStages.InProgress)).Count()
                    };
                    return new JsonResult(companyNotificationCountViewModel);
                }
                else
                {
                    MillsNotificationCountViewModel companyNotificationCountViewModel = new MillsNotificationCountViewModel()
                    {
                        NewProjectCount = newEnquiry != null ? newEnquiry.Count() : list.SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId && x.bidStatus == BidStatus.Initiated).Count(),
                        ApprovalPendingCount = list.SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId && x.bidStatus == BidStatus.Authenticated).Count(),
                        ApprovedBidsCount = list.SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId && (x.bidStatus == BidStatus.Approved || x.bidStatus == BidStatus.Selected || x.bidStatus == BidStatus.BidApproved || x.bidStatus == BidStatus.AwaitingConfirmation)).Count(),
                        NoShowCount = list.SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId && x.bidStatus == BidStatus.NoShow).Count(),
                        ConfirmedCount = list.SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId && x.bidStatus == BidStatus.Confirmed).Count(),
                        CompletedCount = list.SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId && x.bidStatus == BidStatus.Completed).Count(),
                        ProjectReviewCount = projectReviewCount,
                        NoResponseCount = list.Where(x => x.confirmationDate.Date < DateTime.Now.Date).SelectMany(x => x.bids).Where(x => x.companyId == accessToken.CompanyId  && ( x.bidStatus == BidStatus.Initiated || x.bidStatus == BidStatus.Authenticated)).Count()
                    };
                    return new JsonResult(companyNotificationCountViewModel);
                }
            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving project count {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("GetProjectEnquiry")]
        public async Task<IActionResult> GetProjectEnquiry(
           [HttpTrigger(AuthorizationLevel.Function, "options", "get", Route = null)] HttpRequestMessage req, HttpRequest request,
           ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                string id = request.Query["id"];
                if (string.IsNullOrEmpty(id))
                {
                    return new BadRequestObjectResult("Please send request id");
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);

                List<string> CompanyIds = null;
                IEnumerable<CompanyEntity> bidCompanys = null;
                IEnumerable<UserEntity> userList = null;
                IEnumerable<BiddingAuditEntity> bidAuditList = null;

                ProjectEntity project = await _projectRepo.GetItemById(id);
                ProjectSummarydViewModel dashboard = new ProjectSummarydViewModel();
                if (project == null)
                {
                    return new BadRequestObjectResult("Project not found");
                }
                if (project.bids?.Count >= 1)
                {
                    CompanyIds = project.bids?.Select(x => x.companyId).ToList();
                    CompanyIds.Add(project.companyId);
                    bidCompanys = await _companyRepo.GetItemListByIds(CompanyIds.ToArray());
                    bidAuditList = await _bidRepo.GetBidAudit(project.Id);
                    userList = await _userRepo.GetUserListsByCompanyIds(CompanyIds.ToArray());
                }
                UserEntity user = await _userRepo.GetItemById(project.creatorId);

                dashboard.Id = project.Id;
                dashboard.Subcategory = project.controls.Count > 0 ? project.controls[0].value : "";
                dashboard.EnquiryName = project.name;
                dashboard.Merchandiser = user.FullName;
                dashboard.Category = project.category;
                dashboard.InStages = project.stages.GetAttribute<DisplayAttribute>().Name;
                dashboard.Status = project.status;
                dashboard.Action = "View";
                dashboard.RequestedDelivery = Convert.ToString(((project.deliveryDate) - project.CreatedDate).Days);
                dashboard.CompanyId = user.CompanyId;
                dashboard.summary = project.controls.ConvertAll<SummaryViewModel>(x => x);
                dashboard.data = project.data;
                dashboard.CreatedDate = TimeZoneHelper.ConvertDateTimetoIST(project.CreatedDate);
                dashboard.CreatedBy = user.FullName;
                dashboard.Message = project.Message;
                dashboard.BiddingTime = project.BiddingTime;

                ReviewEntity reviewEntity = await _reviewRepo.GetReviewStatusById(id, isCompany);

                if (!isCompany)
                {
                    foreach (Bids bid in project.bids.ToList())
                    {
                        if (bid.companyId == accessToken.CompanyId)
                            continue;

                        project.bids.Remove(bid);
                    }
                }
                else
                {
                    project.bids.RemoveAll(x => x.bidStatus == BidStatus.Initiated || x.bidStatus == BidStatus.Authenticated || x.price == null);
                }

                int bidsReceived = 0;

                foreach (Bids bid in project.bids.OrderByDescending(x => (int)(x.bidStatus)).ToList())
                {
                    CompanyEntity company = bidCompanys.Where(x => x.Id == bid.companyId).FirstOrDefault();
                    if (company == null)
                        continue;
                    byte[] imgBytes = new byte[0];
                    if (company.ImageName != null)
                    {
                        imgBytes = _userProfileRepo.GetImageBytes(company.ImageName);
                    }
                    bidsReceived += 1;
                    var bidSubmitterId = bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.userId).FirstOrDefault();
                    string submittedDate = "";
                    if (bidAuditList.Where(x => x.bidId == bid.Id).Count() > 0)
                    {
                        if (isCompany)
                        {
                            bidSubmitterId = bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.userId).FirstOrDefault();
                            submittedDate = TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.CreatedDate).FirstOrDefault());
                        }
                        else
                        {
                            if (bid.bidStatus == BidStatus.Authenticated || bid.bidStatus == BidStatus.Cancelled)
                            {
                                bidSubmitterId = bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == bid.bidStatus).Select(x => x.userId).FirstOrDefault();
                                submittedDate = TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == bid.bidStatus).Select(x => x.CreatedDate).FirstOrDefault());
                            }
                            else
                            {
                                bidSubmitterId = bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.userId).FirstOrDefault();
                                submittedDate = TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidId == bid.Id && x.bidStatus == BidStatus.Approved).Select(x => x.CreatedDate).FirstOrDefault());
                            }

                            if (bid.bidStatus == BidStatus.Selected || bid.bidStatus == BidStatus.BidApproved || bid.bidStatus == BidStatus.AwaitingConfirmation)
                            {
                                bid.bidStatus = BidStatus.Approved;
                            }
                        }
                    }

                    bool isPriceDecimal = Decimal.TryParse(bid.price, out decimal price);
                    dashboard.bidList.Add(new BidsViewModel()
                    {
                        Id = bid.Id,
                        companyId = bid.companyId,
                        CompanyName = company.UnitName,
                        Review = company.Review,
                        Ratings = reviewEntity?.ReviewRating,
                        Logo = imgBytes,
                        RequestedPrice = price,
                        RequestedCredit = bid.creditPeriod,
                        RequestedDelivery = bid.deliveryPeriod,
                        Status = bid.bidStatus.ToString(),
                        Message = bid.Message,
                        SubmittedBy = bidSubmitterId != null ? userList.Where(x => x.Id == bidSubmitterId).FirstOrDefault()?.FullName : "",
                        SubmittedDate = submittedDate,
                    });
                }
                dashboard.BidsReceived = bidsReceived;

                if (dashboard.bidList.Count >= 0)
                {
                    dashboard.maxPrice = dashboard.bidList.Max(x => x.RequestedPrice);
                    dashboard.maxCreditPeriod = dashboard.bidList.Max(x => x.RequestedCredit);
                    dashboard.maxDeliveryPeriod = dashboard.bidList.Max(x => x.RequestedDelivery);
                }

                return new JsonResult(dashboard);
            }
            catch (Exception ex)
            {
                log.LogError("Error retrieving project details {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }

    }
}
