using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Tik.Dashboard
{
    public class NewProjectFunc
    {
        [FunctionName("NewProject")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ExecutionContext context,
            ILogger log)
        {
            string finaldata = "";
            try
            {
                if (!File.Exists("CreateProject.json"))
                {
                    log.LogError("File does not exists");
                }
                string jsonfile = Path.Combine(context.FunctionAppDirectory, "CreateProject.json");
                using (StreamReader r = new StreamReader(jsonfile))
                {
                    string json = r.ReadToEnd();
                    finaldata = json.Replace("\r\n", "");
                }
            }
            catch (Exception ex)
            {
                log.LogError("Error in New Project", ex.Message, ex.StackTrace);
            }
            return new JsonResult(finaldata);
        }
    }
}
