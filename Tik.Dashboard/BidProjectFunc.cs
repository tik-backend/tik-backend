using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Tik.BusinessService.Interfaces;
using Tik.Repository.ProjectRepository;
using Tik.Repository;
using Tik.DomainModels;
using System.Linq;
using System.Net;
using Tik.BusinessService;
using Tik.BusinessService.Helper;
using Tik.DomainModels.ProjectDomainModel;
using Tik.DomainModels.Enum;
using Tik.Dashboard.ViewModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tik.DomainModels.Validators;
using Tik.DomainModels.CommonEntity;
using Tik.Repository.CommonRepository;
using Tik.Repository.EmployeeRepository;
using Newtonsoft.Json;

namespace Tik.Dashboard
{
    public class BidProjectFunc
    {
        private readonly IProjectRepository _projectRepo;
        private readonly IAuthenticationService _tokenIssuer;
        private readonly IUserRepository _userRepo;
        private readonly IBiddingAuditRepository _bidRepo;
        private readonly IBidNotificationRepository _notificationRepo;
        private readonly IEmployeeRepository _employeeRepo;
        private readonly ISMSNotificationRepository _smsNotificationRepo;
        private readonly ICompanyRepository _companyRepo;
        private readonly ISMSService _smsService;
        public BidProjectFunc(IAuthenticationService tokenIssuer, IProjectRepository projectRepo, IUserRepository userRepo, IBiddingAuditRepository bidRepo, IBidNotificationRepository notificationRepo, IEmployeeRepository employeeRepo, ISMSNotificationRepository smsNotification, ICompanyRepository companyRepo, ISMSService smsService)
        {
            this._projectRepo = projectRepo ?? throw new ArgumentNullException(nameof(projectRepo));
            this._tokenIssuer = tokenIssuer ?? throw new ArgumentNullException(nameof(tokenIssuer));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
            this._bidRepo = bidRepo ?? throw new ArgumentNullException(nameof(bidRepo));
            this._notificationRepo = notificationRepo ?? throw new ArgumentNullException(nameof(notificationRepo));
            this._employeeRepo = employeeRepo ?? throw new ArgumentNullException(nameof(employeeRepo));
            this._smsNotificationRepo = smsNotification ?? throw new ArgumentNullException(nameof(smsNotification));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
            this._smsService = smsService ?? throw new ArgumentNullException(nameof(smsService));
        }

        [FunctionAuthorize("MasterAdmin", "Quote InCharge")]
        [FunctionName("CreateProject")]
        public async Task<IActionResult> CreateProject(
            [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req,
            ILogger log)
        {
            AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
            if (status.Code != HttpStatusCode.Accepted)
            {
                var result = new ObjectResult(status.Message);
                result.StatusCode = Convert.ToInt32(status.Code);
                return result;
            }

            var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
            string jsonContent = await req.Content.ReadAsStringAsync();

            try
            {
                var project = JsonConvert.DeserializeObject<ProjectEntity>(jsonContent);
                project.stages = ProjectStages.EnquirySent;
                project.creatorId = accessToken.Id;
                project.companyId = accessToken.CompanyId;
                project.UpdatedDate = DateTime.Now;
                project.price = project.data.price.requestPrice ? "Requested" : project.data.price.price;
                project.noOfKgs = project.noOfKgs;
                project.confirmationDate = project.data.confirmationDate;
                project.deliveryDate = project.data.deliveryDate;
                project.creditPeriod = project.data.creditPeriod;
                project.requestPrice = project.data.price.requestPrice;

                foreach (Bids bid in project.bids)
                {
                    bid.bidStatus = BidStatus.Initiated;
                    bid.Id = Convert.ToString(Guid.NewGuid());
                }
                _projectRepo.CreateProject(project);
                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);
                UserEntity user = await _userRepo.GetItemById(accessToken.Id);
                sendNotification(isCompany, project.bids, user.FullName, project, accessToken.CompanyId);

                return new OkObjectResult("Project created successfully");
            }
            catch (Exception ex)
            {
                log.LogError("Error creating new project details {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestResult();
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("BidProject")]
        public async Task<IActionResult> BidProject(
            [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req, HttpRequest request,
            ILogger log)
        {
            string id = request.Query["projectId"];
            if (string.IsNullOrEmpty(id))
            {
                return new BadRequestObjectResult("ProjectId is required");
            }

            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(req.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
                var bidRequest = await req.GetJsonBody<Bids, BidProjectValidator>();
                if (!bidRequest.IsValid)
                {
                    return bidRequest.ToBadRequest();
                }

                if (string.IsNullOrEmpty(bidRequest.Value.Id))
                {
                    return new BadRequestObjectResult("Please send bid id");
                }

                bool isFirstTimeBid = true;
                ProjectEntity project = await _projectRepo.GetBidById(id);
                Bids bid = project.bids.Where(x => x.Id == bidRequest.Value.Id && x.companyId == accessToken.CompanyId).FirstOrDefault();
                if (bid == null)
                {
                    return new BadRequestObjectResult("Bid does not exist for current project");
                }

                if (!string.IsNullOrEmpty(bid.price))
                    isFirstTimeBid = false;

                bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);

                if (!isCompany && string.IsNullOrEmpty(bidRequest.Value.ApprovalAdminId) && string.IsNullOrEmpty(bid.ApprovalAdminId))
                {
                    return new BadRequestObjectResult("Approval Admin is required");
                }

                bid.deliveryPeriod = bidRequest.Value.deliveryPeriod;
                bid.creditPeriod = bidRequest.Value.creditPeriod;
                bid.price = bidRequest.Value.price;
                bid.ApprovalAdminId = bid.ApprovalAdminId == null? bidRequest.Value.ApprovalAdminId: bid.ApprovalAdminId;
                bid.UpdatedDate = DateTime.Now;
                if (isFirstTimeBid)
                {
                    bid.creatorId = accessToken.Id;
                }      

                await _bidRepo.AddItemAsync(new BiddingAuditEntity
                {
                    projectId = project.Id,
                    bidId = bid.Id,
                    isCompany = false,
                    bidStatus = BidStatus.Authenticated,
                    userId = accessToken.Id,
                    companyId = accessToken.CompanyId,
                    Id = Guid.NewGuid().ToString(),
                    oldBidStatus = bid.bidStatus.ToString()
                });

                bid.bidStatus = BidStatus.Authenticated;
                bid.companyId = accessToken.CompanyId;
                bid.SubmittedDate = DateTime.Now;

                await _projectRepo.UpdateItemAsync(project.Id, project);
                UserEntity user = await _userRepo.GetItemById(accessToken.Id);
                List<Bids> bids = new List<Bids>();
                bids.Add(bid);
                sendNotification(isCompany, bids, user.FullName, project, accessToken.CompanyId);
                return new OkObjectResult("Bid successfully placed");
            }
            catch (Exception ex)
            {
                log.LogError("Error creating bid {0} {1}", ex.Message, ex.StackTrace);
                return new BadRequestObjectResult("Error placing bid");
            }

        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        [FunctionName("ApproveBid")]
        public async Task<IActionResult> ApproveBid(
            [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req, HttpRequest request,
            ILogger log)
        {

            string projectId = request.Query["projectId"];
            if (string.IsNullOrEmpty(projectId))
            {
                return new BadRequestObjectResult("Project Id is required");
            }
            string bidId = request.Query["bidId"];
            if (string.IsNullOrEmpty(bidId))
            {
                return new BadRequestObjectResult("Bid Id is required");
            }

            string status = request.Query["status"];
            if (string.IsNullOrEmpty(status))
            {
                return new BadRequestObjectResult("Status is required");
            }

            string ApprovalAdminId = request.Query["ApprovalAdminId"];
            string message = request.Query["message"];

            AuthenticationStatus authStatus = _tokenIssuer.GetAuthenticationStatus(req.Headers);
            if (authStatus.Code != HttpStatusCode.Accepted)
            {
                var result = new ObjectResult(authStatus.Message);
                result.StatusCode = Convert.ToInt32(authStatus.Code);
                return result;
            }
            string oldBidStatus = "";
            ProjectEntity project = await _projectRepo.GetBidById(projectId);
            var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);

            bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);

            Enum.TryParse(status, out BidStatus bidStatus);

            if(bidStatus == 0)
                return new BadRequestObjectResult("Invalid Bid status");

            UserEntity user = await _userRepo.GetItemById(accessToken.Id);
            Bids bid = project.bids.Where(x => x.Id == bidId).FirstOrDefault();

            if (bid == null)
            {
                return new BadRequestObjectResult("Bid does exist");
            }
            if (status == BidStatus.Approved.ToString() && accessToken.CompanyId != bid.companyId)
            {
                return new BadRequestObjectResult("Unautherized user cannot bid this project");
            }
            oldBidStatus = bid.bidStatus.ToString();
            if (!isCompany)
            {
                if (status == BidStatus.Authenticated.ToString())
                {
                    bid.bidStatus = BidStatus.Authenticated;
                    project.stages = ProjectStages.BidSelected;
                }
                else if (status == BidStatus.Approved.ToString() && (accessToken.Role == AssignRole.AA.Meaning || accessToken.Role == AssignRole.MA.Meaning || user.ApprovalAdminAccess == "1"))
                {
                    bid.bidStatus = BidStatus.Approved;
                    project.stages = ProjectStages.BidReceived;
                }
            }
            else
            {
                if (status == BidStatus.Selected.ToString() && (accessToken.Role == AssignRole.QI.Meaning || accessToken.Role == AssignRole.MA.Meaning))
                {
                    TimeSpan span = (DateTime.Now - project.CreatedDate);
                    if (span.Days > 0)
                        project.BiddingTime = String.Format("{0} days, {1} hours", span.Days, new DateTime(span.Ticks).ToString("HH:mm"));
                    else
                        project.BiddingTime = String.Format("{0} hours", new DateTime(span.Ticks).ToString("HH:mm"));


                    bid.bidStatus = BidStatus.Selected;
                    project.stages = ProjectStages.BidSelected;
                }
                else if (status == BidStatus.BidApproved.ToString() && (accessToken.Role == AssignRole.M.Meaning || accessToken.Role == AssignRole.MA.Meaning ))
                {
                    if (string.IsNullOrEmpty(ApprovalAdminId))
                    {
                        return new BadRequestObjectResult("Approval Admin Id is required");
                    }
                    bid.bidStatus = BidStatus.BidApproved;
                    project.stages = ProjectStages.BidAuthenticated;
                    project.ApprovalAdminId = ApprovalAdminId;
                }
                else if (status == BidStatus.Confirmed.ToString() && (accessToken.Role == AssignRole.AA.Meaning || accessToken.Role == AssignRole.MA.Meaning || user.ApprovalAdminAccess == "1"))
                {
                    bid.bidStatus = BidStatus.Confirmed;
                    project.stages = ProjectStages.Confirmed;
                }
            }

            if (status == BidStatus.NoShow.ToString())
            {
                if (bid.bidStatus == BidStatus.Confirmed)
                    project.stages = ProjectStages.NoShow;                

                bid.bidStatus = BidStatus.NoShow;               
            }
            else if (status == BidStatus.Cancelled.ToString())
            {
                bid.bidStatus = BidStatus.Cancelled;
            }          
            else if (oldBidStatus == bid.bidStatus.ToString())
            {
                return new BadRequestObjectResult("User does not have the permission to " + bidStatus.GetAttribute<DisplayAttribute>().Name + " bid");
            }
            bid.Message = message;
            UpdateBidStatus(project, bid);
            await _bidRepo.AddItemAsync(new BiddingAuditEntity
            {
                projectId = project.Id,
                bidId = bidId,
                isCompany = isCompany,
                bidStatus = bidStatus,
                userId = user.Id,
                companyId = accessToken.CompanyId,
                Id = Guid.NewGuid().ToString(),
                oldBidStatus = oldBidStatus
            });

            project.UpdatedDate = DateTime.Now;
            await _projectRepo.UpdateItemAsync(project.Id, project);

            List<Bids> bids = new List<Bids>();
            bids.Add(bid);
            sendNotification(isCompany, bids, user.FullName, project, accessToken.CompanyId);

            return new OkObjectResult("Bid " + bidStatus.GetAttribute<DisplayAttribute>().Name);
        }

        [FunctionName("UpdateNoResponse")]
        [FunctionAuthorize("MasterAdmin", "SuperAdmin", "Approval Admin", "Merchandiser", "Quote InCharge")]
        public async Task<IActionResult> UpdateNoResponse(
          [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req, HttpRequest request,
          ILogger log)
        {           
            string projectId = request.Query["projectId"];
            if (string.IsNullOrEmpty(projectId))
            {
                return new BadRequestObjectResult("Project Id is required");
            }
            string message = request.Query["message"];

            AuthenticationStatus authStatus = _tokenIssuer.GetAuthenticationStatus(req.Headers);
            if (authStatus.Code != HttpStatusCode.Accepted)
            {
                var result = new ObjectResult(authStatus.Message);
                result.StatusCode = Convert.ToInt32(authStatus.Code);
                return result;
            }

            string status = request.Query["status"];
            if (string.IsNullOrEmpty(status))
            {
                return new BadRequestObjectResult("Status Id is required");
            }

            ProjectEntity project = await _projectRepo.GetItemById(projectId);
            var accessToken = _tokenIssuer.ValidateAuthenticationToken(req);
            bool isCompany = Common.CheckIsCompanyUser(accessToken.Category);
            if (project == null)
                return new BadRequestObjectResult("Project not found");
           
            string previousStatus = project.stages.ToString();

            if (isCompany && status == ProjectStages.NoResponse.ToString() && project.confirmationDate.Date < DateTime.Now.Date && (project.stages != ProjectStages.Confirmed || project.stages != ProjectStages.Completed))
            {
                project.stages = ProjectStages.NoResponse;
                project.Message = message;
            }
            else
            {
                return new BadRequestObjectResult("User does not have the permission to update the status");
            }          

            foreach (Bids bidder in project.bids)
            {
                bidder.bidStatus = BidStatus.NoResponse;
                bidder.Message = message;
                bidder.UpdatedDate = DateTime.Now;
            }

            project.UpdatedDate = DateTime.Now;
            project.UpdatedBy = accessToken.Id;
            await _projectRepo.UpdateItemAsync(project.Id, project);

            await _bidRepo.AddItemAsync(new BiddingAuditEntity
            {
                projectId = project.Id,
                bidId = "",
                isCompany = isCompany,
                bidStatus = BidStatus.NoResponse,
                userId = accessToken.Id,
                companyId = accessToken.CompanyId,
                Id = Guid.NewGuid().ToString(),
                oldBidStatus = previousStatus,
            });

            return new OkObjectResult("Project status updated successfully");
        }


        [FunctionName("GetBidAudit")]
        [FunctionAuthorize]
        public async Task<IActionResult> GetBidAudit(
           [HttpTrigger(AuthorizationLevel.Function, "options", "post", Route = null)] HttpRequestMessage req, HttpRequest request,
           ILogger log)
        {
            string projectId = request.Query["projectId"];
            if (string.IsNullOrEmpty(projectId))
            {
                return new BadRequestObjectResult("ProjectId is required");
            }

            var bidAuditList = await _bidRepo.GetBidAudit(projectId);
            bidAuditList = bidAuditList.Where(x => x.isCompany == true).ToList();
            List<string> userIds = bidAuditList.Select(x => x.userId).ToList();
            IEnumerable<UserEntity> userList = await _userRepo.GetItemListByIds(userIds.ToArray());
            var selectorId = bidAuditList.Where(x => x.bidStatus == BidStatus.Selected).Select(x => x.userId).FirstOrDefault();
            var approverId = bidAuditList.Where(x => x.bidStatus == BidStatus.BidApproved).Select(x => x.userId).FirstOrDefault();
            var confirmId = bidAuditList.Where(x => x.bidStatus == BidStatus.Confirmed).Select(x => x.userId).FirstOrDefault();

            BidAuditViewModel bidview = new BidAuditViewModel();
            bidview.approveBy = approverId != null ? userList.Where(x => x.Id == approverId).FirstOrDefault()?.FullName : "";
            bidview.selectedBy = selectorId != null ? userList.Where(x => x.Id == selectorId).FirstOrDefault()?.FullName : "";
            bidview.confirmedBy = confirmId != null ? userList.Where(x => x.Id == confirmId).FirstOrDefault()?.FullName : "";

            bidview.approveDate = approverId == null ? "" : TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidStatus == BidStatus.BidApproved).Select(x => x.CreatedDate).FirstOrDefault());
            bidview.selectedDate = selectorId == null ? "" : TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidStatus == BidStatus.Selected).Select(x => x.CreatedDate).FirstOrDefault());
            bidview.confirmedDate = confirmId == null ? "" : TimeZoneHelper.ConvertDateTimetoIST(bidAuditList.Where(x => x.bidStatus == BidStatus.Confirmed).Select(x => x.CreatedDate).FirstOrDefault());

            return new JsonResult(bidview);
        }

        private void UpdateBidStatus(ProjectEntity project, Bids bid)
        {
            if (project.stages == ProjectStages.Confirmed)
            {
                foreach (Bids bidder in project.bids)
                {
                    if (bidder.Id == bid.Id || bid.bidStatus == BidStatus.Rejected || bid.bidStatus == BidStatus.Cancelled)
                        continue;

                    bidder.bidStatus = BidStatus.Rejected;
                    bidder.Message = "Another bid confirmed";
                    bidder.UpdatedDate = DateTime.Now;
                }
            }
        }

        private async void sendNotification(bool isCompany, List<Bids> bids, string user, ProjectEntity project, string companyId)
        {
            List<BidNotification> notificationList = await _notificationRepo.GetNotificationListByStatus(bids.FirstOrDefault().bidStatus.ToString());
            string notificationId = notificationList.FirstOrDefault()?.Id;
            if (!string.IsNullOrEmpty(notificationId))
            {
                List<string> companyIds = bids.Select(x => x.companyId).ToList();
                companyIds.Add(companyId);
                companyIds.Add(project.companyId);
                CompanyEntity company = await _companyRepo.GetCompanyById(companyId);
                IDictionary<string, string> param = new Dictionary<string, string>();
                param.Add("$Category$", project.category);
                param.Add("$EnquiryName$", project.name.ToUpper());
                param.Add("$CompanyName$", company.UnitName);
                param.Add("$User$", user);
                List<UserEntity> userList = await _employeeRepo.GetEmployeeNotificationList(companyIds.Distinct().ToArray(), notificationId);
                List<SMSTemplateEntity> smsTemplate = await _smsNotificationRepo.GetSMSTemplateByStatus(bids.FirstOrDefault().bidStatus.ToString());
                _smsService.SendSMS(userList, smsTemplate, param);
            }
        }
    }
}
