﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Tik.BusinessService;
using Tik.BusinessService.Implementations;
using Tik.BusinessService.Interfaces;
using Tik.Repository;
using Tik.Repository.CertificationRepository;
using Tik.Repository.CommonRepository;
using Tik.Repository.EmployeeRepository;
using Tik.Repository.ProjectRepository;

[assembly: FunctionsStartup(typeof(Tik.Login.AppStartup))]
namespace Tik.Login
{
    public class AppStartup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile($"local.settings.json", optional: true, reloadOnChange: true)
                  .AddEnvironmentVariables()
                  .Build();

            services.AddSingleton<IConfiguration>(configuration);

            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IMillsRepository, MillsRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IUserProfileRepository, UserProfileRepository>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IBiddingAuditRepository, BiddingAuditRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
            services.AddScoped<IBidNotificationRepository, BidNotificationRepository>();
            services.AddScoped<ISMSNotificationRepository, SMSNotificationRepository>();
            services.AddScoped<ISMSService, SMSService>();
            services.AddScoped<ICertificationRepository, CertificationRepository>();

            string conStr = System.Environment.GetEnvironmentVariable($"ConnectionStrings_Tik", EnvironmentVariableTarget.Process);
            string databaseName = System.Environment.GetEnvironmentVariable($"DatabaseName", EnvironmentVariableTarget.Process);
            string blobConStr = System.Environment.GetEnvironmentVariable($"AzureWebJobsStorage", EnvironmentVariableTarget.Process);
            services.AddCosmosDb(conStr, databaseName, blobConStr);
        }
    }
}
