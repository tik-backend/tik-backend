﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.Dashboard.ViewModel
{
    public class ProjectSearchViewModel
    {
        public string CompanyId { get; set; }
        [JsonProperty(PropertyName = "Mill Name")]
        public string MillName { get; set; }       

        [JsonProperty(PropertyName = "Credit Period")]
        public string CreditPeriod { get; set; }
        public string Review { get; set; }

        [JsonProperty(PropertyName = "certification")]
        public string[] Certification { get; set; }
    }
}
