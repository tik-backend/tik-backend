﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.Dashboard.ViewModel
{
    public class BidAuditViewModel
    {
        public string projectId { get; set; }
        public string selectedBy { get; set; }
        public string confirmedBy { get; set; }
        public string approveBy { get; set; }
        public string selectedDate { get; set; }
        public string confirmedDate { get; set; }
        public string approveDate { get; set; }

    }
}
