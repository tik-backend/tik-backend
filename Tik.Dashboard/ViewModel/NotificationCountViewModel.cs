﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.Dashboard.ViewModel
{

    public class BaseNotificationCount
    {
        public int NewProjectCount { get; set; }
        public int ApprovalPendingCount { get; set; }
        public int NoShowCount { get; set; }
        public int ConfirmedCount { get; set; }
        public int CompletedCount { get; set; }
        public int ProjectReviewCount { get; set; }
        public int NoResponseCount { get; set; }
    }
    public class CompanyNotificationCountViewModel : BaseNotificationCount
    {       
        public int BidReceivedCount { get; set; }
        public int PendingAuthenticationCount { get; set; } 
    }

    public class MillsNotificationCountViewModel : BaseNotificationCount
    {
        public int ApprovedBidsCount { get; set; }
    }
}
