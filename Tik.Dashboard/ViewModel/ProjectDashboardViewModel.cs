﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Tik.BusinessService.Helper;
using Tik.DomainModels.Enum;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Dashboard.ViewModel
{
    public class ProjectDashboardViewModel : BaseProjectDashboard
    {
        public int BidsReceived { get; set; }
        public string BiddingTime { get; set; }
        public static implicit operator ProjectDashboardViewModel(ProjectEntity entity)
        {
            return new ProjectDashboardViewModel()
            {
                Id = entity.Id,
                Subcategory = entity.controls.Count > 0 ? entity.controls[0].value : "",
                EnquiryName = entity.name,
                Category = entity.category,
                InStages = entity.stages.GetAttribute<DisplayAttribute>().Name,
                Status = entity.status,
                Action = "View",
                CompanyId = entity.companyId,
                CreatorId = entity.creatorId,
                MerchandiserId = entity.merchandiserId,
                BidsReceived = entity.bids.Where(x => x.bidStatus != BidStatus.Initiated && x.bidStatus != BidStatus.Authenticated && x.bidStatus != BidStatus.Cancelled && x.bidStatus != BidStatus.NoResponse).Count(),
                BiddingTime = entity.BiddingTime
            };
         }
    }

    public class BaseProjectDashboard
    {
        [JsonProperty(PropertyName = "EnquiryName")]
        public string EnquiryName { get; set; }

        [JsonProperty(PropertyName = "Merchandiser")]
        public string Merchandiser { get; set; }
        [JsonProperty(PropertyName = "ApprovalAdmin")]
        public string ApprovalAdmin { get; set; }

        [JsonProperty(PropertyName = "Category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "Subcategory")]
        public string Subcategory { get; set; }

        [JsonProperty(PropertyName = "InStages")]
        public string InStages { get; set; }

        [JsonProperty(PropertyName = "Status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "CompanyId")]
        public string CompanyId { get; set; }

        [JsonProperty(PropertyName = "Action")]
        public string Action { get; set; }

        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [JsonProperty(PropertyName = "CreatedDate")]
        public string CreatedDate { get; set; }

        [JsonProperty(PropertyName = "CreatorId")]
        public string CreatorId { get; set; }
        public string MerchandiserId { get; set; }
    } 

    public class ProjectSummarydViewModel : BaseProjectDashboard
    {        
        [JsonProperty(PropertyName = "deliveryDate")]
        public string RequestedDelivery { get; set; }      
        public List<BidsViewModel> bidList { get; set; }
        public List<SummaryViewModel> summary { get; set; }        
        public DataViewModel data { get; set; }
        public int BidsReceived { get; set; }
        public string Message { get; set; }        
        public string BiddingTime { get; set; }
        public decimal? maxPrice { get; set; }
        public int? maxCreditPeriod { get; set; }
        public int? maxDeliveryPeriod { get; set; }
        public ProjectSummarydViewModel()
        {
            bidList = new List<BidsViewModel>();
        }

    }
    public class BidsViewModel
    {
        public string Id { get; set; }
        [JsonProperty(PropertyName = "requestPrice")]
        public decimal? RequestedPrice { get; set; }
        [JsonProperty(PropertyName = "creditPeriod")]
        public int? RequestedCredit { get; set; }
        [JsonProperty(PropertyName = "deliveryDate")]
        public int? RequestedDelivery { get; set; }      
        public string Status { get; set; }
        public string Message { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string SubmittedDate { get; set; }
        public string SubmittedBy { get; set; }
        public string companyId { get; set; }
        public string CompanyName { get; set; }
        public byte[] Logo { get; set; }
        public decimal? Review { get; set; }
        [JsonProperty(PropertyName = "ratings")]
        public decimal? Ratings { get; set; }
    }

    public class DataViewModel
    {
        public string noOfKgs { get; set; }
        public string confirmationDate { get; set; }
        public bool requestPrice { get; set; }
        public string deliveryDate { get; set; }
        public string price { get; set; }
        public int? creditPeriod { get; set; }
        public string gsm { get; set; }
        public string loopLength { get; set; }
        public Dia dia { get; set; }
        public string certification { get; set; }

        public static implicit operator DataViewModel(ProjectData data)
        {
            if (data == null)
                return new DataViewModel();

            return new DataViewModel()
            {
                confirmationDate = TimeZoneHelper.ConvertDateTimetoIST(data.confirmationDate),
                deliveryDate = TimeZoneHelper.ConvertDateTimetoIST(data.deliveryDate),
                requestPrice = data.price.requestPrice,
                price = data.price.price,
                creditPeriod = data.creditPeriod,
                gsm = data.gsm,
                loopLength = data.loopLength,
                dia = data.dia,
                certification = data.certification,
                noOfKgs = data.noOfKgs == null ? Convert.ToString(data?.dia?.total) : data.noOfKgs
            };
        }
    }

    public class SummaryViewModel
    {
        public string label { get; set; }
        public dynamic value { get; set; }

        public static implicit operator SummaryViewModel(Summary entity)
        {
            return new SummaryViewModel()
            {
                label = entity.label,
                value = entity.value
            };
        }
    }
}
