﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Tik.BusinessService.EmailServices;
using Tik.DomainModels;
using Tik.Repository;
using Tik.BusinessService.Interfaces;
using Tik.BusinessService;

[assembly: FunctionsStartup(typeof(Tik.Seed.AppStartup))]
namespace Tik.Seed
{
    public class AppStartup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            services.AddScoped<ICountryRepository, CountryRepository>();

            services.AddScoped<IStateRepository, StateRepository>();

            services.AddScoped<ICityRepository, CityRepository>();

            services.AddScoped<IInsertStateRepository, InsertStateRepository>();

            services.AddScoped<IInsertCityRepository, InsertCityRepository>();

            services.AddScoped<IAuthenticationService, AuthenticationService>();

            services.Configure<SendGridEmailSettings>(configuration.GetSection("SendGridEmailSettings"));
            services.AddScoped<IEmailService, SendGridEmailService>();

            string conStr = System.Environment.GetEnvironmentVariable($"ConnectionStrings_Tik", EnvironmentVariableTarget.Process);
            string databaseName = System.Environment.GetEnvironmentVariable($"DatabaseName", EnvironmentVariableTarget.Process);
            string blobConStr = System.Environment.GetEnvironmentVariable($"AzureWebJobsStorage", EnvironmentVariableTarget.Process);

            services.AddCosmosDb(conStr, databaseName, blobConStr);
        }
    }
}
