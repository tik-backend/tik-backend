using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using Tik.DomainModels;
using System.IO;
using System.Net.Http;
using System.Data;
using ExcelDataReader;
using System.Linq;

namespace TIK.Seed
{
    public class InsertCityFunc
    {
        private readonly IInsertCityRepository _cityRepo;

        public InsertCityFunc(IInsertCityRepository cityRepo)
        {
            this._cityRepo = cityRepo ?? throw new ArgumentNullException(nameof(cityRepo));
        }

        [FunctionName("InsertCity")]
        public async Task<IActionResult> InsertCity(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                var formCollection = await req.ReadFormAsync();
                IFormFile file = null;
                if (formCollection.Files.Count > 0)
                    file = formCollection.Files.First();

                DataSet dsexcelRecords = new DataSet();
                IExcelDataReader dataReader = null;
                CityEntity cityEntity = new CityEntity();

                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = File.Open(@"C:\PROJECT\TIK\Tik.BusinessService\SeedCommon\City.xlsx", FileMode.Open, FileAccess.Read))
                {
                    if (file != null && stream != null)
                    {
                        if (file.FileName.EndsWith(".xls"))
                            dataReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        else if (file.FileName.EndsWith(".xlsx"))
                            dataReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        else
                            return new BadRequestObjectResult("The file format is not supported.");
                        dsexcelRecords = dataReader.AsDataSet();

                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataTable dtStateRecords = dsexcelRecords.Tables[0];
                            for (int column = 0; column < reader.RowCount; column++)
                            {
                                cityEntity.CityID = Convert.ToInt32(dtStateRecords.Rows[column][0]).ToString();
                                cityEntity.CityName = Convert.ToString(dtStateRecords.Rows[column][1]);
                                cityEntity.StateId = Convert.ToInt32(dtStateRecords.Rows[column][2]).ToString();
                                cityEntity.CountryId = Convert.ToInt32(dtStateRecords.Rows[column][3]).ToString();
                                cityEntity.Status = Convert.ToInt32(dtStateRecords.Rows[column][4]).ToString();
                                bool isSuccess = await _cityRepo.UploadData(cityEntity);

                                if (reader.RowCount > 5161)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    return new OkObjectResult("Data loaded successfully.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
