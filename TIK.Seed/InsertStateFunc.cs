using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Tik.Repository;
using Tik.DomainModels;
using System.IO;
using System.Net.Http;
using System.Data;
using ExcelDataReader;
using System.Linq;

namespace TIK.Seed
{
    public class InsertStateFunc
    {
        private readonly IInsertStateRepository _stateRepo;

        public InsertStateFunc(IInsertStateRepository stateRepo)
        {
            this._stateRepo = stateRepo ?? throw new ArgumentNullException(nameof(stateRepo));
        }

        [FunctionName("InsertState")]
        public async Task<IActionResult> InsertState(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                var formCollection = await req.ReadFormAsync();
                IFormFile file = null;
                if (formCollection.Files.Count > 0)
                    file = formCollection.Files.First();

                DataSet dsexcelRecords = new DataSet();
                IExcelDataReader dataReader = null;
                StateEntity stateEntity = new StateEntity();

                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = File.Open(@"C:\PROJECT\TIK\Tik.BusinessService\SeedCommon\State.xlsx", FileMode.Open, FileAccess.Read))
                {
                    if (file != null && stream != null)
                    {
                        if (file.FileName.EndsWith(".xls"))
                            dataReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        else if (file.FileName.EndsWith(".xlsx"))
                            dataReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        else
                            return new BadRequestObjectResult("The file format is not supported.");
                        dsexcelRecords = dataReader.AsDataSet();

                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataTable dtStateRecords = dsexcelRecords.Tables[0];
                            for (int column = 0; column < reader.RowCount; column++)
                            {
                                stateEntity.StateID = Convert.ToInt32(dtStateRecords.Rows[column][0]).ToString();
                                stateEntity.StateName = Convert.ToString(dtStateRecords.Rows[column][1]);
                                stateEntity.CountryId = Convert.ToInt32(dtStateRecords.Rows[column][2]).ToString();
                                stateEntity.Status = Convert.ToInt32(dtStateRecords.Rows[column][3]).ToString();
                                bool isSuccess = await _stateRepo.UploadData(stateEntity);

                                if (reader.RowCount > 35)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    return new OkObjectResult("Data loaded successfully.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
