﻿using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Tik.BusinessService.Interfaces;
using Tik.DomainModels;

namespace Tik.BusinessService
{
    [Obsolete]
    public class FunctionAuthorizeAttribute : FunctionInvocationFilterAttribute
    {
        private readonly IAuthenticationService _tokenIssuer;
        private readonly string[] _validRoles;

        public FunctionAuthorizeAttribute()
        {
            _tokenIssuer = new AuthenticationService();
        }
        public FunctionAuthorizeAttribute(params string[] validRoles)
        {
            _validRoles = validRoles;
            _tokenIssuer = new AuthenticationService();
        }


        public override Task OnExecutingAsync(FunctionExecutingContext executingContext, CancellationToken cancellationToken)
        {
            var workItem = executingContext.Arguments.First().Value as HttpRequestMessage;
            AuthToken token = _tokenIssuer.ValidateAuthenticationToken(workItem);
            if (!token.ValidToken)
            {
                string statusMessage = "Unauthorized";
                if (token.IsExpired)
                {
                    statusMessage = "token-expired";
                }
                workItem.Headers.Add("AuthorizationStatus", statusMessage);
            }
            else
            {
                if (_validRoles == null || _validRoles.Contains(token.Role))
                    workItem.Headers.Add("AuthorizationStatus", HttpStatusCode.Accepted.ToString());
                else
                    workItem.Headers.Add("AuthorizationStatus", "Forbidden");
            }
            return base.OnExecutingAsync(executingContext, cancellationToken);
        }


    }

}
