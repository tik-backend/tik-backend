﻿using JWT;
using JWT.Algorithms;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Tik.BusinessService.Interfaces;
using Tik.DomainModels;

namespace Tik.BusinessService
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IJwtAlgorithm _algorithm;
        private readonly IBase64UrlEncoder _base64Encoder;

        public AuthenticationService()
        {
            _algorithm = new HMACSHA256Algorithm();
            _base64Encoder = new JwtBase64UrlEncoder();
        }

        public AuthToken IssueTokenForUser(UserEntity userEntity, CompanyEntity company)
        {
            string SessionTimeOut = System.Environment.GetEnvironmentVariable($"SessionTimeOutMinuntes", EnvironmentVariableTarget.Process);
            string key = Constants.SECRET_KEY;
            var issuer = Constants.Issuer;
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var signCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            DateTime expiry = DateTime.UtcNow.AddMinutes(Convert.ToInt32(SessionTimeOut));
            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            new[] { new Claim("Id", userEntity.Id), new Claim("CompanyId", userEntity.CompanyId), new Claim("Role", userEntity.EmployeeRole), new Claim("Category", company.CategoryName) },
                            expires: expiry,
                            signingCredentials: signCredentials);

            return new AuthToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                ExpiresAt = expiry,

            };
        }

        public string GenerateRefreshToken()
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return Convert.ToBase64String(randomBytes);
            }
        }

        public AuthToken ValidateAuthenticationToken(HttpRequestMessage req)
        {
            AuthToken validationPackage = new AuthToken();
            AuthenticationHeaderValue jwtInput = req.Headers.Authorization;
            if (jwtInput != null)
            {
                String jwt = "";
                if (jwtInput.ToString().StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
                {
                    jwt = jwtInput.ToString().Substring("Bearer ".Length).Trim();
                }
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                try
                {
                    validationPackage = ExtractClaims(jwt, handler);
                }
                catch (Exception ex)
                {
                    validationPackage.ValidToken = false;
                }
            }
            return validationPackage;
        }
        private AuthToken ExtractClaims(string jwt, JwtSecurityTokenHandler handler)
        {
            var mySecret = Constants.SECRET_KEY;
            var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));
            AuthToken authToken = new AuthToken();
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                tokenHandler.ValidateToken(jwt, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = Constants.Issuer,
                    ValidAudience = Constants.Issuer,
                    IssuerSigningKey = mySecurityKey,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                authToken.ValidToken = true;
                var jwtToken = (JwtSecurityToken)validatedToken;
                authToken.Id = jwtToken.Claims.Where(x => x.Type == "Id").FirstOrDefault().Value;
                authToken.Role = jwtToken.Claims.Where(x => x.Type == "Role").FirstOrDefault().Value;
                authToken.CompanyId = jwtToken.Claims.Where(x => x.Type == "CompanyId").FirstOrDefault().Value;
                authToken.Category = jwtToken.Claims.Where(x => x.Type == "Category").FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("the token is expired"))
                    authToken.IsExpired = true;

                authToken.Error = ex.Message;
                authToken.ValidToken = false;
                return authToken;
            }
            return authToken;
        }

        public AuthenticationStatus GetAuthenticationStatus(HttpHeaders header)
        {
            IEnumerable<string> authStatus;
            if (header.TryGetValues("AuthorizationStatus", out authStatus))
            { 
                string status = authStatus.FirstOrDefault();
                if (status == HttpStatusCode.Accepted.ToString())
                {
                    return new AuthenticationStatus
                    {
                        Code = HttpStatusCode.Accepted,
                        Message = status
                    };
                }
                else if(status == HttpStatusCode.Forbidden.ToString())
                {
                    return new AuthenticationStatus
                    {
                        Code = HttpStatusCode.Forbidden,
                        Message = "You don't have access to view this page. Please contact your administrator"
                    };
                }
                else
                {
                    return new AuthenticationStatus
                    {
                        Code = HttpStatusCode.Unauthorized,
                        Message = status

                    };
                }
            }
            return new AuthenticationStatus
            {
                Code = HttpStatusCode.Forbidden,
                Message = "Unauthorized"

            };
        }
    }
}

