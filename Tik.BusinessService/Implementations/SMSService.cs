﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using System.Linq;
using Tik.BusinessService.Helper;
using Tik.BusinessService.Interfaces;

namespace Tik.BusinessService.Implementations
{
    public class SMSService: ISMSService
    {
        private const string CONST_ISD_CODE = "+91";
        public void SendSMS(List<UserEntity> users, List<SMSTemplateEntity> smsTemplate, IDictionary<string, string> defaultParameter)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var accountSid = System.Environment.GetEnvironmentVariable($"SMSSID", EnvironmentVariableTarget.Process);
            var authToken = System.Environment.GetEnvironmentVariable($"SMSAuthToken", EnvironmentVariableTarget.Process);
            var messagingServiceSid = System.Environment.GetEnvironmentVariable($"MessageServiceSid", EnvironmentVariableTarget.Process);
            TwilioClient.Init(accountSid, authToken);
            string message = "";
            foreach (UserEntity user in users)
            {
                try
                {
                    bool isCompany = Common.CheckIsCompanyUser(user.Category);
                    if (isCompany)
                        message = smsTemplate.Where(x => x.IsCompany == true).FirstOrDefault()?.Message;
                    else
                        message = smsTemplate.Where(x => x.IsMill == true).FirstOrDefault()?.Message;

                    message = AppendDefaultParameter(message, defaultParameter);
                    var result = MessageResource.Create(
                      to: new Twilio.Types.PhoneNumber(CONST_ISD_CODE + user.PhoneNumber),
                       messagingServiceSid: messagingServiceSid,
                       body: message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        private string AppendDefaultParameter(string message, IDictionary<string, string> defaultParameters)
        {
            return defaultParameters.Aggregate(message, (current, value) =>   current.Replace(value.Key, value.Value));
        }
    }
}
