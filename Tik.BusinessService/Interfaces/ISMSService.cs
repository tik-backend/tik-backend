﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.BusinessService.Interfaces
{
    public interface ISMSService
    {
        void SendSMS(List<UserEntity> users, List<SMSTemplateEntity> smsTemplate, IDictionary<string, string> defaultParameter);
    }
}
