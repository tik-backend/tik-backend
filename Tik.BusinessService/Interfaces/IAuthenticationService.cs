﻿using System.Net.Http;
using System.Net.Http.Headers;
using Tik.DomainModels;

namespace Tik.BusinessService.Interfaces
{
    public interface IAuthenticationService
    {
        AuthToken IssueTokenForUser(UserEntity userEntity, CompanyEntity company);
        AuthToken ValidateAuthenticationToken(HttpRequestMessage req);
        AuthenticationStatus GetAuthenticationStatus(HttpHeaders header);
        string GenerateRefreshToken();
    }
}
