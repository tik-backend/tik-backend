﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.CommonEntity;

namespace Tik.BusinessService.Helper
{
    public static class Common
    {
        public static bool CheckIsCompanyUser(string category)
        {
            return category == Category.Company.ToString();
        }
    }
}
