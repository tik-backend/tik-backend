﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tik.BusinessService.Helper
{
    public class SmartCodes
    {
        [Required]
        [MaxLength(480)]
        public string Meaning { get; set; }
        [Required]
        [MaxLength(2)]
        public string SmartCode { get; set; }
        [Required]
        [MaxLength(120)]
        public string SmartType { get; set; }
        public int SmartValue { get; set; }
        public SmartCodes() { }

        protected SmartCodes(String meaning, String smartCode, String smartType, int smartValue)
        {
            if (meaning == null)
            {
                meaning = "None";
                smartCode = null;
                smartType = null;
            }
            Meaning = meaning;
            SmartCode = smartCode;
            SmartType = smartType;
            SmartValue = smartValue;
        }
        protected SmartCodes(String meaning, String smartCode, String smartType) : this(meaning, smartCode, smartType, 0)
        { }

        public override int GetHashCode()
        {
            return SmartCode.GetHashCode() ^ Meaning.GetHashCode();
        }
        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;
            SmartCodes sc = (SmartCodes)obj;
            return SmartCode.Equals(sc.SmartCode) && Meaning.Equals(sc.Meaning);
        }
    }

    public sealed class ForgetPassword : SmartCodes
    {
        public ForgetPassword() { }
        private ForgetPassword(String meaning, String smartCode) : base(meaning, smartCode, "ForgetPassword") { }
        public static ForgetPassword IA { get { return new ForgetPassword("InActive", "IA"); } }
        public static ForgetPassword A { get { return new ForgetPassword("Active", "A"); } }
    }

    public sealed class AssignRole : SmartCodes
    {
        public AssignRole() { }
        private AssignRole(String meaning, String smartCode, int smartValue) : base(meaning, smartCode, "AssignRole", smartValue) { }
        public static AssignRole MA { get { return new AssignRole("MasterAdmin", "MA", 1); } }
        public static AssignRole AA { get { return new AssignRole("Approval Admin", "AA", 2); } }
        public static AssignRole M { get { return new AssignRole("Merchandiser", "M", 3); } }
        public static AssignRole QI { get { return new AssignRole("Quote InCharge", "QI", 4); } }

        public static AssignRole GetRole(string role)
        {
            AssignRole assignRole = AssignRole.QI;
            switch (role)
            {
                case "MasterAdmin":
                    assignRole = AssignRole.MA;
                    break;
                case "Approval Admin":
                    assignRole = AssignRole.AA;
                    break;
                case "Merchandiser":
                    assignRole = AssignRole.M;
                    break;
                case "Quote InCharge":
                    assignRole = AssignRole.QI;
                    break;
            }

            return assignRole;
        }
    }

    public sealed class ReviewStatus : SmartCodes
    {
        public ReviewStatus() { }
        private ReviewStatus(String meaning, String smartCode) : base(meaning, smartCode, "Status") { }
        public static ReviewStatus R { get { return new ReviewStatus("Reviewed", "R"); } }
        public static ReviewStatus NR { get { return new ReviewStatus("NotReviewed", "NR"); } }
        public static ReviewStatus C { get { return new ReviewStatus("Completed", "C"); } }
    }
}
