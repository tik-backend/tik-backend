﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Tik.BusinessService.Helper
{
    public static class TimeZoneHelper
    {
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        public static string ConvertDateTimetoIST(DateTime utc)
        {
            if (utc == null)
                return "";

            DateTime date = DateTime.SpecifyKind(utc, DateTimeKind.Unspecified);
            DateTime dateTime = TimeZoneInfo.ConvertTime(date, INDIAN_ZONE);
            return dateTime.ToString("dd-MM-yyyy hh:mm");
        }
    }
}
