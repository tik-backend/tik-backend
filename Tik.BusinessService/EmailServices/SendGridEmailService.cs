﻿using Tik.DomainModels;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Text;


namespace Tik.BusinessService.EmailServices
{
    public class SendGridEmailService : IEmailService
    {
        private readonly SendGridEmailSettings _sendGridEmailSettings;

        private string FromEmail => _sendGridEmailSettings.From;
        private string UserName => _sendGridEmailSettings.Username;
        private string Password => _sendGridEmailSettings.Password;
        private string Host => _sendGridEmailSettings.Host;
        private int Port => _sendGridEmailSettings.Port;

        public SendGridEmailService(IOptions<SendGridEmailSettings> sendGridEmailSettings)
        {
            _sendGridEmailSettings = sendGridEmailSettings.Value ?? throw new ArgumentNullException(nameof(sendGridEmailSettings));
        }
        public Task SendEmailAsync(string toEmail, string tempPassword, EmailFormatEntity emailFormat)
        {
            MailAddress fromAddress = new MailAddress(FromEmail, "TIK");
            MailAddress toAddress = new MailAddress(toEmail);
            MailMessage message = new MailMessage(fromAddress, toAddress);
            message.Subject = emailFormat.MailSubject;
            string mailbody = emailFormat.MailBody + " <b> " + tempPassword + " </b><br/><br/> " + emailFormat.MailFooter + " <br/> " + emailFormat.Signature;
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient(Host, Port);
            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(UserName, Password);
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential;
            client.Send(message);
            return Task.CompletedTask;
        }
    }
}
