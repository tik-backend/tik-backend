﻿using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.BusinessService.EmailServices
{
    public interface IEmailService
    {
        Task SendEmailAsync(string toEmail, string tempPassword, EmailFormatEntity emailFormat);
    }
}
