using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Tik.DomainModels;
using System.Collections.Generic;
using Tik.DomainModels.ViewModels;
using Tik.Repository.EmployeeRepository;
using Tik.DomainModels.Validators;
using Tik.BusinessService.Interfaces;
using Tik.BusinessService;
using System.Net;
using Tik.Repository;
using Newtonsoft.Json;

namespace Tik.Employee
{
    public class EmployeeFunc
    {
        private readonly IAuthenticationService _tokenIssuer;
        private readonly IEmployeeRepository _employeeRepo;
        private readonly IUserRepository _userRepo;

        public EmployeeFunc(IAuthenticationService tokenIssuer, IEmployeeRepository employeeRepo, IUserRepository userRepo)
        {
            this._tokenIssuer = tokenIssuer ?? throw new ArgumentNullException(nameof(tokenIssuer));
            this._employeeRepo = employeeRepo ?? throw new ArgumentNullException(nameof(employeeRepo));
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
        }

        [FunctionName("GetEmployee")]
        public async Task<IActionResult> GetEmployee(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequest req, ILogger log)
        {
            try
            {
                string employeeId = req.Query["employeeId"];
                var employee = await _employeeRepo.GetItemById(employeeId);

                if (employee != null)
                {
                    EmployeeViewModel employeeViewModel = new EmployeeViewModel
                    {
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        Gender = employee.Gender,
                        EmployeeRole = employee.EmployeeRole,
                        EmailAddress = employee.EmailAddress,
                        PhoneNumber = employee.PhoneNumber,
                        Address = employee.AddressDetails.Address,
                        Category = employee.Category,
                        ApprovalAdminAccess = employee.ApprovalAdminAccess,
                        StatusList = employee.Notification?.StatusList,
                        IsSMS = employee.Notification?.IsSMS,
                        IsWhatsApp = employee.Notification?.IsWhatsApp,
                        IsEmail = employee.Notification?.IsEmail
                    };
                    return new JsonResult(employeeViewModel);
                }
                else
                {
                    return new BadRequestObjectResult("Employee detail is not available.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize]
        [FunctionName("GetEmployees")]
        public async Task<IActionResult> GetEmployees(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(request);
                string companyId = accessToken.CompanyId;

                string id = req.Query["id"];
                var user = await _employeeRepo.GetItemById(id);
                if (user == null)
                    return new BadRequestObjectResult("User not found.");
                else
                {
                    List<UserEntity> employees = await _employeeRepo.GetEmployeesById(companyId);
                    List<EmployeeViewModel> vmEmployees = new List<EmployeeViewModel>();
                    employees.ForEach(e => vmEmployees.Add(new EmployeeViewModel()
                    {
                        FirstName = e.FirstName,
                        LastName = e.LastName,
                        Gender = e.Gender,
                        EmployeeRole = e.EmployeeRole,
                        EmailAddress = e.EmailAddress,
                        PhoneNumber = e.PhoneNumber,
                        Address = e.AddressDetails.Address,
                        Category = e.Category,
                        ApprovalAdminAccess = e.ApprovalAdminAccess,
                        EmployeeId = e.Id,
                        FullName = e.FullName
                    }));

                    return new JsonResult(vmEmployees);
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin")]
        [FunctionName("CreateEmployee")]
        public async Task<IActionResult> CreateEmployee(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage request, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var result = new ObjectResult(status.Message);
                    result.StatusCode = Convert.ToInt32(status.Code);
                    return result;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(request);
                string companyId = accessToken.CompanyId;

                var empDetails = await request.GetJsonBody<UserEntity, EmployeeValidator>();
                if (!empDetails.IsValid)
                {
                    return empDetails.ToBadRequest();
                }

                bool checkEmployee = await _employeeRepo.EmployeeExists(empDetails.Value.EmailAddress);
                if (!checkEmployee)
                {
                    return new BadRequestObjectResult("Employee already exists.");
                }

                var adrsDetails = await request.GetJsonBody<AddressDetails, UserAddressValidator>();
                if (string.IsNullOrEmpty(adrsDetails.Value.Address))
                    return new BadRequestObjectResult("Address is required.");
                empDetails.Value.AddressDetails = new AddressDetails()
                {
                    Address = adrsDetails.Value.Address
                };

                if (empDetails.Value.ApprovalAdminAccess == null)
                    return new BadRequestObjectResult("Approval Admin access is required.");

                string jsonContent = await request.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<BidStatusNotification>(jsonContent);

                if (data != null && data.StatusList != null && data.StatusList.Count > 0)
                {
                    empDetails.Value.Notification = new BidStatusNotification()
                    {
                        StatusList = data.StatusList,
                        IsSMS = data.IsSMS,
                        IsEmail = data.IsEmail,
                        IsWhatsApp = data.IsWhatsApp
                    };
                }
                else
                    empDetails.Value.Notification = null;



                bool isSuccess = await _employeeRepo.CreateEmployee(empDetails.Value, companyId);
                if (!isSuccess)
                {
                    return new BadRequestObjectResult("Employee already exists.");
                }
                else
                {
                    return new OkObjectResult("Employee created successfully.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin")]
        [FunctionName("EditEmployee")]
        public async Task<IActionResult> EditEmployee(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage request, HttpRequest req, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(request.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var objResult = new ObjectResult(status.Message);
                    objResult.StatusCode = Convert.ToInt32(status.Code);
                    return objResult;
                }

                var accessToken = _tokenIssuer.ValidateAuthenticationToken(request);
                string companyId = accessToken.CompanyId;

                var empDetails = await request.GetJsonBody<UserEntity, EditEmployeeValidator>();
                if (!string.IsNullOrEmpty(empDetails.Value.Password))
                    if (!empDetails.IsValid)
                    {
                        return empDetails.ToBadRequest();
                    }

                string employeeId = req.Query["EmployeeId"];
                if (string.IsNullOrEmpty(employeeId))
                    return new BadRequestObjectResult("EmployeeId is required.");

                UserEntity employeeDetails = await _employeeRepo.GetItemById(employeeId);
                if (employeeDetails == null)
                    return new BadRequestObjectResult("Employee not found.");

                var emailAddress = empDetails.Value.EmailAddress.ToLower().Trim();
                var result = await _userRepo.GetUserByEMailId(emailAddress);
                if (result != null && result.Id != employeeId)
                    return new BadRequestObjectResult("The EmailId is already associated with an existing User.");

                var adrsDetails = await request.GetJsonBody<AddressDetails, UserAddressValidator>();
                if (string.IsNullOrEmpty(adrsDetails.Value.Address))
                    return new BadRequestObjectResult("Address is required.");
                empDetails.Value.AddressDetails = new AddressDetails()
                {
                    Address = adrsDetails.Value.Address
                };

                string jsonContent = await request.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<BidStatusNotification>(jsonContent);

                if (data != null && data.StatusList != null && data.StatusList.Count > 0)
                {
                    empDetails.Value.Notification = new BidStatusNotification()
                    {
                        StatusList = data.StatusList,
                        IsSMS = data.IsSMS,
                        IsEmail = data.IsEmail,
                        IsWhatsApp = data.IsWhatsApp
                    };
                }
                else
                    empDetails.Value.Notification = null;

                bool isSuccess = await _employeeRepo.EditEmployeeDetails(employeeId, empDetails.Value, companyId);
                if (!isSuccess)
                {
                    return new BadRequestObjectResult("Employee details not updated.");
                }
                else
                {
                    return new OkObjectResult("Employee details updated successfully...");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }

        [FunctionAuthorize("MasterAdmin", "SuperAdmin")]
        [FunctionName("DeleteEmployee")]
        public async Task<IActionResult> DeleteEmployee(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage reqMsg, HttpRequest request, ILogger log)
        {
            try
            {
                AuthenticationStatus status = _tokenIssuer.GetAuthenticationStatus(reqMsg.Headers);
                if (status.Code != HttpStatusCode.Accepted)
                {
                    var objResult = new ObjectResult(status.Message);
                    objResult.StatusCode = Convert.ToInt32(status.Code);
                    return objResult;
                }

                string employeeId = request.Query["EmployeeId"];

                if (string.IsNullOrEmpty(employeeId))
                    return new BadRequestObjectResult("EmployeeId is required.");

                UserEntity employee = await _employeeRepo.GetItemById(employeeId);
                if (employee == null)
                    return new BadRequestObjectResult("Employee not found");

                bool isSuccess = await _employeeRepo.DeleteEmployeeDetails(employeeId, employee);
                if (!isSuccess)
                {
                    return new BadRequestObjectResult("Something went wrong.");
                }
                else
                {
                    return new OkObjectResult("Employee details deleted successfully.");
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}