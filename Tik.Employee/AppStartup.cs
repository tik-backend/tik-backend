﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Tik.BusinessService;
using Tik.BusinessService.Interfaces;
using Tik.Repository;
using Tik.Repository.EmployeeRepository;

[assembly: FunctionsStartup(typeof(Tik.Common.AppStartup))]
namespace Tik.Common
{
    public class AppStartup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton<IConfiguration>(configuration);
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserRepository, UserRepository>();

            string conStr = System.Environment.GetEnvironmentVariable($"ConnectionStrings_Tik", EnvironmentVariableTarget.Process);
            string databaseName = System.Environment.GetEnvironmentVariable($"DatabaseName", EnvironmentVariableTarget.Process);

            services.AddCosmosDb(conStr, databaseName, "");
        }
    }
}
