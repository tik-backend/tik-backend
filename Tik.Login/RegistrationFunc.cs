using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;
using Tik.DomainModels.Validators;
using Tik.DomainModels.ViewModels;
using Tik.Repository;
using static Tik.DomainModels.CompanyEntity;

namespace Tik.Login
{
    public class RegistrationFunc
    {
        private readonly IUserRepository _repo;

        public RegistrationFunc(IUserRepository repo)
        {
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }

        [FunctionName("Registration")]
        public async Task<IActionResult> Registration(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage req, ILogger log)
        {
            try
            {
                var userDetails = await req.GetJsonBody<UserEntity, UserValidator>();
                if (!userDetails.IsValid)
                {
                    return userDetails.ToBadRequest();
                }

                var adrsDetails = await req.GetJsonBody<AddressDetails, UserAddressValidator>();
                if (!adrsDetails.IsValid)
                {
                    return adrsDetails.ToBadRequest();
                }

                var companyDetails = await req.GetJsonBody<CompanyEntity, UserCompanyValidator>();
                if (!companyDetails.IsValid)
                {
                    return companyDetails.ToBadRequest();
                }
                
                string jsonContent = await req.Content.ReadAsStringAsync();

                var validateuser = JsonConvert.DeserializeObject<UserViewModel>(jsonContent);
                if (string.IsNullOrEmpty(validateuser.ConfirmPassword))
                    return new BadRequestObjectResult("Confirm Password is Required.");
                if (validateuser.ConfirmPassword != userDetails.Value.Password)
                {
                    return new BadRequestObjectResult("Password Does not Match.");
                }

                UserEntity data = await _repo.GetUserByEMailId(userDetails.Value.EmailAddress.ToLower().Trim());
                if (data != null)
                {
                    return new BadRequestObjectResult("User Already Exists.");
                }

                Category category = (Category)Convert.ToInt32(companyDetails.Value.CategoryName);
                companyDetails.Value.CategoryName = category.ToString();

                bool isCompanyExists = await _repo.RegisterCompany(userDetails.Value, companyDetails.Value);
                if (!isCompanyExists)
                {
                    return new BadRequestObjectResult("Company Already Exists.");
                }

                userDetails.Value.AddressDetails = new AddressDetails()
                {
                    Address = adrsDetails.Value.Address,
                    ApartmentUnitOffice = adrsDetails.Value.ApartmentUnitOffice,
                    CountryId = adrsDetails.Value.CountryId,
                    StateId = adrsDetails.Value.StateId,
                    CityId = adrsDetails.Value.CityId,
                    ZipCode = adrsDetails.Value.ZipCode
                };

                userDetails.Value.Category = category.ToString();
                await _repo.RegisterUser(userDetails.Value);
                return new OkObjectResult("User Registered Succesfully.");
            }
            catch (Exception ex)
            {
                log.LogError($"Internal Server Error. Exception: {ex.Message}");
                return new BadRequestObjectResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}