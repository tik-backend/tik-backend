using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using Tik.BusinessService.Interfaces;
using Tik.Repository;
using Tik.DomainModels;

namespace Tik.Login
{
    public class GetAccessTokenFunc
    {
        private readonly IAuthenticationService _tokenIssuer;
        private readonly IUserRepository _repo;
        private readonly ICompanyRepository _companyRepo;
        public GetAccessTokenFunc(IAuthenticationService tokenIssuer, IUserRepository repo, ICompanyRepository companyRepo)
        {
            _tokenIssuer = tokenIssuer;
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
        }

        [FunctionName("GetAccessToken")]
        public async Task<IActionResult> GetAccessToken(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage req,
            ILogger log)
        {
            try
            {
                string jsonContent = await req.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<UserEntity>(jsonContent);

                if (string.IsNullOrEmpty(data.Id))
                    return new BadRequestObjectResult("Please enter the Id");

                if (string.IsNullOrEmpty(data.RefreshToken))
                    return new BadRequestObjectResult("please enter refresh token");

                var user = await _repo.GetItemById(data.Id);
                if (user != null)
                {
                    bool validToken = await _repo.ValidateRefreshToken(user.Id, data.RefreshToken);
                    if (validToken)
                    {
                        CompanyEntity company = await _companyRepo.GetItemById(user.CompanyId);
                        AuthToken authToken = _tokenIssuer.IssueTokenForUser(user, company);
                        return new JsonResult(new
                        {
                            accessToken = authToken.Token,
                            id = user.Id,
                            role = user.EmployeeRole,
                            expiry = authToken.ExpiresAt,
                            companyId = user.CompanyId,
                            category = company.CategoryName,
                            ApprovalAdminAccess = user.ApprovalAdminAccess == null ? "0" : user.ApprovalAdminAccess
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogError("Eror creating access token {0} {1} ", ex.Message, ex.StackTrace);
            }

            return new BadRequestObjectResult("Invalid refresh token");
        }
    }
}
