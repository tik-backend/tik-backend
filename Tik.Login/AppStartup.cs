﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Tik.BusinessService;
using Tik.BusinessService.EmailServices;
using Tik.BusinessService.Interfaces;
using Tik.DomainModels;
using Tik.Repository;
using Tik.Repository.CommonRepository;

[assembly: FunctionsStartup(typeof(Tik.Login.AppStartup))]
namespace Tik.Login
{
    public class AppStartup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            services.AddSingleton<IAuthenticationService, AuthenticationService>();

            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ICompanyRepository, CompanyRepository>();

            services.AddScoped<IForgetPasswordRepository, ForgetPasswordRepository>();

            services.AddScoped<IEmailFormatRepository, EmailFormatRepository>();

            services.AddScoped<IRoleRepository, RoleRepository>();

            services.Configure<SendGridEmailSettings>(configuration.GetSection("SendGridEmailSettings"));
            services.AddScoped<IEmailService, SendGridEmailService>();

            string conStr = System.Environment.GetEnvironmentVariable($"ConnectionStrings_Tik", EnvironmentVariableTarget.Process);
            string databaseName = System.Environment.GetEnvironmentVariable($"DatabaseName", EnvironmentVariableTarget.Process);          

            services.AddCosmosDb(conStr, databaseName, "");
        }
    }
}
