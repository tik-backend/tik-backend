using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Tik.BusinessService.Interfaces;
using Tik.DomainModels;
using Tik.Repository;
using Tik.Repository.CommonRepository;

namespace Tik.Login
{
    public class AuthenticationFunc
    {
        private readonly IAuthenticationService _tokenIssuer;
        private readonly IUserRepository _repo;
        private readonly ICompanyRepository _companyRepo;
        private readonly IForgetPasswordRepository _forgetPasswordRepo;

        public AuthenticationFunc(IAuthenticationService tokenIssuer, IUserRepository repo, IForgetPasswordRepository forgetPasswordRepo, ICompanyRepository companyRepo)
        {
            _tokenIssuer = tokenIssuer;
            this._repo = repo ?? throw new ArgumentNullException(nameof(repo));
            this._forgetPasswordRepo = forgetPasswordRepo ?? throw new ArgumentNullException(nameof(forgetPasswordRepo));
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
        }

        [FunctionName("Authenticate")]
        public async Task<IActionResult> Authenticate(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "options")] HttpRequestMessage req, ILogger log)
        {
            string jsonContent = await req.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<UserEntity>(jsonContent);
            string RefreshTokenTimeOut = System.Environment.GetEnvironmentVariable($"RefreshTokenTimeOut", EnvironmentVariableTarget.Process);

            if (string.IsNullOrEmpty(user.EmailAddress) || string.IsNullOrEmpty(user.Password))
                return new BadRequestObjectResult("Username and Password is mandatory.");

            var userData = await _repo.CheckCredentials(user.EmailAddress, user.Password);
            if (userData != null)
            {
                CompanyEntity company = await _companyRepo.GetItemById(userData.CompanyId);
                AuthToken authToken = _tokenIssuer.IssueTokenForUser(userData, company);
                string refreshToken = _tokenIssuer.GenerateRefreshToken();
                _repo.UpdateRecord(refreshToken, DateTime.Now.AddDays(Convert.ToInt32(RefreshTokenTimeOut)), user.EmailAddress);
                return new JsonResult(new { accessToken = authToken.Token, refreshToken = refreshToken, id = userData.Id, role = userData.EmployeeRole, expiry = authToken.ExpiresAt, companyId = userData.CompanyId, category = company.CategoryName, ApprovalAdminAccess = userData.ApprovalAdminAccess == null ? "0" : userData.ApprovalAdminAccess });
            }

            var forgetData = await _forgetPasswordRepo.GetUserByEMailIdAndPassword(user.EmailAddress.ToLower().Trim(), user.Password);
            if (forgetData != null)
            {
                return new JsonResult(new { userId = forgetData.Id });
            }
            return new BadRequestObjectResult("Invalid username or password");
        }
    }
}
