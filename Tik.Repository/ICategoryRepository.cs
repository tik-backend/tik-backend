﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface ICategoryRepository : IRepository<CompanyEntity>
    {
        Task<List<CompanyEntity>> GetCategories();
    }
}