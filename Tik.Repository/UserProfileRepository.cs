﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.Repository.CommonRepository;

namespace Tik.Repository
{
    public class UserProfileRepository : CosmosDbRepository<CompanyEntity>, IUserProfileRepository
    {
        private readonly string _azureConnectionString;

        public override string ContainerName { get; } = "Company";

        public override string GenerateId(CompanyEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(CompanyEntity entity) => new PartitionKey(entity.GSTNumber);

        private readonly IUserRepository _userRepo;
        private readonly IReviewRepository _reviewRepo;
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public UserProfileRepository(ICosmosDbContainerFactory cosmosDbContainerFactory, IUserRepository userRepo, IReviewRepository reviewRepo) : base(cosmosDbContainerFactory)
        {
            _azureConnectionString = cosmosDbContainerFactory.GetBlobConnectionString();
            this._userRepo = userRepo;
            this._reviewRepo = reviewRepo;
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<bool> RegisterUserProfile(string id, CompanyEntity companyEntity, UserEntity user, IFormFile file)
        {
            if (file != null)
            {
                var container = new BlobContainerClient(_azureConnectionString, "image");
                var createResponse = await container.CreateIfNotExistsAsync();
                if (createResponse != null && createResponse.GetRawResponse().Status == 201)
                    await container.SetAccessPolicyAsync(PublicAccessType.Blob);
                string extension = Path.GetExtension(file.FileName);
                var blob = container.GetBlobClient(file.FileName.Replace(file.FileName, Guid.NewGuid().ToString() + extension));
                using (var fileStream = file.OpenReadStream())
                {
                    await blob.UploadAsync(fileStream, new BlobHttpHeaders { ContentType = file.ContentType });
                }
                companyEntity.ImageUrl = blob.Uri.ToString();
                companyEntity.ImageId = Guid.NewGuid().ToString();
                companyEntity.ImageName = blob.Name;
            }

            if (companyEntity == null)
            {
                await AddItemAsync(companyEntity);
            }
            else
            {
                companyEntity.UpdatedDate = DateTime.Now;
                await UpdateItemAsync(companyEntity.Id, companyEntity);
            }
            return true;
        }

        public byte[] GetImageBytes(string usrImgName)
        {
            var container = new BlobContainerClient(_azureConnectionString, "image");
            var blob = container.GetBlobClient(usrImgName);
            byte[] imageBytes = blob.DownloadContent().Value.Content.ToArray();
            return imageBytes;
        }

        public async Task EditUserAddress(string id, UserEntity user)
        {
            UserEntity data = await _userRepo.GetItemById(id);
            if (data != null)
            {
                data.AddressDetails.Address = user.AddressDetails.Address;
                data.PhoneNumber = user.PhoneNumber;
                data.AddressDetails.PhoneNumber2 = user.AddressDetails.PhoneNumber2;
                data.UpdatedDate = DateTime.Now;
                await _userRepo.UpdateItemAsync(id, data);
            }
        }

        public async Task InsertRatingAverage(string id, decimal average)
        {
            CompanyEntity data = await GetItemById(id);
            if (data != null)
            {
                data.Review = average;
                await UpdateItemAsync(id, data);
            }
        }

        public decimal GetRatingAverage(List<ReviewEntity> totalTransactions)
        {
            decimal sum = 0;
            var count = totalTransactions.Count();
            if (count < 3)
            {
                sum = totalTransactions.Sum(x => x.ReviewRating);
            }
            else if (count > 3)
            {
                int ratingCount = count % 3;
                count = count - ratingCount;
                sum = totalTransactions.Take(count).OrderBy(x => x.CreatedDate).Sum(x => x.ReviewRating);
            }
            var ratingAvg = sum / count;
            return Math.Round(ratingAvg, 2);
        }
    }
}
