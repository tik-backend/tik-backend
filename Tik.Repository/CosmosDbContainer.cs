﻿using Microsoft.Azure.Cosmos;

namespace Tik.Repository
{
    public class CosmosDbContainer : ICosmosDbContainer
    {
        public Microsoft.Azure.Cosmos.Container _container { get; }

        public CosmosDbContainer(CosmosClient cosmosClient,
                                 string databaseName,
                                 string containerName)
        {
            this._container = cosmosClient.GetContainer(databaseName, containerName);
        }
    }
}
