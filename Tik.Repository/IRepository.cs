﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetItemsAsync(string query);
        Task<T> GetItemAsync(string id, T item);
        Task AddItemAsync(T item);
        Task UpdateItemAsync(string id, T item);
        Task DeleteItemAsync(string id, T item);
        Task<bool> IsRecordExists(string queryString);
        Task<T> GetItemById(string Id);
        Task<IEnumerable<T>> GetItemListByIds(string[] Ids);
    }
}
