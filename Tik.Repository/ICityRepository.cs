﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface ICityRepository : IRepository<CityEntity>
    {
        Task<List<CityEntity>> GetCities(string stateId);
    }
}