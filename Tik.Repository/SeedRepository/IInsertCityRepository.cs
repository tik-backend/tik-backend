﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface IInsertCityRepository : IRepository<CityEntity>
    {
        Task<bool> UploadData(CityEntity cityEntity);
    }
}
