﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface IInsertStateRepository : IRepository<StateEntity>
    {
        Task<bool> UploadData(StateEntity stateEntity);
    }
}
