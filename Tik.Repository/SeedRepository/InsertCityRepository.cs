﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public class InsertCityRepository : CosmosDbRepository<CityEntity>, IInsertCityRepository
    {
        public override string ContainerName { get; } = "City";

        public override string GenerateId(CityEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(CityEntity entity) => new PartitionKey(entity.CityName);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public InsertCityRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<bool> UploadData(CityEntity cityEntity)
        {
            await AddItemAsync(cityEntity);
            return true;
        }
    }
}
