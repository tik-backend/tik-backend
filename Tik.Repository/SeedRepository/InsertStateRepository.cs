﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public class InsertStateRepository : CosmosDbRepository<StateEntity>, IInsertStateRepository
    {
        public override string ContainerName { get; } = "StateNew";

        public override string GenerateId(StateEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(StateEntity entity) => new PartitionKey(entity.StateName);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public InsertStateRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<bool> UploadData(StateEntity stateEntity)
        {
            await AddItemAsync(stateEntity);
            return true;
        }
    }
}
