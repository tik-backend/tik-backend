﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface IUserProfileRepository : IRepository<CompanyEntity>
    {
        Task<bool> RegisterUserProfile(string id, CompanyEntity companyEntity, UserEntity user, IFormFile file);
        public byte[] GetImageBytes(string usrImgName);
        Task EditUserAddress(string id, UserEntity user);        
        Task InsertRatingAverage(string id, decimal average);
        public decimal GetRatingAverage(List<ReviewEntity> totalTransactions);
    }
}
