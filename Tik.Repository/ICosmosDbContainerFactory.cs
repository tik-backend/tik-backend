﻿using Microsoft.Azure.Cosmos;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface ICosmosDbContainerFactory
    {
        ICosmosDbContainer GetContainer(string containerName);

        Task EnsureDbSetupAsync();

        public string GetBlobConnectionString();
    }

    public interface IContainerContext<T> where T : BaseEntity
    {
        string ContainerName { get; }
        string GenerateId(T entity);
        PartitionKey ResolvePartitionKey(T entity);
    }
}
