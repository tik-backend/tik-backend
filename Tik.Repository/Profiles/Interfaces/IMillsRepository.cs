﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;

namespace Tik.Repository.Profiles.Interfaces
{
    public interface IMillsRepository : IRepository<MillsDomainModel>
    {
        Task AddMills(MillsDomainModel mills);
        Task<List<MillsDomainModel>> GetMillsByCompanyId(string companyId);
    }
}
