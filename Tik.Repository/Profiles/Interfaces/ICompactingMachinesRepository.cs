﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;

namespace Tik.Repository.Profiles.Interfaces
{
    public interface ICompactingMachinesRepository : IRepository<CompactingMachinesDomainModel>
    {
        Task<List<CompactingMachinesDomainModel>> GetCompactingMachinesByCategory(string category);
    }
}
