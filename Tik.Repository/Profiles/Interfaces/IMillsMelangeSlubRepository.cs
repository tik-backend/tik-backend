﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;

namespace Tik.Repository.Profiles.Interfaces
{
    public interface IMillsMelangeSlubRepository : IRepository<MelanSlubTypeTemplateModel>
    {
        Task<List<MelanSlubTypeTemplateModel>> GetMelanSlubTypes(string Category);
    }
}
