﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;

namespace Tik.Repository.Profiles.Interfaces
{
    public interface IDyeingDipStrentnerRepository : IRepository<DyeingDipStrenterDomainModel>
    {
        Task<List<DyeingDipStrenterDomainModel>> GetDyeingDipStrentner();
    }
}
