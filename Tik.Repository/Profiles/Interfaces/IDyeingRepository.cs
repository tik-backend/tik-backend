﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;

namespace Tik.Repository.Profiles.Interfaces
{
    public interface IDyeingRepository : IRepository<DyeingDomainModel>
    {
        Task AddDyeing(DyeingDomainModel dyeing);
        Task<List<DyeingDomainModel>> GetDyeingByCompanyId(string companyId);
    }
}
