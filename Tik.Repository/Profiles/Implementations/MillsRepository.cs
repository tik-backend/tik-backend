﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class MillsRepository : CosmosDbRepository<MillsDomainModel>, IMillsRepository
    {
        public override string ContainerName { get; } = "Mills";

        public override string GenerateId(MillsDomainModel entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(MillsDomainModel entity) => new PartitionKey(entity.Id);
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public MillsRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task AddMills(MillsDomainModel newMillItems)
        {
            List<MillsDomainModel> existingMillItems = await GetMillsByCompanyId(newMillItems.CompanyId);

            foreach (var existingItem in existingMillItems)
                await DeleteItemAsync(existingItem.Id, existingItem);

            await AddItemAsync(newMillItems);
        }

        public async Task<List<MillsDomainModel>> GetMillsByCompanyId(string companyId)
        {
            var feedIterator = _container.GetItemLinqQueryable<MillsDomainModel>().Where(x => x.CompanyId.ToLower() == companyId.ToLower()).ToFeedIterator();

            List<MillsDomainModel> millItems = new List<MillsDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                millItems.AddRange(res.ToList());
            }
            return millItems;
        }
    }
}

