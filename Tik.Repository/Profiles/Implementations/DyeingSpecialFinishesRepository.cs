﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class DyeingSpecialFinishesRepository : CosmosDbRepository<DyeingSpecialFinishesDomainModel>, IDyeingSpecialFinishesRepository
    {
        public override string ContainerName { get; } = "DyeingSpecialFinishes";

        public override string GenerateId(DyeingSpecialFinishesDomainModel entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(DyeingSpecialFinishesDomainModel entity) => new PartitionKey(entity.Id);



        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public DyeingSpecialFinishesRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<DyeingSpecialFinishesDomainModel>> GetDyeingSpecialFinishesByCategory(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<DyeingSpecialFinishesDomainModel>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<DyeingSpecialFinishesDomainModel> dyeingSpecialFinishes = new List<DyeingSpecialFinishesDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                dyeingSpecialFinishes.AddRange(res.ToList());
            }
            return dyeingSpecialFinishes;
        }
    }
}

