﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class DyeingDipStrentnerRepository : CosmosDbRepository<DyeingDipStrenterDomainModel>, IDyeingDipStrentnerRepository
    {
        public override string ContainerName { get; } = "DyeingDipStrentner";

        public override string GenerateId(DyeingDipStrenterDomainModel entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(DyeingDipStrenterDomainModel entity) => new PartitionKey(entity.Id);
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public DyeingDipStrentnerRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<DyeingDipStrenterDomainModel>> GetDyeingDipStrentner()
        {
            var feedIterator = _container.GetItemLinqQueryable<DyeingDipStrenterDomainModel>().ToFeedIterator();

            List<DyeingDipStrenterDomainModel> dipStrentners = new List<DyeingDipStrenterDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                dipStrentners.AddRange(res.ToList());
            }
            return dipStrentners;
        }
    }
}
