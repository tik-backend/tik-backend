﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
   public class DyeingRepository : CosmosDbRepository<DyeingDomainModel>, IDyeingRepository
    {
        public override string ContainerName { get; } = "Dyeing";

        public override string GenerateId(DyeingDomainModel entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(DyeingDomainModel entity) => new PartitionKey(entity.Id);
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public DyeingRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task AddDyeing(DyeingDomainModel newDyeingItems)
        {
            List<DyeingDomainModel> existingDyeingItems = await GetDyeingByCompanyId(newDyeingItems.CompanyId);

            foreach (var existingItem in existingDyeingItems)
                await DeleteItemAsync(existingItem.Id, existingItem);

            await AddItemAsync(newDyeingItems);
        }

        public async Task<List<DyeingDomainModel>> GetDyeingByCompanyId(string companyId)
        {
            var feedIterator = _container.GetItemLinqQueryable<DyeingDomainModel>().Where(x => x.CompanyId.ToLower() == companyId.ToLower()).ToFeedIterator();

            List<DyeingDomainModel> dyeingItems = new List<DyeingDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                dyeingItems.AddRange(res.ToList());
            }
            return dyeingItems;
        }
    }
}
