﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class DyeingDryingProcessessRepository : CosmosDbRepository<DyeingTubularDryingProcessDomainModel>, IDyeingDryingProcessesRepository
    {
        public override string ContainerName { get; } = "DyeingDryingProcesses";

        public override string GenerateId(DyeingTubularDryingProcessDomainModel entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(DyeingTubularDryingProcessDomainModel entity) => new PartitionKey(entity.Id);



        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public DyeingDryingProcessessRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<DyeingTubularDryingProcessDomainModel>> GetDyeingDryingProcessesByCategory(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<DyeingTubularDryingProcessDomainModel>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<DyeingTubularDryingProcessDomainModel> dyeingDryingProcesses = new List<DyeingTubularDryingProcessDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                dyeingDryingProcesses.AddRange(res.ToList());
            }
            return dyeingDryingProcesses;
        }

    }
}
