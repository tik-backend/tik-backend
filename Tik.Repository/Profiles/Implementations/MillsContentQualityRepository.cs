﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class MillsContentQualityRepository : CosmosDbRepository<MillsContentQualityTemplate>, IMillsContentQualityRepository
    {
        public override string ContainerName { get; } = "MillsContentQuality";

        public override string GenerateId(MillsContentQualityTemplate entity) => $"{entity.Category}:{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(MillsContentQualityTemplate entity) => new PartitionKey((entity.Id).Split(':')[0]);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public MillsContentQualityRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<MillsContentQualityTemplate>> GetQualitiesByCategory(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<MillsContentQualityTemplate>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<MillsContentQualityTemplate> millsContentQuality = new List<MillsContentQualityTemplate>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                millsContentQuality.AddRange(res.ToList());
            }
            return millsContentQuality;
        }
    }
}
