﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class FabricStructureRepository : CosmosDbRepository<FabricTypesTemplateModel>, IFabricStructureRepository
    {
        public override string ContainerName { get; } = "FabricStructureTypes";

        public override string GenerateId(FabricTypesTemplateModel entity) => $"{entity.Category}:{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(FabricTypesTemplateModel entity) => new PartitionKey((entity.Id).Split(':')[0]);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;
        public FabricStructureRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<FabricTypesTemplateModel>> GetFabricStructureTypes()
        {
            var feedIterator = _container.GetItemLinqQueryable<FabricTypesTemplateModel>().ToFeedIterator();

            List<FabricTypesTemplateModel> fabricStructureTypes = new List<FabricTypesTemplateModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                fabricStructureTypes.AddRange(res.ToList());
            }
            return fabricStructureTypes;
        }
    }
}
