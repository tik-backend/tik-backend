﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class DyeingProcessesRepository : CosmosDbRepository<DyeingProcessDomainModel>, IDyeingProcessesRepository
    {
        public override string ContainerName { get; } = "DyeingProcesses";
        public override string GenerateId(DyeingProcessDomainModel entity) => $"{entity}:{Guid.NewGuid()}";
        public override PartitionKey ResolvePartitionKey(DyeingProcessDomainModel entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public DyeingProcessesRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<DyeingProcessDomainModel>> GetDyeingProcesses()
        {
            var feedIterator = _container.GetItemLinqQueryable<DyeingProcessDomainModel>().ToFeedIterator();

            List<DyeingProcessDomainModel> dyeingProcessesItems = new List<DyeingProcessDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                dyeingProcessesItems.AddRange(res.ToList());
            }
            return dyeingProcessesItems;
        }
    }
}
