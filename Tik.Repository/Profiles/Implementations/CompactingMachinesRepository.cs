﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class CompactingMachinesRepository : CosmosDbRepository<CompactingMachinesDomainModel>, ICompactingMachinesRepository
    {
        public override string ContainerName { get; } = "CompactingMachines";

        public override string GenerateId(CompactingMachinesDomainModel entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(CompactingMachinesDomainModel entity) => new PartitionKey(entity.Id);
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public CompactingMachinesRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<CompactingMachinesDomainModel>> GetCompactingMachinesByCategory(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<CompactingMachinesDomainModel>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<CompactingMachinesDomainModel> compactingMachines = new List<CompactingMachinesDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                compactingMachines.AddRange(res.ToList());
            }
            return compactingMachines;
        }
    }
}
