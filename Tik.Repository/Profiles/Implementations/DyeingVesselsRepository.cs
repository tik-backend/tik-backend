﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class DyeingVesselsRepository : CosmosDbRepository<DyeingVesselsDomainModel>, IDyeingVesselsRepository
    {
        public override string ContainerName { get; } = "DyeingVessels";
        public override string GenerateId(DyeingVesselsDomainModel entity) => $"{entity}:{Guid.NewGuid()}";
        public override PartitionKey ResolvePartitionKey(DyeingVesselsDomainModel entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public DyeingVesselsRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<DyeingVesselsDomainModel>> GetDyeingVesselSizes()
        {
            var feedIterator = _container.GetItemLinqQueryable<DyeingVesselsDomainModel>().ToFeedIterator();

            List<DyeingVesselsDomainModel> dyeingVessels = new List<DyeingVesselsDomainModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                dyeingVessels.AddRange(res.ToList());
            }
            return dyeingVessels;
        }
    }
}
