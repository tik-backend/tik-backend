﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class SpecialDyedRepository : CosmosDbRepository<SpecialDyedYarnTypeTemplate>, ISpecialDyedRepository
    {
        public override string ContainerName { get; } = "MillsSpecialTypes";

        public override string GenerateId(SpecialDyedYarnTypeTemplate entity) => $"{entity.Category}:{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(SpecialDyedYarnTypeTemplate entity) => new PartitionKey((entity.Id).Split(':')[0]);


        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;


        public SpecialDyedRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<SpecialDyedYarnTypeTemplate>> GetSpecialDyedYarnTypes(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<SpecialDyedYarnTypeTemplate>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<SpecialDyedYarnTypeTemplate> spclDyedTypes = new List<SpecialDyedYarnTypeTemplate>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                spclDyedTypes.AddRange(res.ToList());
            }
            return spclDyedTypes;
        }
    }
}