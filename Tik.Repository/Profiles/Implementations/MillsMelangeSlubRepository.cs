﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;

namespace Tik.Repository.Profiles.Implementations
{
    public class MillsMelangeSlubRepository : CosmosDbRepository<MelanSlubTypeTemplateModel>, IMillsMelangeSlubRepository
    {
        public override string ContainerName { get; } = "MelangeSlubTypes";

        public override string GenerateId(MelanSlubTypeTemplateModel entity) => $"{entity.Category}:{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(MelanSlubTypeTemplateModel entity) => new PartitionKey((entity.Id).Split(':')[0]);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;


        public MillsMelangeSlubRepository(ICosmosDbContainerFactory factory) : base (factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
           
        public async Task<List<MelanSlubTypeTemplateModel>> GetMelanSlubTypes(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<MelanSlubTypeTemplateModel>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<MelanSlubTypeTemplateModel> melanSlubTypes = new List<MelanSlubTypeTemplateModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                melanSlubTypes.AddRange(res.ToList());
            }
            return melanSlubTypes;
        }
    }
}
