﻿using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels.ProfileModels;
using Tik.Repository.Profiles.Interfaces;
using Tik.Repository;
using System.Linq;
using Microsoft.Azure.Cosmos.Linq;

namespace Tik.Repository.Profiles.Implementations
{
    public class MillsContentRepository : CosmosDbRepository<MillsContentTemplateModel>, IMillsContentRepository
    {
        public override string ContainerName { get; } = "MillsContent";
        public override string GenerateId(MillsContentTemplateModel entity) => $"{entity.Category}:{Guid.NewGuid()}";
        public override PartitionKey ResolvePartitionKey(MillsContentTemplateModel entity) => new PartitionKey((entity.Id).Split(':')[0]);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public MillsContentRepository(ICosmosDbContainerFactory factory) : base(factory)
        {
            this._cosmosDbContainerFactory = factory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<MillsContentTemplateModel>> GetMillsContents(string category)
        {
            var feedIterator = _container.GetItemLinqQueryable<MillsContentTemplateModel>().Where(x => x.Category.ToLower() == category.ToLower()).ToFeedIterator();

            List<MillsContentTemplateModel> millsContents = new List<MillsContentTemplateModel>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                millsContents.AddRange(res.ToList());
            }
            return millsContents;
        }
    }
}
