﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface IStateRepository : IRepository<StateEntity>
    {
        Task<List<StateEntity>> GetStates(string countryId);
    }
}
