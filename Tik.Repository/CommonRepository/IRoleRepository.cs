﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public interface IRoleRepository : IRepository<RoleEntity>
    {
        Task<List<RoleEntity>> GetRoles(bool isCompany);
    }
}