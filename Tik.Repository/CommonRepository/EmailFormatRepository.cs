﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public class EmailFormatRepository : CosmosDbRepository<EmailFormatEntity>, IEmailFormatRepository
    {
        public override string ContainerName { get; } = "EmailFormats";
        public override string GenerateId(EmailFormatEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(EmailFormatEntity entity) => new PartitionKey(entity.Status);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public EmailFormatRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<EmailFormatEntity> GetEmailFormat(string emailTemplateName)
        {
            var iterator = _container.GetItemLinqQueryable<EmailFormatEntity>().Where(x => x.EmailTemplateName.Contains(emailTemplateName)).ToFeedIterator();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }
    }
}
