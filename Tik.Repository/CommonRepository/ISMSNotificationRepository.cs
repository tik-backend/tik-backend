﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public interface ISMSNotificationRepository : IRepository<SMSTemplateEntity>
    {
        Task<List<SMSTemplateEntity>> GetSMSTemplateByStatus(string status);
    }
}
