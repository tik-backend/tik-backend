﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface ICertificationRepository : IRepository<CertificationEntity>
    {
        Task<List<CertificationEntity>> GetCertificates();
    }
}
