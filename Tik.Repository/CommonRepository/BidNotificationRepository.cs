﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels.CommonEntity;
namespace Tik.Repository.CommonRepository
{
    public class BidNotificationRepository : CosmosDbRepository<BidNotification>, IBidNotificationRepository
    {
        public override string ContainerName { get; } = "BidStatusNotificationList";

        public override string GenerateId(BidNotification entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(BidNotification entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public BidNotificationRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }
        public async Task<List<BidNotification>> GetNotificationList(bool isCompany)
        {
            FeedIterator<BidNotification> feedIterator = null;
            if (isCompany)
                feedIterator = _container.GetItemLinqQueryable<BidNotification>().Where(x => x.IsAllowed && x.IsCompany == true).ToFeedIterator();
            else
                feedIterator = _container.GetItemLinqQueryable<BidNotification>().Where(x => x.IsAllowed && x.IsMill == true).ToFeedIterator();
            var notificationList = new List<BidNotification>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                notificationList.AddRange(res.ToList());
            }
            return notificationList;
        }

        public async Task<List<BidNotification>> GetNotificationListByStatus(string status)
        {
            FeedIterator<BidNotification> feedIterator = _container.GetItemLinqQueryable<BidNotification>().Where(x => x.IsAllowed && x.Status == status).ToFeedIterator();
            var notificationList = new List<BidNotification>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                notificationList.AddRange(res.ToList());
            }
            return notificationList;
        }
    }
}
