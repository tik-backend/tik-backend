﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.ProjectDomainModel;
using Tik.BusinessService.Helper;

namespace Tik.Repository.CommonRepository
{
    public class ReviewRepository : CosmosDbRepository<ReviewEntity>, IReviewRepository
    {
        public override string ContainerName { get; } = "Review";

        public override string GenerateId(ReviewEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(ReviewEntity entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public ReviewRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<ReviewEntity>> GetReviewAverage(string companyId)
        {
            var feedIterator = _container.GetItemLinqQueryable<ReviewEntity>().Where(x => x.CompanyId.Contains(companyId)).OrderByDescending(x => x.CreatedDate).ToFeedIterator();

            var reviewAvg = new List<ReviewEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                reviewAvg.AddRange(res.ToList());
            }
            return reviewAvg;
        }

        public async Task<bool> ReviewRating(string companyId, ProjectEntity projectConfirmed, Bids bidConfirmed, UserEntity user, ReviewEntity review, bool isCompany)
        {
            review.Id = Guid.NewGuid().ToString();
            review.CompanyId = companyId;
            review.ProjectId = projectConfirmed.Id;
            review.BiddingId = bidConfirmed.Id;
            review.ReviewRating = review.ReviewRating;
            review.Comments = review.Comments;
            review.ReviewedBy = user.Id;
            review.isCompany = isCompany;
            review.Status = ReviewStatus.R.Meaning;
            review.CreatedDate = DateTime.Now;
            await AddItemAsync(review);
            return true;
        }

        public async Task<ReviewEntity> GetReviewStatusById(string projectId, bool isCompany)
        {
            FeedIterator<ReviewEntity> iterator = null;
            iterator = _container.GetItemLinqQueryable<ReviewEntity>().Where(x => x.ProjectId == projectId && x.Status == ReviewStatus.R.Meaning && x.isCompany == isCompany).ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }

        public async Task<List<ReviewEntity>> GetReviewStatusByProjectIds(string[] projectId, bool isCompany)
        {
            FeedIterator<ReviewEntity> iterator = null;
            iterator = _container.GetItemLinqQueryable<ReviewEntity>().Where(x => projectId.Contains(x.ProjectId) && x.Status == ReviewStatus.R.Meaning && x.isCompany == isCompany).ToFeedIterator();
            var reviewList = new List<ReviewEntity>();
            while (iterator.HasMoreResults)
            {
                var res = await iterator.ReadNextAsync();
                reviewList.AddRange(res.ToList());
            }
            return reviewList;
        }

        public async Task<int> CheckReviewStatus(string projectId)
        {
            IQueryable<ReviewEntity> q = _container.GetItemLinqQueryable<ReviewEntity>().Where(x => x.ProjectId.Contains(projectId) && x.Status != ReviewStatus.C.Meaning);

            return await q.CountAsync();
        }
    }
}
