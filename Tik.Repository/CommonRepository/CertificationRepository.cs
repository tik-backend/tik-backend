﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository.CertificationRepository
{
    public class CertificationRepository : CosmosDbRepository<CertificationEntity>, ICertificationRepository
    {
        public override string ContainerName { get; } = "Certifications";

        public override string GenerateId(CertificationEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(CertificationEntity entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public CertificationRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<CertificationEntity>> GetCertificates()
        {
            var feedIterator = _container.GetItemLinqQueryable<CertificationEntity>().Select(x => x).ToFeedIterator();

            var certificates = new List<CertificationEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                certificates.AddRange(res.ToList());
            }
            return certificates;
        }
    }
}