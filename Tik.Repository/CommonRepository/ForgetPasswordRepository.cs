﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Tik.BusinessService.EmailServices;
using Tik.BusinessService.Helper;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public class ForgetPasswordRepository : CosmosDbRepository<ForgetPasswordEntity>, IForgetPasswordRepository
    {
        public override string ContainerName { get; } = "ForgetPassword";

        public override string GenerateId(ForgetPasswordEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(ForgetPasswordEntity entity) => new PartitionKey(entity.Status);

        private readonly IUserRepository _userRepo;
        private readonly IEmailService _emailService;
        private readonly IEmailFormatRepository _emailFormatRepo;
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public ForgetPasswordRepository(ICosmosDbContainerFactory cosmosDbContainerFactory, IUserRepository userRepo, IEmailService emailService, IEmailFormatRepository emailFormatRepo) : base(cosmosDbContainerFactory)
        {
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
            this._emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this._emailFormatRepo = emailFormatRepo ?? throw new ArgumentNullException(nameof(emailFormatRepo));
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<bool> CreateTempPasswordandSendMail(UserEntity userEntity)
        {
            var tempPassword = GetRandomPassword();
            var emailAddress = userEntity.EmailAddress.ToLower();
            UserEntity data = await _userRepo.GetUserByEMailId(emailAddress);
            if (data == null)
                return false;
            else
            {
                ForgetPasswordEntity forgetPasswordEntity = new ForgetPasswordEntity()
                {
                    Id = data.Id,
                    TemporaryPassword = tempPassword,
                    EmailAddress = emailAddress,
                    Status = ForgetPassword.A.Meaning
                };

                int value = 0;
                string emailTemplateName = Enum.GetName(typeof(EmailTemplate), value);
                EmailFormatEntity emailFormat = await _emailFormatRepo.GetEmailFormat(emailTemplateName);

                ForgetPasswordEntity forgetData = await GetItemById(data.Id);
                if (forgetData == null)
                {
                    await AddItemAsync(forgetPasswordEntity);
                    await _emailService.SendEmailAsync(emailAddress, tempPassword, emailFormat);
                }
                else
                {
                    forgetData.TemporaryPassword = tempPassword;
                    forgetData.EmailAddress = emailAddress;
                    forgetData.UpdatedDate = DateTime.Now;
                    await UpdateItemAsync(forgetData.Id, forgetData);
                    await _emailService.SendEmailAsync(emailAddress, tempPassword, emailFormat);
                }
                return true;
            }
        }

        public string GetRandomPassword()
        {
            return new string(
                Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*", 13)
                    .Select(s =>
                    {
                        var cryptoResult = new byte[4];
                        using (var cryptoProvider = new RNGCryptoServiceProvider())
                            cryptoProvider.GetBytes(cryptoResult);
                        return s[new Random(BitConverter.ToInt32(cryptoResult, 0)).Next(s.Length)];
                    }).ToArray());
        }

        public async Task<bool> UpdatePassword(string id, UserEntity userEntity)
        {
            UserEntity data = await _userRepo.GetItemById(id);
            ForgetPasswordEntity forgetPassword = await GetItemById(id);

            if (data != null)
            {
                userEntity.Id = data.Id;
                userEntity.Password = PasswordSecurity.HashPassword(userEntity.Password);
                userEntity.UpdatedDate = DateTime.Now;
                await _userRepo.UpdateItemAsync(data.Id, userEntity);
                await DeleteItemAsync(forgetPassword.Id, forgetPassword);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<ForgetPasswordEntity> GetUserByEMailIdAndPassword(string EmailId, string password)
        {
            var iterator = _container.GetItemLinqQueryable<ForgetPasswordEntity>().Where(x => x.EmailAddress == EmailId && x.TemporaryPassword == password).ToFeedIterator();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }
    }
}
