﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.CommonRepository
{
    public interface IReviewRepository : IRepository<ReviewEntity>
    {
        Task<bool> ReviewRating(string companyId, ProjectEntity projectConfirmed, Bids bidConfirmed, UserEntity user, ReviewEntity review, bool isCompany);
        Task<List<ReviewEntity>> GetReviewAverage(string companyId);
        Task<ReviewEntity> GetReviewStatusById(string projectId, bool isCompany);
        Task<int> CheckReviewStatus(string projectId);
        Task<List<ReviewEntity>> GetReviewStatusByProjectIds(string[] projectId, bool isCompany);
    }
}
