﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels.CommonEntity;
namespace Tik.Repository.CommonRepository
{
    public class SMSNotificationRepository : CosmosDbRepository<SMSTemplateEntity>, ISMSNotificationRepository
    {
        public override string ContainerName { get; } = "SMSTemplate";

        public override string GenerateId(SMSTemplateEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(SMSTemplateEntity entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public SMSNotificationRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<SMSTemplateEntity>> GetSMSTemplateByStatus(string status)
        {
            FeedIterator<SMSTemplateEntity> feedIterator = _container.GetItemLinqQueryable<SMSTemplateEntity>().Where(x => x.Status == status).ToFeedIterator();
            var notificationList = new List<SMSTemplateEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                notificationList.AddRange(res.ToList());
            }
            return notificationList;
        }
    }
}
