﻿using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public interface IForgetPasswordRepository : IRepository<ForgetPasswordEntity>
    {
        Task<bool> CreateTempPasswordandSendMail(UserEntity userEntity);
        Task<bool> UpdatePassword(string id, UserEntity userEntity);
        Task<ForgetPasswordEntity> GetUserByEMailIdAndPassword(string EmailId, string password);
    }
}