﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public class RoleRepository : CosmosDbRepository<RoleEntity>, IRoleRepository
    {
        public override string ContainerName { get; } = "Roles";

        public override string GenerateId(RoleEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(RoleEntity entity) => new PartitionKey(entity.Status);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public RoleRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<RoleEntity>> GetRoles(bool isCompany)
        {
            FeedIterator<RoleEntity> feedIterator = null;
            if (isCompany)
            feedIterator = _container.GetItemLinqQueryable<RoleEntity>().Where(x => x.IsCompany.Contains("1")).ToFeedIterator();
            else
                feedIterator = _container.GetItemLinqQueryable<RoleEntity>().Where(x => x.IsVendor.Contains("1")).ToFeedIterator();

            var roles = new List<RoleEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                roles.AddRange(res.ToList());
            }
            return roles;
        }
    }
}
