﻿using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public interface IEmailFormatRepository : IRepository<EmailFormatEntity>
    {
        Task<EmailFormatEntity> GetEmailFormat(string emailTemplateName);
    }
}
