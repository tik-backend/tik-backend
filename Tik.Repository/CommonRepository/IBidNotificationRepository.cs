﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.CommonEntity;

namespace Tik.Repository.CommonRepository
{
    public interface IBidNotificationRepository : IRepository<BidNotification>
    {
        Task<List<BidNotification>> GetNotificationList(bool isCompany);
        Task<List<BidNotification>> GetNotificationListByStatus(string status);
    }
}
