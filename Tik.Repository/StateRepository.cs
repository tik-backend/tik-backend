﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public class StateRepository : CosmosDbRepository<StateEntity>, IStateRepository
    {
        public override string ContainerName { get; } = "State";

        public override string GenerateId(StateEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(StateEntity entity) => new PartitionKey(entity.StateName);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public StateRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<StateEntity>> GetStates(string countryId)
        {
            var feedIterator = _container.GetItemLinqQueryable<StateEntity>().Where(x => x.CountryId.Contains(countryId)).ToFeedIterator();

            var states = new List<StateEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                states.AddRange(res.ToList());
            }
            return states;
        }
    }
}
