﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using Tik.DomainModels.Enum;
using Tik.BusinessService.Helper;
using Tik.DomainModels.Filter;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.ProjectRepository
{
    public class ProjectRepository : CosmosDbRepository<ProjectEntity>, IProjectRepository
    {
        public override string ContainerName { get; } = "Project";

        public override string GenerateId(ProjectEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(ProjectEntity entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public ProjectRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async void CreateProject(ProjectEntity result)
        {
            await AddItemAsync(result);
        }

        public async Task<List<ProjectEntity>> GetProjectListByCreator(string companyId, AssignRole role, bool isCompany, string userId)
        {
            FeedIterator<ProjectEntity> feedIterator = null;
            if (isCompany)
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>()
                    .Where(x => x.companyId == companyId && role.Meaning == AssignRole.QI.Meaning? x.creatorId == userId : x.merchandiserId == userId)
                    .Select(x => x).ToFeedIterator();
            else
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>()
                    .Where(x => x.bids.Any(b => b.companyId == companyId && b.creatorId == userId))                    
                    .Select(x => x).ToFeedIterator();


            var projectList = new List<ProjectEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                projectList.AddRange(res.ToList());
            }
            return projectList;
        }
        public async Task<List<ProjectEntity>> ListProject(string companyId, bool isCompany)
        {
            FeedIterator<ProjectEntity> feedIterator = null;
            if (isCompany)
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>().Where(x => x.companyId == companyId).Select(x => x).ToFeedIterator();
            else
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>().Where(x => x.bids.Any(b => b.companyId == companyId)).ToFeedIterator();


            var projectList = new List<ProjectEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                projectList.AddRange(res.ToList());
            }
            return projectList;
        }

        public async Task<List<ProjectEntity>> GetFilteredProjectList(ProjectFilter filter)
        {
            FeedIterator<ProjectEntity> feedIterator = null;
            if (filter.isCompany)
            {
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>()
                    .Where(x => x.companyId == filter.companyId)
                    .Where(x => filter.Status == null || filter.Status.Contains((int)x.stages))
                    .Select(x => x).ToFeedIterator();
            }
            else
            {

                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>()                 
                    .Where(x => filter.Status == null || x.bids.Any(b=> filter.Status.Contains((int)b.bidStatus) && b.companyId == filter.companyId))
                    .Select(x => x).ToFeedIterator();
            }
            var projectList = new List<ProjectEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                projectList.AddRange(res.ToList());
            }
            return projectList;
        }

        public async Task<List<Bids>> GetBidListByFilter(BidFilter filter)
        {
            FeedIterator<Bids> feedIterator = null;
            if (filter.maxCreditPeriod == 0 && filter.maxDeliveryPeriod == 0)
            {
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>()
                    .Where(x => x.Id == filter.projectId)
                    .SelectMany(x => x.bids)
                    .ToFeedIterator();
            }
            else
            {
                feedIterator = _container.GetItemLinqQueryable<ProjectEntity>()
                    .Where(x => x.Id == filter.projectId)
                    .SelectMany(x => x.bids)
                    .Where(x => (filter.maxCreditPeriod > 0) ?  x.creditPeriod <= filter.maxCreditPeriod : x.creditPeriod >= 0)
                    .Where(x => (filter.maxDeliveryPeriod > 0) ? x.deliveryPeriod <= filter.maxDeliveryPeriod : x.deliveryPeriod >= 0)
                    .Select(x => x)
                    .ToFeedIterator();
            }
            var bidList = new List<Bids>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                bidList.AddRange(res.ToList());
            }
            return bidList;
        }
        public async Task<ProjectEntity> GetBidById(string id)
        {
            var iterator = _container.GetItemLinqQueryable<ProjectEntity>().Where(x => x.Id == id).ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }
    }
}
