﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tik.DomainModels;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.ProjectRepository
{
    public interface IMillsRepository : IRepository<MillsEntity>
    {
        Task<IEnumerable<MillsEntity>> SearchProject(List<Summary> resultSet);
    }
}
