﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.ProjectRepository
{
    public interface IBiddingAuditRepository : IRepository<BiddingAuditEntity>
    {
        Task<IEnumerable<BiddingAuditEntity>> GetBidAudit(string projectId);
        Task<BiddingAuditEntity> GetBidAuditById(string projectId);
    }
}
