﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.BusinessService.Helper;
using Tik.DomainModels.Filter;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.ProjectRepository
{
    public interface IProjectRepository : IRepository<ProjectEntity>
    {
        void CreateProject(ProjectEntity result);
        Task<List<ProjectEntity>> ListProject(string companyId, bool isCompany);
        Task<List<ProjectEntity>> GetFilteredProjectList(ProjectFilter filter);
        Task<ProjectEntity> GetBidById(string id);
        Task<List<Bids>> GetBidListByFilter(BidFilter filter);
        Task<List<ProjectEntity>> GetProjectListByCreator(string companyId, AssignRole role, bool isCompany, string userId);
    }
}
