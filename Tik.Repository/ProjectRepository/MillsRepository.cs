﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using Newtonsoft.Json.Linq;
using Tik.DomainModels;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.ProjectRepository
{
    public class MillsRepository: CosmosDbRepository<MillsEntity>, IMillsRepository
    {
        public override string ContainerName { get; } = "Mills";

        public override string GenerateId(MillsEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(MillsEntity entity) => new PartitionKey(entity.Id);       

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public MillsRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<IEnumerable<MillsEntity>> SearchProject(List<Summary> resultSet)
        {
            StringBuilder selectQuery = new StringBuilder();
            StringBuilder joinQuery = new StringBuilder();
            selectQuery.AppendLine("select distinct mil.id, mil.CompanyId, mil.CreatorId,");
            Dictionary<string, string> joinsList = new Dictionary<string, string>();

            foreach (Summary res in resultSet)
            {
                if (!string.IsNullOrEmpty(res.data_path) &&  res.data_id != "Array" && res.data_id != "ExcludeJoins")
                {
                    if (joinsList.ContainsKey(res.data_path))
                    {
                        res.joinValue = joinsList.GetValueOrDefault(res.data_path);
                    }
                    else
                    {
                        res.joinValue = RandomString(5);
                        if (!string.IsNullOrEmpty(res.data_id))
                            res.joinValue += ".";
                        joinQuery.AppendLine("join " + res.joinValue.TrimEnd('.') + " in " + res.data_path);

                        joinsList.Add(res.data_path, res.joinValue);
                    }
                }
            }

            joinQuery.AppendLine("where ");
            foreach (Summary res in resultSet)
            {
                if (string.IsNullOrEmpty(res.data_path) && string.IsNullOrEmpty(res.data_id))
                    continue;

                if (string.IsNullOrEmpty(res.data_path) && !string.IsNullOrEmpty(res.data_id))
                {
                    selectQuery.Append(res.data_id + ",");
                    continue;
                }

                if (res.data_id == "Array")
                {
                    Type type = res.value.GetType();
                    if (type.Name == "JArray")
                    {
                        var data = res.value.ToObject<List<object>>();
                        foreach (var r in data)
                        {
                            joinQuery.AppendLine("Array_contains(" + res.data_path + ",'" + r + "', true) and");
                        }
                    }
                    else
                    {
                        joinQuery.AppendLine("Array_contains(" + res.data_path + ",'" + res.value + "', true) and");
                    }
                }
                else if (res.data_id.ToLower() == "combos")
                {
                    joinQuery.AppendLine("Array_contains(" + res.joinValue + res.data_id + ",{'CombinationOne': " + res.value.Split(',')[0] + ", 'CombinationTwo': " + res.value.Split(',')[1] + "},true) and");
                }
                else if(res.data_id == "ExcludeJoins")
                {
                    joinQuery.AppendLine(res.data_path+ "='" + res.value.Trim() + "' and");
                }
                else
                {
                    joinQuery.AppendLine(res.joinValue + res.data_id + "='" + res.value.Trim() + "' and");
                }
            }

            string finalquery = selectQuery.ToString().Trim().TrimEnd(',') + " from Mills mil " + joinQuery.ToString().Substring(0, joinQuery.Length - 5);
            return await GetItemsAsync(finalquery);
        }

        private string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
