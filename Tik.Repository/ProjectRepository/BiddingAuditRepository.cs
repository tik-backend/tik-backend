﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels.Enum;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.Repository.ProjectRepository
{
    public class BiddingAuditRepository : CosmosDbRepository<BiddingAuditEntity>, IBiddingAuditRepository
    {
        public override string ContainerName { get; } = "BiddingAudit";

        public override string GenerateId(BiddingAuditEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(BiddingAuditEntity entity) => new PartitionKey(entity.Id);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public BiddingAuditRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<IEnumerable<BiddingAuditEntity>> GetBidAudit(string projectId)
        {
            var iterator = _container.GetItemLinqQueryable<BiddingAuditEntity>().Where(x => x.projectId == projectId).ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                var res = await iterator.ReadNextAsync();
                return res.ToList();
            }
            return null;
        }

        public async Task<BiddingAuditEntity> GetBidAuditById(string projectId)
        {
            var iterator = _container.GetItemLinqQueryable<BiddingAuditEntity>().Where(x => x.projectId == projectId && x.bidStatus == BidStatus.Confirmed).ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }
    }
}
