﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface ICountryRepository : IRepository<CountryEntity>
    {
        Task<List<CountryEntity>> GetCountries();
    }
}
