﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public abstract class CosmosDbRepository<T> : IRepository<T>, IContainerContext<T> where T : BaseEntity
    {
        public abstract string ContainerName { get; }

        public abstract string GenerateId(T entity);

        public abstract PartitionKey ResolvePartitionKey(T entity);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public CosmosDbRepository(ICosmosDbContainerFactory cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task AddItemAsync(T item)
        {
            try
            {
                item.Id = GenerateId(item);
                await _container.CreateItemAsync<T>(item, ResolvePartitionKey(item));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteItemAsync(string id, T item)
        {
            await this._container.DeleteItemAsync<T>(id, ResolvePartitionKey(item));
        }

        public async Task<T> GetItemAsync(string id, T item)
        {
            try
            {
                ItemResponse<T> response = await _container.ReadItemAsync<T>(id, ResolvePartitionKey(item));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
        }
        public async Task<T> GetItemById(string Id)
        {
            var iterator = _container.GetItemLinqQueryable<T>().Where(x => x.Id == Id).ToFeedIterator<T>();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }

            return null;
        }

        public async Task<IEnumerable<T>> GetItemListByIds(string[] Ids)
        {
            var iterator = _container.GetItemLinqQueryable<T>().Where(x => Ids.Contains(x.Id)).ToFeedIterator<T>();
            List<T> results = new List<T>();
            while (iterator.HasMoreResults)
            {
                var response = await iterator.ReadNextAsync();
                results.AddRange(response.ToList());
            }
            return results;
        }

        public async Task<IEnumerable<T>> GetItemsAsync(string queryString)
        {
            var query = _container.GetItemQueryIterator<T>(new QueryDefinition(queryString));
            List<T> results = new List<T>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();

                results.AddRange(response.ToList());
            }
            return results;
        }

        public async Task<bool> IsRecordExists(string queryString)
        {
            var result = await GetItemsAsync(queryString);
            return result.Count() > 0 ? true : false;
        }

        public async Task UpdateItemAsync(string id, T item)
        {
            await this._container.UpsertItemAsync<T>(item, ResolvePartitionKey(item));
        }
    }
}
