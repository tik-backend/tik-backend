﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface IUserRepository : IRepository<UserEntity>
    {
        Task<bool> RegisterCompany(UserEntity user, CompanyEntity company);
        Task<bool> RegisterUser(UserEntity user);
        Task<UserEntity> CheckCredentials(string email, string password);
        void UpdateRecord(string refreshToken, DateTime expiryDate, string emailAddress);
        Task<bool> ValidateRefreshToken(string id, string refreshToken);
        Task<UserEntity> GetUserByCompanyId(string companyId);
        Task<List<UserEntity>> GetAllQuoteInchargeOfMerchandiser(string companyId, string merchandiserId);
        Task<List<UserEntity>> GetAllMerchandiserOfApprovalAdmin(string companyId, string approvalAdminId);
        Task<List<UserEntity>> GetUserListsByCompanyIds(string[] companyId);
        Task<List<UserEntity>> GetUserByRoles(string companyId, string role);
        Task<UserEntity> GetUserByEMailId(string EmailId);
    }
}
