﻿using Microsoft.Azure.Cosmos;
using System;
using System.Threading.Tasks;
using Tik.BusinessService.Helper;
using Tik.DomainModels;
using System.Security.Cryptography;
using Tik.DomainModels.CommonEntity;
using System.Linq;
using Microsoft.Azure.Cosmos.Linq;
using System.Collections.Generic;

namespace Tik.Repository
{
    public class UserRepository : CosmosDbRepository<UserEntity>, IUserRepository
    {
        public override string ContainerName { get; } = "Users";

        public override string GenerateId(UserEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(UserEntity entity) => new PartitionKey(entity.Id);

        private readonly ICompanyRepository _companyRepo;
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public UserRepository(ICosmosDbContainerFactory cosmosDbContainerFactory, ICompanyRepository companyRepo) : base(cosmosDbContainerFactory)
        {
            this._companyRepo = companyRepo ?? throw new ArgumentNullException(nameof(companyRepo));
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<bool> RegisterCompany(UserEntity user, CompanyEntity company)
        {
            user.CompanyId = Guid.NewGuid().ToString();
            CompanyEntity companyEntity = await _companyRepo.GetCompanyGST(company.GSTNumber);
            if (companyEntity != null)
                return false;
            else
                await _companyRepo.CreateUserCompany(company, user.CompanyId);
            return true;
        }

        public async Task<bool> RegisterUser(UserEntity user)
        {
            user.Password = PasswordSecurity.HashPassword(user.Password);
            var emailAddress = user.EmailAddress.ToLower().Trim();
            UserEntity data = await GetUserByEMailId(emailAddress);
            if (data != null)
                return false;
            else
            {
                int value = 0;
                string role = Enum.GetName(typeof(Roles), value);
                user.EmployeeRole = role;
                await AddItemAsync(user);
            }
            return true;
        }

        public async Task<UserEntity> CheckCredentials(string email, string password)
        {
            var result = await GetUserByEMailId(email.ToLower().Trim());
            if (result == null)
                return null;
            try
            {
                VerifyPassword(result.Password, password);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async void UpdateRecord(string refreshToken, DateTime expiryDate, string emailAddress)
        {
            UserEntity result = await GetUserByEMailId(emailAddress.ToLower().Trim());
            result.RefreshToken = refreshToken;
            result.ExpiryDate = expiryDate;
            await UpdateItemAsync(result.Id, result);
        }

        private void VerifyPassword(string savedPassword, string userPassword)
        {
            byte[] hashBytes = Convert.FromBase64String(savedPassword);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(userPassword, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    throw new UnauthorizedAccessException();
        }

        public async Task<bool> ValidateRefreshToken(string id, string refreshToken)
        {
            var result = await GetItemById(id);

            if (result == null || !result.ExpiryDate.HasValue)
                return false;

            if (result.RefreshToken == refreshToken && result.ExpiryDate >= DateTime.UtcNow)
            {
                return true;
            }
            return false;
        }

        public async Task<List<UserEntity>> GetUserByRoles(string companyId, string role)
        {
            FeedIterator<UserEntity> feedIterator = null;
            feedIterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => x.CompanyId == companyId && x.EmployeeRole == role).ToFeedIterator();

            var roles = new List<UserEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                roles.AddRange(res.ToList());
            }
            return roles;
        }

        public async Task<UserEntity> GetUserByCompanyId(string companyId)
        {
            var iterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => x.CompanyId == companyId).ToFeedIterator();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }

        public async Task<List<UserEntity>> GetUserListsByCompanyIds(string[] companyId)
        {
            var iterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => companyId.Contains(x.CompanyId)).ToFeedIterator();

            var users = new List<UserEntity>();
            while (iterator.HasMoreResults)
            {
                var res = await iterator.ReadNextAsync();
                users.AddRange(res.ToList());
            }
            return users;
        }

        public async Task<List<UserEntity>> GetAllQuoteInchargeOfMerchandiser(string companyId, string merchandiserId)
        {
            var iterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => x.CompanyId == companyId /*&& x.MerchandiserId == merchandiserId*/ && x.EmployeeRole == AssignRole.QI.Meaning).ToFeedIterator();
            var users = new List<UserEntity>();
            while (iterator.HasMoreResults)
            {
                var res = await iterator.ReadNextAsync();
                users.AddRange(res.ToList());
            }
            return users;
        }

        public async Task<List<UserEntity>> GetAllMerchandiserOfApprovalAdmin(string companyId, string approvalAdminId)
        {
            var iterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => x.CompanyId == companyId /*&& x.ApprovalAdminId == approvalAdminId*/ && x.EmployeeRole == AssignRole.M.Meaning).ToFeedIterator();
            var users = new List<UserEntity>();
            while (iterator.HasMoreResults)
            {
                var res = await iterator.ReadNextAsync();
                users.AddRange(res.ToList());
            }
            return users;
        }

        public async Task<UserEntity> GetUserByEMailId(string EmailId)
        {
            var iterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => x.EmailAddress == EmailId).ToFeedIterator();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }
    }
}
