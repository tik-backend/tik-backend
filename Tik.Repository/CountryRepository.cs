﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public class CountryRepository : CosmosDbRepository<CountryEntity>, ICountryRepository
    {
        public override string ContainerName { get; } = "Countries";

        public override string GenerateId(CountryEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(CountryEntity entity) => new PartitionKey(entity.CountryName);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public CountryRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<CountryEntity>> GetCountries()
        {
            var feedIterator = _container.GetItemLinqQueryable<CountryEntity>().Select(x => x).ToFeedIterator();

            var countries = new List<CountryEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                countries.AddRange(res.ToList());
            }
            return countries;
        }
    }
}
