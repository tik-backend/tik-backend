﻿using Microsoft.Azure.Cosmos;

namespace Tik.Repository
{
    public interface ICosmosDbContainer
    {
        Container _container { get; }
    }
}
