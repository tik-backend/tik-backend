﻿using Microsoft.Extensions.DependencyInjection;

namespace Tik.Repository
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddCosmosDb(this IServiceCollection services,
                                                     string endpointUrl,
                                                     string databaseName,
                                                     string blobConStr)
        {
            Microsoft.Azure.Cosmos.CosmosClient client = new Microsoft.Azure.Cosmos.CosmosClient(endpointUrl);
            var cosmosDbClientFactory = new CosmosDbContainerFactory(client, databaseName, blobConStr);

            services.AddSingleton<ICosmosDbContainerFactory>(cosmosDbClientFactory);

            return services;
        }
    }
}
