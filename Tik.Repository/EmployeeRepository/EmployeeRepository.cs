﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.BusinessService.Helper;
using Tik.DomainModels;

namespace Tik.Repository.EmployeeRepository
{
    public class EmployeeRepository : CosmosDbRepository<UserEntity>, IEmployeeRepository
    {
        public override string ContainerName { get; } = "Users";

        public override string GenerateId(UserEntity entity) => $"{Guid.NewGuid()}";

        public string[] CertificateID { get; set; }
        public override PartitionKey ResolvePartitionKey(UserEntity entity) => new PartitionKey(entity.Id);

        private readonly IUserRepository _userRepo;
        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public EmployeeRepository(ICosmosDbContainerFactory cosmosDbContainerFactory, IUserRepository userRepo) : base(cosmosDbContainerFactory)
        {
            this._userRepo = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<UserEntity>> GetEmployeesById(string companyId)
        {
            var feedIterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => x.CompanyId.Contains(companyId)).ToFeedIterator();

            var employees = new List<UserEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                employees.AddRange(res.ToList());
            }
            return employees;
        }

        public async Task<bool> CreateEmployee(UserEntity employee, string companyId)
        {
            var emailAddress = employee.EmailAddress.ToLower().Trim();
            UserEntity data = await _userRepo.GetUserByEMailId(emailAddress);
            if (data != null)
                return false;
            else
            {
                employee.Password = PasswordSecurity.HashPassword(employee.Password);
                employee.CompanyId = companyId;
                await AddItemAsync(employee);
            }
            return true;
        }

        public async Task<bool> EditEmployeeDetails(string employeeId, UserEntity employee, string companyId)
        {
            UserEntity data = await GetItemById(employeeId);
            if (data == null)
                return false;
            else
            {
                if (!string.IsNullOrEmpty(employee.Password))
                    employee.Password = PasswordSecurity.HashPassword(employee.Password);
                else
                    employee.Password = data.Password;

                employee.Id = employeeId;
                employee.UpdatedDate = DateTime.Now;
                employee.CompanyId = companyId;
                await UpdateItemAsync(data.Id, employee);
            }
            return true;
        }

        public async Task<bool> DeleteEmployeeDetails(string employeeId, UserEntity employee)
        {
            UserEntity data = await GetItemById(employeeId);
            if (data != null)
            {
                await DeleteItemAsync(data.Id, employee);
            }
            else
            {
                return false;
            }
            return true;
        }

        public async Task<bool> EmployeeExists(string emailId)
        {
            UserEntity data = await _userRepo.GetUserByEMailId(emailId);
            if (data != null)
                return false;
            return true;
        }

        public async Task<List<UserEntity>> GetEmployeeNotificationList(string[] companyIds, string StatusId)
        {
            var feedIterator = _container.GetItemLinqQueryable<UserEntity>().Where(x => companyIds.Contains(x.CompanyId) && x.Notification.StatusList.Contains(StatusId) && x.Notification.IsSMS == true).ToFeedIterator();
            var employees = new List<UserEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                employees.AddRange(res.ToList());
            }
            return employees;
        }

    }
}