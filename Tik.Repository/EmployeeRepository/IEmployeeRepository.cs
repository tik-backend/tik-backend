﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository.EmployeeRepository
{
    public interface IEmployeeRepository : IRepository<UserEntity>
    {
        Task<List<UserEntity>> GetEmployeesById(string companyId);
        Task<bool> CreateEmployee(UserEntity employee, string companyId);
        Task<bool> EditEmployeeDetails(string employeeId, UserEntity employee, string companyId);
        Task<bool> DeleteEmployeeDetails(string employeeId, UserEntity employee);
        Task<bool> EmployeeExists(string emailId);
        Task<List<UserEntity>> GetEmployeeNotificationList(string[] companyIds, string StatusId);
    }
}