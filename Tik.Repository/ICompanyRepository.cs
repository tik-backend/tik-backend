﻿using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public interface ICompanyRepository : IRepository<CompanyEntity>
    {
        Task CreateUserCompany(CompanyEntity company, string refId);
        Task<CompanyEntity> GetCompanyGST(string gstNumber);
        Task<CompanyEntity> GetCompanyById(string companyId);
    }
}