﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public class CompanyRepository : CosmosDbRepository<CompanyEntity>, ICompanyRepository
    {
        public override string ContainerName { get; } = "Company";

        public override string GenerateId(CompanyEntity entity) => $"{entity.Id}";

        public override PartitionKey ResolvePartitionKey(CompanyEntity entity) => new PartitionKey(entity.GSTNumber);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;
        private readonly Container _container;

        public CompanyRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task CreateUserCompany(CompanyEntity company, string refId)
        {
            company.Id = refId;
            await AddItemAsync(company);
        }

        public async Task<CompanyEntity> GetCompanyGST(string gstNumber)
        {
            var iterator = _container.GetItemLinqQueryable<CompanyEntity>().Where(x => x.GSTNumber.Contains(gstNumber)).ToFeedIterator();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }

        public async Task<CompanyEntity> GetCompanyById(string companyId)
        {
            var iterator = _container.GetItemLinqQueryable<CompanyEntity>().Where(x => x.Id == companyId).ToFeedIterator();

            while (iterator.HasMoreResults)
            {
                foreach (var item in await iterator.ReadNextAsync())
                {
                    return item;
                }
            }
            return null;
        }
    }
}
