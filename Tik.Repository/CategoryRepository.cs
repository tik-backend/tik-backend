﻿using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tik.DomainModels;

namespace Tik.Repository
{
    public class CategoryRepository : CosmosDbRepository<CompanyEntity>, ICategoryRepository
    {
        public override string ContainerName { get; } = "Category";

        public override string GenerateId(CompanyEntity entity) => $"{Guid.NewGuid()}";

        public override PartitionKey ResolvePartitionKey(CompanyEntity entity) => new PartitionKey(entity.CategoryName);

        private readonly ICosmosDbContainerFactory _cosmosDbContainerFactory;

        private readonly Container _container;

        public CategoryRepository(ICosmosDbContainerFactory cosmosDbContainerFactory) : base(cosmosDbContainerFactory)
        {
            this._cosmosDbContainerFactory = cosmosDbContainerFactory ?? throw new ArgumentNullException(nameof(ICosmosDbContainerFactory));
            this._container = this._cosmosDbContainerFactory.GetContainer(ContainerName)._container;
        }

        public async Task<List<CompanyEntity>> GetCategories()
        {
            var feedIterator = _container.GetItemLinqQueryable<CompanyEntity>().Select(x => x).ToFeedIterator();

            var companies = new List<CompanyEntity>();
            while (feedIterator.HasMoreResults)
            {
                var res = await feedIterator.ReadNextAsync();
                companies.AddRange(res.ToList());
            }
            return companies;
        }
    }
}
