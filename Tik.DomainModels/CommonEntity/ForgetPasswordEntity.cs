﻿
namespace Tik.DomainModels.CommonEntity
{
    public class ForgetPasswordEntity : BaseEntity
    {
        public string TemporaryPassword { get; set; }
        public string Status { get; set; }
        public string EmailAddress { get; set; }
    }
}
