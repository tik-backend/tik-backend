﻿using Newtonsoft.Json;

namespace Tik.DomainModels
{
    public class ReviewEntity : BaseEntity
    {
        [JsonProperty(PropertyName = "reviewRating")]
        public decimal ReviewRating { get; set; }

        public decimal? ProductQualityRating { get; set; }
        public decimal? TimelyDeliveryRating { get; set; }

        public decimal? PaymentRating { get; set; }
        public decimal? InputMaterialRating { get; set; }

        [JsonProperty(PropertyName = "Comments")]
        public string Comments { get; set; }
        [JsonProperty(PropertyName = "companyId")]
        public string CompanyId { get; set; }
        [JsonProperty(PropertyName = "projectId")]
        public string ProjectId { get; set; }
        [JsonProperty(PropertyName = "biddingId")]
        public string BiddingId { get; set; }
        public string ReviewedBy { get; set; }
        public bool isCompany { get; set; }
        public string Status { get; set; }
    }
}