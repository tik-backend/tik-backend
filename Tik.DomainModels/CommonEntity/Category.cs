﻿using System.ComponentModel.DataAnnotations;

namespace Tik.DomainModels.CommonEntity
{
    public enum Category
    {
        Company = 1,
        [Display(Name = "Company + JobWork Unit")]
        CompanyJobworkUnit = 2,
        Mills = 3,
        Knitting = 4,
        Dyeing = 5,
        Processing = 6,
        Printing = 7,
        [Display(Name = "Ready Fabrics")]
        ReadyFabrics = 8,
        Embroidery = 9,
        [Display(Name = "Job Work Units")]
        JobWorkUnits = 10,
        Pieces = 11
    }
}
