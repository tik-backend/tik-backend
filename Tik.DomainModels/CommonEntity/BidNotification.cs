﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.CommonEntity
{
    public class BidNotification : BaseEntity
    {
        public string Status { get; set; }        
        public string DisplayName { get; set; }
        
        //Bid status that are allowed for notification
        public bool IsAllowed { get; set; }
        public bool IsCompany { get; set; }
        public bool IsMill { get; set; }
    }
}
