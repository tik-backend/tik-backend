﻿
namespace Tik.DomainModels
{
    public class CertificationEntity : BaseEntity
    {
        public string CertificateId { get; set; }
        public string CertificateName { get; set; }
    }
}
