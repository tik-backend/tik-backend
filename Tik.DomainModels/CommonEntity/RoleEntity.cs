﻿
namespace Tik.DomainModels.CommonEntity
{
    public class RoleEntity : BaseEntity
    {
        public string Role { get; set; }
        public string Status { get; set; }
        public string IsCompany { get; set; }
        public string IsVendor { get; set; }
    }
    public enum Roles
    {
        MasterAdmin = 0
    }
}
