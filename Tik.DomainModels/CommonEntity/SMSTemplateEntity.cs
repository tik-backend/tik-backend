﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.CommonEntity
{
    public class SMSTemplateEntity : BaseEntity
    {
        public string Status { get; set; }        
        public string Message { get; set; }
        public bool IsCompany { get; set; }
        public bool IsMill { get; set; }
    }
}
