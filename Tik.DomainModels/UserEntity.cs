﻿using System;
using System.Collections.Generic;

namespace Tik.DomainModels
{
    public class UserEntity : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string EmployeeRole { get; set; }
        public string ApprovalAdminAccess { get; set; }
        public string CompanyId { get; set; }
        public AddressDetails AddressDetails { get; set; }
        public string Category { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public BidStatusNotification Notification { get; set; }
    }
    public class AddressDetails
    {
        public string Address { get; set; }
        public string ApartmentUnitOffice { get; set; }
        public int? CountryId { get; set; }
        public string StateId { get; set; }
        public string CityId { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber2 { get; set; }
    }
    public class BidStatusNotification
    {       
        public List<string> StatusList { get; set; }
        public bool? IsSMS { get; set; }
        public bool? IsEmail { get; set; }
        public bool? IsWhatsApp { get; set; }

    }

    
}
