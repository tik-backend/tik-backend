﻿using FluentValidation;
using FluentValidation.Validators;

namespace Tik.DomainModels.Validators
{
    public class EmployeeValidator : AbstractValidator<UserEntity>
    {
        [System.Obsolete]
        public EmployeeValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("First Name is required.")
                .Length(1, 250).WithMessage("FirstName must be between 1 and 250 characters.")
                .Matches("^[a-zA-Z\\s\\.]+$").WithMessage("First Name will allow only characters.");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("Last Name is required.")
                .Length(1, 250).WithMessage("LastName must be between 1 and 250 characters.")
                .Matches("^[a-zA-Z\\s\\.]+$").WithMessage("Last Name will allow only characters.");
            RuleFor(x => x.Gender).NotEmpty().WithMessage("Gender is required.")
                .Matches("^[a-zA-Z]+$").WithMessage("Gender will allow only characters."); ;
            RuleFor(x => x.EmailAddress).NotEmpty().WithMessage("Email Address is required.").
                EmailAddress(EmailValidationMode.Net4xRegex).WithMessage("Email Format is incorrect.");
            RuleFor(x => x.Password).MinimumLength(8).WithMessage("Password must be atleast 8 characters long.")
                .Matches("[A-Z]").WithMessage("Password must contain atleast one uppercase character.")
                .Matches("[a-z]").WithMessage("Password must contain atleast one lower character.")
            .Matches("[0-9]").WithMessage("Password must contain atleast one number.")
            .Matches("[^a-zA-Z0-9]").WithMessage("Password must contain atleast one special character.");
            RuleFor(x => x.PhoneNumber).NotNull().WithMessage("Phone Number is required.")
                .Length(10).WithMessage("Not a valid 10-digit Phone Number.")
                .Matches("^[0-9]*$").WithMessage("Phone Number format is wrong.");
            RuleFor(x => x.EmployeeRole).NotEmpty().WithMessage("Employee Role is required.");
            RuleFor(x => x.Category).NotEmpty().WithMessage("Category is required.");
        }
    }
}
