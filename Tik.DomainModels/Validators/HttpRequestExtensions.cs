﻿using FluentValidation;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Tik.DomainModels.Validators;

namespace Tik.DomainModels
{
    public static class HttpRequestExtensions
    {
        public static async Task<ValidatableRequest<T>> GetJsonBody<T, V>(this HttpRequestMessage request)
            where V : AbstractValidator<T>, new()
        {
            var requestObject = await GetJsonBody<T>(request);
            var validator = new V();
            var validationResult = validator.Validate(requestObject);

            if (!validationResult.IsValid)
            {
                return new ValidatableRequest<T>
                {
                    Value = requestObject,
                    IsValid = false,
                    Errors = validationResult.Errors
                };
            }

            return new ValidatableRequest<T>
            {
                Value = requestObject,
                IsValid = true
            };
        }
        public static async Task<ValidatableRequest<T>> ValidateRequest<T, V>(this T requestObject)
            where V : AbstractValidator<T>, new()
        {
            var validator = new V();
            var validationResult = validator.Validate(requestObject);

            if (!validationResult.IsValid)
            {
                return new ValidatableRequest<T>
                {
                    Value = requestObject,
                    IsValid = false,
                    Errors = validationResult.Errors
                };
            }

            return new ValidatableRequest<T>
            {
                Value = requestObject,
                IsValid = true
            };
        }

            public static async Task<T> GetJsonBody<T>(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(requestBody);
        }
    }
}
