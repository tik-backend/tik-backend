﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ProjectDomainModel;

namespace Tik.DomainModels.Validators
{
    public class BidProjectValidator: AbstractValidator<Bids>
    {
        public BidProjectValidator()
        {
            RuleFor(bid => bid.creditPeriod).NotEmpty().WithMessage("Credit Period is required.");
            RuleFor(bid => bid.deliveryPeriod).NotEmpty().WithMessage("Delivery Period is required.");
            RuleFor(bid => bid.price).NotEmpty().WithMessage("Price is required.");
        }
    }
}
