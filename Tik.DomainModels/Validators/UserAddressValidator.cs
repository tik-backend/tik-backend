﻿using FluentValidation;

namespace Tik.DomainModels.Validators
{
    public class UserAddressValidator : AbstractValidator<AddressDetails>
    {
        public UserAddressValidator()
        {
            RuleFor(x => x.CountryId).NotEmpty().WithMessage("Country Name is required.");
            RuleFor(x => x.StateId).NotEmpty().WithMessage("State Name is required.");
            RuleFor(x => x.CityId).NotEmpty().WithMessage("City Name is required.");
            RuleFor(x => x.ZipCode).NotEmpty().WithMessage("Zipcode is required.");
        }
    }
}
