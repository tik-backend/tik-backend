﻿using FluentValidation;

namespace Tik.DomainModels.Validators
{
    public class ProfileValidator : AbstractValidator<CompanyEntity>
    {
        public ProfileValidator()
        {
            RuleFor(x => x.UnitName).NotEmpty().WithMessage("Unit Name is required.");
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title is required.");
            RuleFor(x => x.Description).NotEmpty().WithMessage("Description is required.");
            //RuleFor(x => x.EmailAddress).NotEmpty().WithMessage("EmailAddress is required.");
        }
    }
}
