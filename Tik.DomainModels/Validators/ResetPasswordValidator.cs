﻿using FluentValidation;
using Tik.DomainModels.CommonEntity;

namespace Tik.DomainModels.Validators
{
    public class ResetPasswordValidator : AbstractValidator<ForgetPasswordEntity>
    {
        public ResetPasswordValidator()
        {
            RuleFor(x => x.TemporaryPassword).NotEmpty().WithMessage("Temporary password is required.");
            RuleFor(x => x.Id).NotEmpty().WithMessage("User Id is required.");
        }
    }
}
