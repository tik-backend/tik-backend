﻿using FluentValidation;

namespace Tik.DomainModels.Validators
{
    public class ReviewValidator : AbstractValidator<ReviewEntity>
    {
        public ReviewValidator()
        {
            RuleFor(x => x.ProjectId).NotEmpty().WithMessage("Project Id is required.");
            RuleFor(x => x.BiddingId).NotEmpty().WithMessage("Bidding Id is required.");

            When(review => review.isCompany, () =>
            {
                RuleFor(x => x.TimelyDeliveryRating)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Timely Delivery rating is required.")                
                .LessThanOrEqualTo(5)
                .WithMessage("Timely Delivery rating should not be greater than 5");

                RuleFor(x => x.ProductQualityRating)
                .Cascade(CascadeMode.Stop)
               .NotEmpty()
               .WithMessage("Product Quality rating is required.")
               .LessThanOrEqualTo(5)
               .WithMessage("Product Quality rating should not be greater than 5");

            });

            When(review => review.isCompany == false, () =>
            {
                RuleFor(x => x.PaymentRating)
                .Cascade(CascadeMode.Stop)
               .NotEmpty()
               .WithMessage("Payment rating is required.")
               .LessThanOrEqualTo(5)
               .WithMessage("Payment rating should not be greater than 5");

                RuleFor(x => x.InputMaterialRating)
                .Cascade(CascadeMode.Stop)
               .NotEmpty()
               .WithMessage("Input Material is required.")
               .LessThanOrEqualTo(5)
               .WithMessage("Input Material rating should not be greater than 5");
            });
        }
    }
}
