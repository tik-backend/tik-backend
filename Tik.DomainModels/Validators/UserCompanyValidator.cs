﻿using FluentValidation;

namespace Tik.DomainModels.Validators
{
    public class UserCompanyValidator : AbstractValidator<CompanyEntity>
    {
        public UserCompanyValidator()
        {
            RuleFor(x => x.CategoryName).NotEmpty().WithMessage("Category is required.");
            RuleFor(x => x.GSTNumber).NotEmpty().WithMessage("GST Number is required.");
            RuleFor(x => x.Certification).NotEmpty().WithMessage("Certificate is required.");
        }
    }
}
