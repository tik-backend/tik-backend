﻿using FluentValidation;
using FluentValidation.Validators;
using Tik.DomainModels.ViewModels;

namespace Tik.DomainModels.Validators
{
    public class UserValidator : AbstractValidator<UserEntity>
    {
        [System.Obsolete]
        public UserValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("First Name is required.")
                .Length(1, 250).WithMessage("FirstName must be between 1 and 250 characters.")
                .Matches("^[a-zA-Z\\s\\.]+$").WithMessage("First Name will allow only characters.");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("Last Name is required.")
                .Length(1, 250).WithMessage("LastName must be between 1 and 250 characters.")
                .Matches("^[a-zA-Z\\s\\.]+$").WithMessage("Last Name will allow only characters.");
            RuleFor(x => x.EmailAddress).NotEmpty().WithMessage("Email Address is required.").
                EmailAddress(EmailValidationMode.Net4xRegex).WithMessage("Email Format is incorrect.");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password is required.");
        }
    }
}
