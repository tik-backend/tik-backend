﻿
namespace Tik.DomainModels
{
    public class CountryEntity : BaseEntity
    {
        public string CountryName { get; set; }
        public string Iso3 { get; set; }
        public string Iso2 { get; set; }
        public string PhoneCode { get; set; }
        public string Capital { get; set; }
        public string Currency { get; set; }
        public int Status { get; set; }
    }
}
