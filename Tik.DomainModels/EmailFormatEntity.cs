﻿
namespace Tik.DomainModels
{
    public class EmailFormatEntity : BaseEntity
    {
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public string MailFooter { get; set; }
        public string Signature { get; set; }
        public string Status { get; set; }
        public string EmailTemplateName { get; set; }
    }

    public enum EmailTemplate
    {
        ForgetPasswordTemplate = 0
    }
}
