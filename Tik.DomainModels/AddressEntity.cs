﻿using System;
using System.Collections.Generic;
using System.Text;
using Tik.DomainModels.ViewModels;

namespace Tik.DomainModels
{
    public class AddressEntity : BaseEntity
    {
        public string Address { get; set; }
        public string ApartmentUnitOffice { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
    }
}
