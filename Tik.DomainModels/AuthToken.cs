﻿
using System;
using System.Net;

namespace Tik.DomainModels
{
    public class AuthToken : BaseEntity
    {
        public AuthToken()
        {
            ValidToken = false;
        }
        public bool ValidToken { get; set; }
        public DateTime ExpiresAt { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Error { get; set; }
        public bool IsExpired { get; set; }
        public string CompanyId { get; set; }
        public string Category { get; set; }
    }

    public class AuthenticationStatus
    {
        public HttpStatusCode Code { get; set; }
        public string Message { get; set; }
    }
}
