﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.Filter
{
    public class BidFilter
    {
        public string sortBy { get; set; }
        public string companyId { get; set; }
        public int minPrice { get; set; }
        public int maxPrice { get; set; }
        public int minCreditPeriod { get; set; }
        public int maxCreditPeriod { get; set; }
        public int minDeliveryPeriod { get; set; }
        public int maxDeliveryPeriod { get; set; }
        public string projectId { get; set; }
        public decimal? review { get; set; }
    }
}
