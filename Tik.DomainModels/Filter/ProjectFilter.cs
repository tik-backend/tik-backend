﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.Filter
{
    public class ProjectFilter
    {
        public string companyId { get; set; }
        public string stages { get; set; }
        public bool isCompany { get; set; }
        public string name { get; set; }
        public bool myproject { get; set; }

        public int[] Status
        {
            get
            {
                return getStage(stages, isCompany);
            }
        }

        public IDictionary<string, int[]> stageMapping;
        public int[] getStage(string stage, bool isCompany)
        {
            int[] stages;
            stageMapping = new Dictionary<string, int[]>();
            if (isCompany)
            {
                stageMapping.Add("All", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                stageMapping.Add("NewEnquiry", new int[] { 1, 2 });
                stageMapping.Add("SubmittedEnquirys", new int[] { 3, 4, 9 });
                stageMapping.Add("ConfirmedProjects", new int[] { 5, 6 });
                stageMapping.Add("CompletedProjects", new int[] { 7 });
                stageMapping.Add("CancelledProjects", new int[] { 10 });
                stageMapping.Add("NewProjects", new int[] { 1 });
                stageMapping.Add("BidReceived", new int[] { 2 });
                stageMapping.Add("ApprovalPending", new int[] { 3 });
                stageMapping.Add("AwaitingAuthentication", new int[] { 9 });
                stageMapping.Add("NoResponse", new int[] { 1, 2, 3, 4 , 9 });
                stageMapping.Add("NoShow", new int[] { 8 });
                stageMapping.Add("ConfirmedCompletedProjects", new int[] { 5, 6, 7 });
            }
            else
            {
                stageMapping.Add("All", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
                stageMapping.Add("Initiated", new int[] { 1 });
                stageMapping.Add("AwaitingApproval", new int[] { 2 });
                stageMapping.Add("Approved", new int[] { 3, 4, 5, 8 });
                stageMapping.Add("ConfirmedProjects", new int[] { 6 });
                stageMapping.Add("Completed", new int[] { 9 });
                stageMapping.Add("Rejected", new int[] { 7 });
                stageMapping.Add("NoShow", new int[] { 10 });
                stageMapping.Add("Cancelled", new int[] { 7, 11, 12 });
                stageMapping.Add("NoResponse", new int[] { 1, 2 });
                stageMapping.Add("ConfirmedCompletedProjects", new int[] { 6, 9 });
            }
            if (!stageMapping.TryGetValue(stage, out stages))
            {
                stages = new int[] { };
            }
            return stages;
        }
    }
}
