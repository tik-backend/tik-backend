﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class CountryViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "countryName")]
        public string CountryName { get; set; }
    }
}