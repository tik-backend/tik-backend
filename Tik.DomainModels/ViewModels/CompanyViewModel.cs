﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class CompanyViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "companyName")]
        public string UnitName { get; set; }
        [JsonProperty(PropertyName = "review")]
        public decimal? Review { get; set; }
        [JsonProperty(PropertyName = "logo")]
        public byte[] Image { get; set; }
    }
}
