﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class CityViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "stateId")]
        public string StateId { get; set; }
        [JsonProperty(PropertyName = "cityName")]
        public string CityName { get; set; }
    }
}
