﻿
using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class UserProfileViewModel
    {
        public string Id { get; set; }
        public string UnitName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public decimal? Review { get; set; }
        public string ImageName { get; set; }
        public byte[] Image { get; set; }
        
        [JsonProperty(PropertyName = "certification")]
        public string[] Certification { get; set; }
    }
}
