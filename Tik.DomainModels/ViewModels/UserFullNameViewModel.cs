﻿
using System;

namespace Tik.DomainModels.ViewModels
{
    public class UserFullNameViewModel
    {
        public string FullName { get; set; }
        public string CurrentDate { get; set; }
        public string CurrentDay { get; set; }
    }
}
