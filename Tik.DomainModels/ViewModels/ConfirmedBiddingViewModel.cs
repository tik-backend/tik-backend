﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class ConfirmedBiddingViewModel
    {
        [JsonProperty(PropertyName = "price")]
        public string Price { get; set; }

        [JsonProperty(PropertyName = "deliveryPeriod")]
        public int? DeliveryPeriod { get; set; }

        [JsonProperty(PropertyName = "creditPeriod")]
        public int? CreditPeriod { get; set; }

        [JsonProperty(PropertyName = "companyReview")]
        public decimal? CompanyReview { get; set; }
    }
}
