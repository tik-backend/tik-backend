﻿
namespace Tik.DomainModels.ViewModels
{
    public class UserViewModel
    {
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
