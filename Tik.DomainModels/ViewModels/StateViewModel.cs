﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class StateViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }
        [JsonProperty(PropertyName = "stateId")]
        public string stateId { get; set; }
    }
}
