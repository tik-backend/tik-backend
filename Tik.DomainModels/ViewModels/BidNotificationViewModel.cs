﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class BidNotificationViewModel
    {
        public string Id { get; set; }
        public string Status { get; set; }
    }
}
