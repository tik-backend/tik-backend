﻿
namespace Tik.DomainModels.ViewModels
{
    public class ReviewViewModel
    {
        public string ReviewRating { get; set; }
        public string Comments { get; set; }
    }
}
