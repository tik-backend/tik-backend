﻿
namespace Tik.DomainModels.ViewModels
{
    public class CategoryViewModel
    {
        public string Id { get; set; }
        public string CategoryName { get; set; }
    }
}
