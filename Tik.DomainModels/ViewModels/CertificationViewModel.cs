﻿using Newtonsoft.Json;

namespace Tik.DomainModels.ViewModels
{
    public class CertificationViewModel
    {
        [JsonProperty(PropertyName = "certificateID")]
        public string CertificateID { get; set; }
        [JsonProperty(PropertyName = "certificateName")]
        public string CertificateName { get; set; }
    }
}