﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.Utilities.Constants
{
    public static class Constant
    {
        public const string MILLS_CONTENTTYPE_SINGLE = "Single";
        public const string MILLS_CONTENTTYPE_BLEND = "Blend";

        public const string MILLS_MELANGE = "Melange";
        public const string MILLS_SLUB = "Slub";
        public const string MILLS_Regular = "Regular";
        public const string MILLS_Special = "Special";
        public const string MILLS_Dyed = "Dyed";

        public const string MILLS_SINGLEJERSEY = "Single Jersey";
        public const string MILLS_INTERLOCK = "Interlock";
        public const string MILLS_RIB = "RIB";

        public const string DYEING_TUBULAR = "TUB";
        public const string DYEING_OPENWIDTH = "OPW";


    }
}
