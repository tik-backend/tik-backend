﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.Helpers
{
   public class MillsHelper
    {
        public List<int> GetCountsDeniers()
        {
            List<int> CountsDeniers = new List<int>();
            for (int i = 1; i <= 100; i++)
            {
                CountsDeniers.Add(i);
            }
            return CountsDeniers;
        }
    }
}
