﻿using Newtonsoft.Json;

namespace Tik.DomainModels
{
    public class CompanyEntity : BaseEntity
    {
        public string UnitName { get; set; }
        [JsonProperty(PropertyName = "categoryName")]
        public string CategoryName { get; set; }
        public string GSTNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ImageId { get; set; }
        public string ImageName { get; set; }
        public string ReviewRating { get; set; }
        public decimal? Review { get; set; }
        public int? MinimumBidsRequired { get; set; }
        public double? MinimumTimeToRespond { get; set; }
        public double? MaximumTimeToRespond { get; set; }
        [JsonProperty(PropertyName = "certification")]
        public string[] Certification { get; set; }
    }
}
