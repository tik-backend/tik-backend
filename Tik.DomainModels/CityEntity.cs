﻿
namespace Tik.DomainModels
{
    public class CityEntity : BaseEntity
    {
        public string CityID { get; set; }
        public string CityName { get; set; }
        public string StateId { get; set; }
        public string CountryId { get; set; }
        public string Status { get; set; }
    }
}
