﻿
namespace Tik.DomainModels
{
    public enum Role
    {
        SuperAdmin = 1,
        MasterAdmin = 2,
        ApprovalAdmin = 3,
        Employee = 4,
    }
}