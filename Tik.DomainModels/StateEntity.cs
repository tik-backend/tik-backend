﻿
namespace Tik.DomainModels
{
    public class StateEntity : BaseEntity
    {
        public string StateID { get; set; }
        public string StateName { get; set; }
        public string CountryId { get; set; }
        public string Status { get; set; }
    }
}
