﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels
{
    public class MillsEntity: BaseEntity
    {
        public string CompanyId { get; set; }
        public string CreatorId { get; set; }
        public string AVGCreditTime { get; set; }
    }
}
