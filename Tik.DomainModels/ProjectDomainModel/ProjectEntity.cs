﻿using System;
using System.Collections.Generic;
using Tik.DomainModels.Enum;

namespace Tik.DomainModels.ProjectDomainModel
{
    public class ProjectBase: BaseEntity
    {
        public string price { get; set; }
        public int? creditPeriod { get; set; }
        public string creatorId { get; set; }
        public string UpdatedBy { get; set; }
        public string ApprovalAdminId { get; set; }
        public string Message { get; set; }
    }
    public class ProjectEntity : ProjectBase
    {
        public string name { get; set; }
        public string noOfKgs { get; set; }
        public DateTime confirmationDate { get; set; }
        public List<Summary> controls { get; set; }
        public string companyId { get; set; }
        public ProjectStages stages { get; set; }
        public string status { get; set; }
        public string category { get; set; }
        public DateTime deliveryDate { get; set; }
        public List<Bids> bids { get; set; }
        public string merchandiserId { get; set; }
        public bool requestPrice { get; set; }
        public ProjectData data { get; set; }
        public string BiddingTime { get; set; }
    }

    public class Bids : ProjectBase
    {
        public string companyId { get; set; }
        public BidStatus bidStatus { get; set; }
        public int? deliveryPeriod { get; set; }       
        public DateTime? SubmittedDate { get; set; }
    }

    public class ProjectData
    {
        public string noOfKgs { get; set; }
        public DateTime confirmationDate { get; set; }       
        public DateTime deliveryDate { get; set; }
        public int? creditPeriod { get; set; }
        public string gsm { get; set; }
        public string loopLength { get; set; }
        public Dia dia { get; set; }
        public Price price { get; set; }
        public string certification { get; set; }
    }
    public class Price
    {
        public string price { get; set; }
        public bool requestPrice { get; set; }
    }
    public class Item
    {
        public int dia { get; set; }
        public string kgs { get; set; }
    }

    public class Dia
    {
        public List<Item> items { get; set; }
        public int total { get; set; }
    }

    public class Summary
    {
        public string id { get; set; }
        public string label { get; set; }
        public dynamic value { get; set; }
        public string path { get; set; }
        public string data_id { get; set; }
        public string data_path { get; set; }
        public string joinValue { get; set; }
    }      
}
