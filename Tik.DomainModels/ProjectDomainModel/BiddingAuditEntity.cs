﻿using Tik.DomainModels.Enum;

namespace Tik.DomainModels.ProjectDomainModel
{
    public class BiddingAuditEntity : BaseEntity
    {
        public string projectId { get; set; }

        public bool isCompany { get; set; }

        public string companyId { get; set; }

        public string bidId { get; set; }

        public string userId { get; set; }
        public BidStatus bidStatus{ get; set; }
        public string oldBidStatus { get; set; }
    }
}
