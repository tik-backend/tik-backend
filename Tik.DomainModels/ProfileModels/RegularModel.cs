﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class RegularDomainModel : BaseEntity
    {
        public List<SingleContentDomainModel> SingleContents { get; set; }
        public List<BlendContentDomainModel> BlendContents { get; set; }
        public List<MillsContentQualityDomainModel> SingleQualities { get; set; }
        public List<MillsContentQualityDomainModel> BlendQualities { get; set; }
        public List<string> Counts { get; set; }
        public List<string> Deniers { get; set; }
        public string AVGCreditTime { get; set; }
        public FabricStructureDomainModel FabricStructure { get; set; }
    }
}
