﻿using System.Collections.Generic;

namespace Tik.DomainModels.ProfileModels
{
    public class DyeingTubularProcessDomainModel
    {
        public string HeatSetting { get; set; }
        public List<string> SpecialFinishes { get; set; }
        public List<DyeingTubularDryingProcessDomainModel> TubularDryingProcesses { get; set; }
        public DyeingOpenWidthDryingProcessDomainModel OpenWidthDryingProcesses { get; set; }
        public List<string> OpenWidthCompactingMachines { get; set; }
        public List<string> TubularCompactingMachines { get; set; }
    }
    public class DyeingSpecialFinishesDomainModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
    public class DyeingTubularDryingProcessDomainModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
    public class CompactingMachinesDomainModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}