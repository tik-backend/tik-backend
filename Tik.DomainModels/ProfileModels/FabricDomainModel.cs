﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class FabricDomainModel
    {
        public RegularDomainModel RegularFabric { get; set; }
        public MelangeSlubDomainModel MelangeSlubFabric { get; set; }
        public SpecialDyedDomainModel SpecialFabric { get; set; }
    }
    public class FabricStructureDomainModel
    {
        public List<FabricTypesDomainModel> SingleJersey { get; set; }
        public List<FabricTypesDomainModel> Interlock { get; set; }
        public List<FabricTypesDomainModel> Rib { get; set; }
        public FabricRollFormat Tubular { get; set; }
        public FabricRollFormat OpenWidth { get; set; }
    }
    public class FabricTypesDomainModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
    }
    public class FabricRollFormat
    {
        public List<string> GG { get; set; }
        public List<string> Dia { get; set; }
        public string AllFeeder { get; set; }
        public string AlternateFeeder { get; set; }
    }
}
