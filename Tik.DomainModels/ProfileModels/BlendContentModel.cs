﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class BlendContentDomainModel
    {
        public MelanSlubTypeDomainModel MelanSlubType { get; set; }
        public MillsContentDomainModel Content { get; set; }
        public List<ComboDomainModel> Combos { get; set; }
    }
}
