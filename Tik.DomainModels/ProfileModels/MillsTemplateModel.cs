﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class MillsTemplateModel
    {
        public YarnTemplateModel Yarn { get; set; }
        public FabricTemplateModel Fabric { get; set; } 
    }
    public class FabricTemplateModel
    {
        public RegularTemplateModel RegularFabric { get; set; }
        public MelangeSlubTemplateModel MelangeSlubFabric { get; set; }
        public SpecialDyedTemplateModel SpecialFabric { get; set; }
    }
    public class FabricStructureTemplateModel
    {
        public List<FabricTypesTemplateModel> SingleJersey { get; set; }
        public List<FabricTypesTemplateModel> Interlock { get; set; }
        public List<FabricTypesTemplateModel> Rib { get; set; }
        public FabricRollFormatTemplateModel Tubular { get; set; }
        public FabricRollFormatTemplateModel OpenWidth { get; set; }
    }
    public class FabricTypesTemplateModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
    public class FabricRollFormatTemplateModel
    {
        public List<string> GG { get; set; }
        public List<string> Dia { get; set; }
        public string AllFeeder { get; set; }
        public string AlternateFeeder { get; set; }
    }
    public class YarnTemplateModel
    {
        public RegularTemplateModel RegularYarn { get; set; }
        public MelangeSlubTemplateModel MelangeSlubYarn { get; set; }
        public SpecialDyedTemplateModel SpecialYarn { get; set; }
        public SpecialDyedTemplateModel DyedYarn { get; set; }
    }
    public class SpecialDyedTemplateModel : BaseEntity
    {
        public List<SpecialDyedYarnTypeTemplate> SingleSpecialDyedTypes { get; set; }
        public List<SpecialDyedYarnTypeTemplate> BlendSpecialDyedTypes { get; set; }
        public List<SingleContentTemplateModel> SingleContents { get; set; }
        public List<BlendContentTemplateModel> BlendContents { get; set; }
        public List<MillsContentQualityTemplate> SingleQualities { get; set; }
        public List<MillsContentQualityTemplate> BlendQualities { get; set; }
        public List<int> Counts { get; set; }
        public List<int> Deniers { get; set; }
        public string AVGCreditTime { get; set; }
        public FabricStructureTemplateModel FabricStructure { get; set; }
    }
    public class SpecialDyedYarnTypeTemplate : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
    public class RegularTemplateModel : BaseEntity
    {
        public List<SingleContentTemplateModel> SingleContents { get; set; }
        public List<BlendContentTemplateModel> BlendContents { get; set; }
        public List<MillsContentQualityTemplate> SingleQualities { get; set; }
        public List<MillsContentQualityTemplate> BlendQualities { get; set; }
        public List<int> Counts { get; set; }
        public List<int> Deniers { get; set; }
        public string AVGCreditTime { get; set; }
        public FabricStructureTemplateModel FabricStructure { get; set; }
    }
    public class MillsContentQualityTemplate : BaseEntity
    {
        public string Name;
        public string Category;
    }
    public class SingleContentTemplateModel
    {
        public List<MelanSlubTypeTemplateModel> MelanSlubTypes { get; set; }
        public List<MillsContentTemplateModel> Contents { get; set; }
        public string Filament { get; set; }
        public string Spun { get; set; }
    }
    public class MelangeSlubTemplateModel : BaseEntity
    {
        public MelanSlubTemplateModel Melange { get; set; }
        public MelanSlubTemplateModel Slub { get; set; }

    }
    public class BlendContentTemplateModel
    {
        public List<MelanSlubTypeTemplateModel> MelanSlubTypes { get; set; }
        public List<MillsContentTemplateModel> Contents { get; set; }
        public List<ComboTemplateModel> Combos { get; set; }
    }
    public class ComboTemplateModel
    {
        public int CombinationOne { get; set; }
        public int CombinationTwo { get; set; }
    }
    public class MillsContentTemplateModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
    public class MelanSlubTemplateModel
    {
        public List<MelanSlubTypeTemplateModel> MelanSlubTypes { get; set; }
        public List<SingleContentTemplateModel> SingleContents { get; set; }
        public List<BlendContentTemplateModel> BlendContents { get; set; }
        public List<int> Counts { get; set; }
        public string AVGCreditTime { get; set; }
        public FabricStructureTemplateModel FabricStructure { get; set; }
    }
    public class MelanSlubTypeTemplateModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
