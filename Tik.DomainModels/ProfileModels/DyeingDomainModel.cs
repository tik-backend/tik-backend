﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
   public class DyeingDomainModel : BaseEntity
    {
        public string CompanyId { get; set; }
        public DyeingYarnDomainModel YarnDyeing { get; set; }
        public DyeingFabricDomainModel FabricDyeing { get; set; }
    }
}
