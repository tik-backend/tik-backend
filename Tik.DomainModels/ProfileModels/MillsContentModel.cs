﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class MillsContentDomainModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
