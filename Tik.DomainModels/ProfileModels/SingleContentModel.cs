﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class SingleContentDomainModel
    {
        public MelanSlubTypeDomainModel MelanSlubType { get; set; }
        public MillsContentDomainModel Content { get; set; }
        public string Filament { get; set; }
        public string Spun { get; set; }
    }
}
