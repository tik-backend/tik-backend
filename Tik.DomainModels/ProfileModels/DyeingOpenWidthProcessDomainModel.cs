﻿using System;
using System.Collections.Generic;

namespace Tik.DomainModels.ProfileModels
{
    public class DyeingOpenWidthProcessDomainModel
    {
        public string HeatSetting { get; set; }
        public List<string> SpecialFinishes { get; set; }
        public DyeingOpenWidthDryingProcessDomainModel OpenWidthDryingProcesses { get; set; }
        public List<string> OpenWidthCompactingMachines { get; set; }
    }
    public class DyeingOpenWidthDryingProcessDomainModel
    {
        public string StenterDryer { get; set; }
        public string RelaxDryer { get; set; }
        public List<DyeingDipStrenterDomainModel> DipStenters { get; set; }
    }
    public class DyeingDipStrenterDomainModel : BaseEntity
    {
        public string Name { get; set; }
    }
}