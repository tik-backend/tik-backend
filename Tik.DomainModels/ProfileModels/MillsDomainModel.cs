﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class MillsDomainModel : BaseEntity
    {
        public string CompanyId { get; set; }
        public string CreatorId { get; set; }
        public YarnDomainModel Yarn { get; set; }
        public FabricDomainModel Fabric { get; set; }
    }
}
