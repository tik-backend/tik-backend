﻿using System.Collections.Generic;

namespace Tik.DomainModels.ProfileModels
{
    public class DyeingMethodologyDomianModel
    {
        public List<DyeingProcessDomainModel> DyeingProcesses { get; set; }
        public List<MillsContentDomainModel> SingleContents { get; set; }
        public List<MillsContentDomainModel> BlendContents { get; set; }
        public List<FabricTypesDomainModel> SingleJersey { get; set; }
        public List<FabricTypesDomainModel> Interlock { get; set; }
        public List<FabricTypesDomainModel> Rib { get; set; }
        public List<string> VesselSizes { get; set; }
        public string LycraBlend { get; set; }
        public DyeingTubularProcessDomainModel TubularProcess {get;set;}
        public DyeingOpenWidthProcessDomainModel OpenWidthProcess { get; set; }
    }
}