﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class YarnDomainModel
    {
        public RegularDomainModel RegularYarn { get; set; }
        public MelangeSlubDomainModel MelangeSlubYarn { get; set; }
        public SpecialDyedDomainModel SpecialYarn { get; set; }
        public SpecialDyedDomainModel DyedYarn { get; set; }
    }
}
