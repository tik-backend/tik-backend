﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class ComboDomainModel
    {
        public int CombinationOne { get; set; }
        public int CombinationTwo { get; set; }
    }
}
