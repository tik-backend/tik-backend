﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.ProfileModels
{
    public class MelangeSlubDomainModel : BaseEntity
    {
        public MelanSlubDomainModel Melange { get; set; }
        public MelanSlubDomainModel Slub { get; set; }
        
    }
    public class MelanSlubDomainModel
    {
        public List<MelanSlubTypeDomainModel> MelanSlubTypes { get; set; }
        public List<SingleContentDomainModel> SingleContents { get; set; }
        public List<BlendContentDomainModel> BlendContents { get; set; }
        public List<string> Counts { get; set; }
        public string AVGCreditTime { get; set; }
        public FabricStructureDomainModel FabricStructure { get; set; }
    }
    public class MelanSlubTypeDomainModel : BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
