﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tik.DomainModels.Enum
{
    public enum ProjectStages
    {
        [Display(Name = "Enquiry Sent")]
        EnquirySent = 1,

        [Display(Name = "Bid Received")]
        BidReceived = 2,

        [Display(Name = "Awaiting Approval")]
        BidAuthenticated = 3,

        [Display(Name = "Bid Approved")]
        BidApproved = 4,

        [Display(Name = "Confirmed")]
        Confirmed = 5,

        [Display(Name = "In Progress")]
        InProgress = 6,

        [Display(Name = "Completed")]
        Completed = 7,

        [Display(Name = "No Show")]
        NoShow = 8,

        [Display(Name = "Awaiting Authentication")]
        BidSelected = 9,

        [Display(Name = "No Response")]
        NoResponse = 10,
    }
}
