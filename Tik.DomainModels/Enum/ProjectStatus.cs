﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tik.DomainModels.Enum
{
    public enum ProjectStatus
    {
        Green = 1,
        Orange = 2,
        Red = 3
    }
}
