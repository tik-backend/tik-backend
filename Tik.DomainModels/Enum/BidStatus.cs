﻿using System.ComponentModel.DataAnnotations;

namespace Tik.DomainModels.Enum
{
    public enum BidStatus
    {
        [Display(Name = "Initiated")]
        Initiated = 1,

        [Display(Name = "Awaiting Approval")]
        Authenticated = 2,

        [Display(Name = "Approved")]
        Approved = 3,

        [Display(Name = "Selected")]
        Selected = 4,

        [Display(Name = "Approved")] // Authenticate 
        BidApproved = 5,

        [Display(Name = "Confirmed")]
        Confirmed = 6,

        [Display(Name = "Rejected")]
        Rejected = 7,

        [Display(Name = "Awaiting Confirmation")]
        AwaitingConfirmation = 8,

        [Display(Name = "Completed")]
        Completed = 9,

        [Display(Name = "No Show")]
        NoShow = 10,

        [Display(Name = "Cancelled")]
        Cancelled = 11,

        [Display(Name = "No Response")]
        NoResponse = 12
    }
}
