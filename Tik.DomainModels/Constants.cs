﻿namespace Tik.DomainModels
{
    public static class Constants
    {
        /// <summary>
        ///     You can put this secret key in your config instead or read it from Key Vault
        /// </summary>
        public static readonly string SECRET_KEY = "asdv234234^&%&^%&^hjsdfb2%%%";

        public static readonly string Issuer = "www.tikonline.in";
    }
}
