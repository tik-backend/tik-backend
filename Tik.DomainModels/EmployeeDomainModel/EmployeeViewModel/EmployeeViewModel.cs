﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Tik.DomainModels.ViewModels
{
    public class EmployeeViewModel
    {
        [JsonProperty(PropertyName = "FirstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "Gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "EmployeeRole")]
        public string EmployeeRole { get; set; }

        [JsonProperty(PropertyName = "EmailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty(PropertyName = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "ApprovalAdminAccess")]
        public string ApprovalAdminAccess { get; set; }

        [JsonProperty(PropertyName = "EmployeeId")]
        public string EmployeeId { get; set; }

        [JsonProperty(PropertyName = "Category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "StatusList")]
        public List<string> StatusList { get; set; }

        [JsonProperty(PropertyName = "IsSMS")]
        public bool? IsSMS { get; set; }

        [JsonProperty(PropertyName = "IsEmail")]
        public bool? IsEmail { get; set; }

        [JsonProperty(PropertyName = "IsWhatsApp")]
        public bool? IsWhatsApp { get; set; }

        [JsonProperty(PropertyName = "FullName")]
        public string FullName { get; set; }
    }
}
