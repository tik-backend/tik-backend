﻿using Newtonsoft.Json;

namespace Tik.DomainModels.EmployeeDomainModel.EmployeeViewModel
{
    public class RoleViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "EmployeeRole")]
        public string EmployeeRole { get; set; }
    } 

    public class MasterAdminViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "MasterAdmin")]
        public string MasterAdmin { get; set; }
    }

    public class ApprovalAdminViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "ApprovalAdmin")]
        public string ApprovalAdmin { get; set; }
    }

    public class MerchandiserViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "Merchandiser")]
        public string Merchandiser { get; set; }
    }
}
